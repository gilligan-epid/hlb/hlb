import context
import data.const as const
import data.input.california as inp
import data.output.california as outp
import idm.model.struct as mstruct
import idm.model.SECD as mlib
import idm.simulation.struct as sstruct
import casouth_configs
import cv_configs
import cvreset_configs
from fit_slurm import make_outdir

import sys
import numpy as np
import copy
np.seterr(divide='ignore')

data_type = ['acptrap']
obs_models = ['Noulli']
n_dcomp = 0


def main(run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
         delta_ratio=1, eta=-1., condor_run=True, adir=None, beta_ratio=1,
         ttrunc=0, tradius=None, qtrunc=0, qradius=None, extend_run=False,
         n_iter_sim=50, n_block_sim=2, with_retropred=0, with_movie=False,
         data_map=False, retro_suffix='', chunk_no=0, n_skipped_samp=0,
         treatment_stop=False, param_cf_no=-1):

    """
    Predictive simulation
     1. Load parameter distribution
    """
    #
    # Region-specific settings
    #
    hc = casouth_configs if region == 'CAsouth' else\
        cv_configs if region == 'CentralValley' else\
        cvreset_configs if region == 'CVreset' else None
    cf = hc.configs['acptrap'][cf_no]
    yr0 = 2009 if region == 'CAsouth' else 2012
    n_snaps = 1
    cf0 = hc.configs['acptrap'][0]
    if extend_run and cf_no != 0:
        tF = cf0['tF']
        tFobs = cf['tFobs']
        snap_points = [cf['tFobs'], cf0['tFobs']]
    else:
        tF = cf['tF']
        tFobs = cf['tFobs']
        snap_points = [tFobs] if tF > tFobs else []
    tstop, treset = None, None
    if treatment_stop and hc.acp_tchange is not None:
        tstop = hc.acp_tchange
        treset = tstop + 30 * 9
    #
    # Create options
    #
    app_opts = inp.AppOptions(
        region=region, csize=hc.resol, mlib=mlib, data_type=data_type,
        n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
        obs_models=obs_models, data_tau=hc.data_tau, cscale=hc.cscale,
        with_weather=cf['with_weather'], vector_cov=cf['vector_cov'],
        latent_init_period=cf['latent_dt'], init_source='data',
        plant_thres=cf['plant_thres'], n_dcomp=n_dcomp, condor_run=condor_run,
        urban_fraction=cf['urban_fraction'], nw_f=cf['nw_f'], t0=cf['t0'],
        tF=tF, with_data=True, adir=adir, tFobs=tFobs,
        tparams=[ttrunc], qparams=[0, qtrunc],
    )
    snap_times, ignore = outp.make_snap_times(app_opts, n_snaps=n_snaps,
                                              snap_points=snap_points,)
    print('snapshots:', snap_times)
    sim_tau = 1  # ?
    sim_opts = sstruct.SimOptions(
        outdir=app_opts.outdir, n_comp=mlib.n_comp, snap_times=snap_times,
        n_iter=n_iter_sim, n_block=n_block_sim, tau=sim_tau,
        primary_intros=cf['primary_intros'], with_single=True, n_dcomp=n_dcomp,
        chunk_no=chunk_no, n_skipped_samp=n_skipped_samp,
    )
    if data_map:
        outp.make_data_maps(app_opts)
        assert False
    #
    # Load parameter estimates
    #
    param_list = [
        'alpha', 'mu', 'beta', 'epsilon', 'eta', 'varepsilon', 'delta']
    cprefix = 'c{}_'.format(chain_no) if param_region is not None else ''
    if param_region is not None:
        param_cf_no = cf_no if param_cf_no < 0 else param_cf_no
        app_opts.cprefix = cprefix
        app_opts.paramdir = make_outdir(
            run_type, 'hlb_acp', param_wt, param_cf_no, '{}_1.0km'.format(
                param_region))
        print app_opts.paramdir
        param_dist = sstruct.PopLearn.load_params(
            app_opts.paramdir, param_list, prefix=cprefix)
        # Adjust parameters
        param_dist['gamma'] = np.ones_like(param_dist['beta']) * cf['gamma']
        param_dist['drate'] = np.ones_like(param_dist['beta']) * cf['drate']
        print "beta mean", param_dist['beta'].mean()
        print np.percentile(param_dist['beta'], 5),
        print np.percentile(param_dist['beta'], 95)
        param_dist['beta'] *= beta_ratio * param_dist['delta']
        print "beta mean", param_dist['beta'].mean()
        print np.percentile(param_dist['beta'], 5),
        print np.percentile(param_dist['beta'], 95)
        print "delta mean", param_dist['delta'].mean()
        print np.percentile(param_dist['delta'], 5),
        print np.percentile(param_dist['delta'], 95)
        print "alpha mean", param_dist['alpha'].mean()
        assert False
        # no primary infections
        param_dist['epsilon'][:] = 1e-5
        param_dist['varepsilon'] = param_dist['varepsilon'][:, 0] * 0
        if eta >= 0:
            param_dist['eta'][:] = eta
        param_dist['dprob'] = np.ones_like(param_dist['beta'])
    else:
        assert False
    #
    # Set simulation configurations
    #
    smod_opts = mstruct.ModelOptions(
        param_dist=param_dist, param_wbins=cf['param_wbins'],
        dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
    )
    smod_opts_copy = copy.deepcopy(smod_opts)
    print 'with_retropred', with_retropred
    if with_retropred:
        # backward model
        smod_opts_copy = copy.deepcopy(smod_opts)
        app_opts.tF = cf['tFobs']
        postmodel = inp.load_popmodel(
            app_opts, smod_opts, True, 'data', traj_update=False)
        # forward model
        app_opts.t0 = cf['tFobs']
        app_opts.tF = cf['tF']
        app_opts.init_dt = 0
        priormodel = inp.load_popmodel(
            app_opts, smod_opts_copy, False, tradius=tradius, qradius=qradius,
            tstop=tstop, treset=treset)
        posts_dir = make_outdir(
            run_type, 'acptrap', param_wt, cf_no, '{}_1.0km'.format(
                region), hc=hc) + '_retropred_acp{}'.format(param_region)
        posts_dir += '_br{}{}'.format(1 - 1.1 * ttrunc, retro_suffix)\
            if ttrunc > 0 else retro_suffix
        psim = sstruct.PopSimRetro(
            postmodel, priormodel, sim_opts, posts_dir=posts_dir)
        app_opts.t0 = cf['t0']
        app_opts.init_dt = cf['latent_dt']
    else:
        smodel = inp.load_popmodel(
            app_opts, smod_opts, False, tradius=tradius, qradius=qradius,
            tstop=tstop, treset=treset)
        #
        outp.write_config_file(app_opts, sim_opts, 'sim')
        psim = sstruct.PopSim(smodel, sim_opts)
    #
    # Run simulations
    #
    psim.run(prefix=cprefix)
    outp.make_sim_maps(
        app_opts, prim_intros=cf['primary_intros'][1], n_snaps=n_snaps,
        snap_points=snap_points, yr0=yr0, corrgram=True)
    outp.make_roc_plots(app_opts, tFobs=cf['tFobs'])
    if with_movie and run_type == 'long':
        app_opts.dtau = ['Week', 7]
        outp.make_sim_movies(app_opts, hobs=True)


if __name__ == '__main__':
    args = list(sys.argv)
    if len(args) != 22:
        print(len(args))
        for i in xrange(len(args)):
            print(i, args[i])
        print "Usage: python prospred_acp.py ",
        print "<run_type> <param_wt> <chain_no> <cf_no> <eps_ratio>",
        print "<region> <param_region> <delta_ratio> <eta> <beta_ratio>",
        print "<qtrunc> <qradius> <ttrunc> <tradius>",
        print "<with_retropred> <retro_suffix> <n_iter_sim>",
        print "<n_block_sim> <n_skipped_samp> <chunk_no>",
        print "<param_cf_no>"
    else:
        main(args[1], args[2], int(args[3]), int(args[4]), float(args[5]),
             args[6], args[7], float(args[8]), eta=float(args[9]),
             beta_ratio=float(args[10]), qtrunc=float(args[11]),
             qradius=float(args[12]), ttrunc=float(args[13]),
             tradius=float(args[14]), with_retropred=int(args[15]),
             retro_suffix=args[16], n_iter_sim=int(args[17]),
             n_block_sim=int(args[18]), n_skipped_samp=int(args[19]),
             chunk_no=int(args[20]), param_cf_no=int(args[21]))
