import context
import data.const as const
import data.input.texas
import data.input.california
import data.output.texas as outp
import hidalgo_configs
import casouth_configs
import idm.model.SECI as mlib
import idm.model.struct as mstruct
import idm.simulation.struct as sstruct

import os
import numpy as np


region = 'CAsouth'
cf_no, model = 0, 'hlb_acp'
chain_no = 0
prefix = 'longfit_v2.15'
#
adir = '{}_{}_pw0.01_cf{}_wTrue_fd1'.format(prefix, model, cf_no)
hc = hidalgo_configs if region == 'TX' else casouth_configs
cf = hc.configs[model][cf_no]
inp = data.input.texas if region == 'TX' else data.input.california
init_source = 'infer' if region == 'TX' else 'data'
latent_dt = cf['latent_dt'] if init_source == 'data' else 0
prim_frac = 0.5 if region == 'CAsouth' else 1
#
app_opts = inp.AppOptions(
    region='TX', csize=hc.resol, mlib=mlib, data_type=['plant'],
    n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
    data_tau=hc.data_tau, cscale=hc.cscale,
    with_weather=cf['with_weather'], vector_cov=cf['vector_cov'],
    latent_init_period=latent_dt, init_source=init_source,
    n_dcomp=0, adir=adir, tF=cf['tF'], cprefix='c{}_'.format(chain_no),
)
#
param_list = ['alpha', 'mu', 'beta', 'epsilon', 'eta', 'varepsilon',
              'drate', 'sigma', 'dprob']
param_dist = sstruct.PopLearn.load_params(
    os.path.join(const.output_path, app_opts.dname, adir), param_list,
    prefix=app_opts.cprefix)
param_dist['gamma'] = np.ones_like(param_dist['beta']) * cf['gamma']
param_dist['drate'] = 1. / (
    1. / param_dist['sigma'] + 1. / param_dist['drate'])
for pname in param_list:
    param_dist[pname] = [param_dist[pname].mean(0)]
for pname in ['beta', 'epsilon', 'varepsilon', 'gamma', 'drate']:
    param_dist[pname] = [param_dist[pname][0] * 365]
#
smod_opts = mstruct.ModelOptions(
    param_dist=param_dist, param_wbins=cf['param_wbins'],
    dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
    eta_fn=cf['eta_fn'], knorm=cf['knorm'])
app_opts = inp.AppOptions(
    region=region, csize=hc.resol, mlib=mlib, data_type=['plant'],
    n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
    data_tau=hc.data_tau, cscale=hc.cscale,
    with_weather=cf['with_weather'], vector_cov=cf['vector_cov'],
    latent_init_period=latent_dt, init_source=init_source,
    n_dcomp=0, adir=adir, tF=cf['tF'], cprefix='c{}_'.format(chain_no),
)
outp.make_cmi_input_files(
    app_opts, smod_opts, inlib=inp, prim_frac=prim_frac)
