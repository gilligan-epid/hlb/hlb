import data.const as const
import cv_configs as cc
from cv_configs import resol, cscale, dist_band, hdens_threshold, data_tau

import copy
import datetime

#
cf_acp = copy.deepcopy(cc.cf_acp)
cf_acp['t0'] = const.tFobs_acp('CVreset')
cf_acp['tFobs'] = const.tFobs_acp('CVreset') + 1
cf_acp['tF'] = const.tF_acp('CVreset') + 1
acp_tchange = (datetime.datetime(2027, 1, 1) - const.date0['CVreset']).days

#
cf_hlb = copy.deepcopy(cc.cf_hlb)
cf_hlb['t0'] = cf_acp['t0']
cf_hlb['tFobs'] = const.tFobs('CVreset') + 1
cf_hlb['tF'] = const.tF('CVreset') + 1

#
configs = {
    'acptrap': [cf_acp],
    'acp_hlb': [cf_hlb]
}
