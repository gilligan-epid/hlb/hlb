"""
This file sets the context for running the project.

It does so by adding the paths to the current directory and the idmf directory
to the system paths.
"""

from machine import machine_type

import os
import sys
import matplotlib


if machine_type == 'work':
    if sys.platform == 'win32':
        # Path to models
        sys.path.append(os.path.join('E:/', 'PROJECTS', 'IDMF'))
        # Path to data
        sys.path.append(os.path.join(".", "code"))
    elif sys.platform == 'linux' or sys.platform == 'linux2':
        sys.path.append(os.path.join(
            "/rds", "project", "cag1", "rds-cag1-general", "epidem-userspaces",
            "van25", "CODE", "idmf"))
        sys.path.append(os.path.join(".", "code"))
        matplotlib.use('Agg')
    else:
        assert False, sys.platform
elif machine_type == 'personal':
    if sys.platform == 'win32':
        # Path to models
        sys.path.append(os.path.join('D:/', 'PROJECTS', 'IDMF'))
        # Path to data
        sys.path.append(os.path.join('..'))
    elif sys.platform == 'linux' or sys.platform == 'linux2':
        # Path to models
        sys.path.append(os.path.join(
            "/home", "viet", "PROJECTS", "private", "idmf"))
        # Path to scripts
        sys.path.append(os.path.join(".", "code"))
        matplotlib.use('Agg')
    else:
        assert False, sys.platform
