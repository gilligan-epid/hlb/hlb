"""
Example parameter fitting using Texas survey data.

Make sure that you activate the 'idmf' virtual environment before running the
code. To activate:

module load rhel7/default-peta4
module load gcc-7.2.0-gcc-4.8.5-pqn7o2k
source /rds/project/cag1/rds-cag1-general/epidem-userspaces/van25/Envs/idmf_venv/bin/activate
"""

import context
import fit_hlb_only

# Region with survey data
region = 'TX'

# index of the configuration used (listed in hidalgo_configs)
cf_no = 1
# test run (defined in fit_slurm.py)
run_type = 'test'
# hyper-parameter for tuning: the proportion of latent time updates
param_wt = 0.01
# MCMC chain index
chain_no = 0
# number of simulations for validation
n_iter_sim, n_block_sim = 10, 2
n_samp = n_iter_sim * n_block_sim

"""
Estimate parameters using data from
"""
adir = '{}fit_hlb_only_pw{}_cf{}'.format(
    run_type, param_wt, cf_no)
fit_hlb_only.main(
    run_type, param_wt, chain_no, cf_no, region,
    condor_run=False, n_iter_sim=n_iter_sim, n_block_sim=n_block_sim,
    to_fit=True, adir=adir)
