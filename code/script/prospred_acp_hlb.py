import context
import data.const as const
import data.input.california as inp
import data.output.california as outp
import idm.model.struct as mstruct
import idm.model.SECD_SEI as mlib
import idm.simulation.struct as sstruct
import cv_configs
import cvreset_configs
from fit_condor import make_outdir

import sys
import numpy as np
import copy
import datetime
np.seterr(divide='ignore')

data_type = ['plant', 'acptrap']
obs_models = ['Noulli', 'Noulli']
n_dcomp = 3


def main(run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
         delta_ratio=1, eta=-1., condor_run=True, adir=None, beta_ratio=1,
         qtruncH=0, qradiusH=None, qtruncV=0, qradiusV=None,
         ttruncH=0, tradiusH=None, ttruncV=0, tradiusV=None,
         n_iter_sim=50, n_block_sim=2, adir_ps=None, with_retropred=0,
         with_movie=False, data_map=False, retro_suffix='', chunk_no=0,
         n_skipped_samp=0, D_removal=True, treatment_stop=True,
         param_cf_no=1, n_snap_year0=2020, n_gap_year_snap=1):

    """
    Predictive simulation
     1. Load parameter distribution
    """
    assert region == 'CentralValley' or region == 'CVreset'
    hc = cv_configs if region == 'CentralValley' else cvreset_configs
    cf = hc.configs['acp_hlb'][cf_no]
    init_source = 'data'
    #
    tstop, treset = None, None
    if treatment_stop and hc.acp_tchange is not None:
        tstop = hc.acp_tchange
        treset = tstop + 30 * 9
    #

    n_snaps = 1
    cropped = True
    tF = cf['tF']
    tFobs = cf['tFobs']
    snap_points = [tFobs] if tF > tFobs else []
    t_start = (
        datetime.datetime(n_snap_year0, 12, 20) - const.date0[region]).days\
        if n_snap_year0 is not None else tFobs + 1
    for t in np.arange(t_start, tF, 365 * n_gap_year_snap):
        snap_points += [t]
    param_list = ['alpha', 'mu', 'beta', 'epsilon', 'eta', 'varepsilon',
                  'drate', 'delta', 'sigma', 'dprob']
    #
    app_opts = inp.AppOptions(
        region=region, csize=hc.resol, mlib=mlib, data_type=data_type,
        n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
        obs_models=obs_models, data_tau=hc.data_tau, cscale=hc.cscale,
        with_weather=cf['with_weather'], vector_cov=cf['vector_cov'],
        latent_init_period=cf['latent_dt'], init_source=init_source,
        plant_thres=cf['plant_thres'], n_dcomp=n_dcomp, condor_run=condor_run,
        urban_fraction=cf['urban_fraction'], nw_f=cf['nw_f'], t0=cf['t0'],
        tFobs=cf['tFobs'], tF=cf['tF'], with_data=False, adir=adir,
        qparams=[[0, qtruncH], [0, qtruncV]], tparams=[[ttruncH], [ttruncV]],
    )
    snap_times, ignore = outp.make_snap_times(
        app_opts, n_snaps=n_snaps, snap_points=snap_points, cropped=cropped)
    print('snapshots:', snap_times)
    print app_opts.t0, app_opts.tFobs, app_opts.tF
    print tstop, treset
    print snap_points
    sim_tau = 1  # ?
    sim_opts = sstruct.SimOptions(
        outdir=app_opts.outdir, n_comp=mlib.n_comp, snap_times=snap_times,
        n_iter=n_iter_sim, n_block=n_block_sim, tau=sim_tau,
        primary_intros=cf['primary_intros'], with_single=True, n_dcomp=n_dcomp,
        draw_data=False, n_skipped_samp=n_skipped_samp,
    )
    if data_map:
        outp.make_data_maps(app_opts)
    #
    cprefix = 'c{}_'.format(chain_no) if param_region is not None else ''
    if param_region is not None:
        param_cf_no = cf_no if param_cf_no < 0 else param_cf_no
        app_opts.cprefix = cprefix
        app_opts.paramdir = make_outdir(
            run_type, 'hlb_acp', param_wt, param_cf_no, '{}_1.0km'.format(
                param_region))
        param_dist = sstruct.PopLearn.load_params(
            app_opts.paramdir, param_list, prefix=cprefix)
        # Adjust parameters
        param_dist['epsilon'] *= eps_ratio  # for HLB
        param_dist['delta'] *= delta_ratio
        param_dist['beta_H'] = param_dist.pop('beta') * beta_ratio
        param_dist['beta_V'] = param_dist['beta_H'] * param_dist['delta']
        param_dist['gamma'] = cf['gamma'] *\
            np.ones_like(param_dist['beta_H'])
        param_dist['gamma_V'] = cf['gamma_V'] *\
            np.ones_like(param_dist['beta_H'])
        param_dist['drate_H'] = 1. / (
            1. / param_dist['sigma'] + 1. / param_dist['drate'])
        param_dist['drate_V'] = cf['drate_V'] *\
            np.ones_like(param_dist['beta_H'])
        if eta >= 0:
            param_dist['eta'][:] = eta
        if region != 'TX' and region != 'Hidalgo':
            param_dist['varepsilon'] = param_dist['varepsilon'][:, 0]
        param_dist['dprob'] = param_dist['dprob'][:, 0]
        if not D_removal:
            param_dist['dprob'][:] = 1
    else:
        param_dist = {
            'epsilon': [cf['epsilon']], 'alpha': [cf['alpha']],
            'beta': [cf['beta']], 'varepsilon': [cf['varepsilon']],
            'gamma': [cf['gamma']], 'sigma': [cf['sigma']],
            'dprob': [cf['dprob']], 'eta': [cf['eta']], 'mu': [cf['mu']]}
    #
    smod_opts = mstruct.ModelOptions(
        param_dist=param_dist, param_wbins=cf['param_wbins'],
        dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
        eta_fn=cf['eta_fn'], knorm=cf['knorm'],
    )
    if with_retropred:
        # backward model
        smod_opts_copy = copy.deepcopy(smod_opts)
        app_opts.tF = cf['tFobs']
        postmodel = inp.load_popmodel(
            app_opts, smod_opts, True, 'data', traj_update=False)
        # forward model
        app_opts.t0 = cf['tFobs']
        app_opts.tF = cf['tF']
        app_opts.init_dt = 0
        priormodel = inp.load_popmodel(
            app_opts, smod_opts_copy, False,
            tradius=[tradiusH, tradiusV], qradius=[qradiusH, qradiusV],
            tstop=tstop, treset=treset)
        # preparation of retropred data
        posts_dir_acp = make_outdir(
            run_type, 'acptrap', param_wt, cf_no, '{}_1.0km'.format(
                region), hc=hc) + '_retropred_acp{}'.format(param_region)
        posts_dir_acp += '_br{}{}'.format(1 - 1.1 * ttruncV, retro_suffix)\
            if ttruncV > 0 else retro_suffix
        posts_dir_hlb = make_outdir(
            run_type, 'acp_hlb', param_wt, cf_no, '{}_1.0km'.format(
                region), hc=hc) + '_retropred_acp_hlb{}'.format(param_region)
        posts_dir_hlb += '_ttruncV{}{}'.format(ttruncV, retro_suffix)\
            if ttruncV > 0 else retro_suffix
        outp.make_postsim_file(
            postmodel, app_opts, sim_opts, posts_dir_hlb, posts_dir_acp)
        # joint model
        psim = sstruct.PopSimRetro(
            postmodel, priormodel, sim_opts, posts_dir=posts_dir_hlb)
        app_opts.t0 = cf['t0']
        app_opts.init_dt = cf['latent_dt']
    else:
        smodel = inp.load_popmodel(
            app_opts, smod_opts, False, qradius=[qradiusH, qradiusV],
            tradius=[tradiusH, tradiusV], treset=treset, tstop=tstop)
        #
        outp.write_config_file(app_opts, sim_opts, 'sim')
        psim = sstruct.PopSim(smodel, sim_opts)
    #
    # print app_opts.outdir
    # assert False
    psim.run(prefix=cprefix)
    outp.make_sim_maps(
        app_opts, prim_intros=cf['primary_intros'][1], n_snaps=n_snaps,
        snap_points=snap_points, corrgram=False, cropped=cropped,
        yr0=2020, n_yr=15, n_step_yr=5)

if __name__ == '__main__':
    args = list(sys.argv)
    if len(args) != 26:
        print len(args)
        for i in xrange(len(args)):
            print i, args[i]
        print "Usage: python prospred_acp_hlb.py ",
        print "<run_type> <param_wt> <chain_no> <cf_no> <eps_ratio>",
        print "<region> <param_region> <delta_ratio> <eta> <beta_ratio>",
        print "<qtruncV> <qradiusV> <ttruncV> <tradiusV>",
        print "<qtruncH> <qradiusH> <ttruncH> <tradiusH>",
        print "<with_retropred> <retro_suffix> <n_iter_sim> <n_block_sim>",
        print "<n_skipped_samp> <chunk_no> <param_cf_no>"
    else:
        main(args[1], args[2], int(args[3]), int(args[4]), float(args[5]),
             args[6], args[7], float(args[8]), eta=float(args[9]),
             beta_ratio=float(args[10]),
             qtruncV=float(args[11]), qradiusV=float(args[12]),
             ttruncV=float(args[13]), tradiusV=float(args[14]),
             qtruncH=float(args[15]), qradiusH=float(args[16]),
             ttruncH=float(args[17]), tradiusH=float(args[18]),
             with_retropred=int(args[19]), retro_suffix=args[20],
             n_iter_sim=int(args[21]), n_block_sim=int(args[22]),
             n_skipped_samp=int(args[23]), chunk_no=int(args[24]),
             param_cf_no=int(args[25]))
