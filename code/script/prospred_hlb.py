import context
import data.const as const
import data.input.texas
import data.output.texas
import data.input.california
import data.output.california
import data.input.florida
import data.output.florida
import idm.model.struct as mstruct
import idm.model.SECD as SECD
import idm.model.SECI as SECI
import idm.simulation.struct as sstruct
import hidalgo_configs
import casouth_configs
import florida_configs
from fit_slurm import make_outdir

import sys
import numpy as np
import copy
np.seterr(divide='ignore')


def main(run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
         exp_str='hlb_only', eta=-1., condor_run=True, adir=None, beta_ratio=1,
         qtrunc=0, qradius=None, ttrunc=0, tradius=None, adir_ps=None,
         with_retropred=0, n_iter_sim=50, n_block_sim=2,
         n_skipped_samp=0, data_map=False, param_cf_no=1,
         with_movie=False, extend_run=True,
         retro_suffix='', chunk_no=0):

    """
    Predictive simulation
     1. Load parameter distribution
    """
    data_type = ['plant']
    obs_models = ['Noulli']
    n_dcomp = 0
    #
    # region-specific settings
    #
    with_corrgram = False
    init_source = 'data'
    cropped = True
    snap_dt = None
    n_snaps = 1
    D_removal = False
    with_spreadmap = with_retropred
    if region == 'TX' or region == 'Hidalgo':
        inp = data.input.texas
        outp = data.output.texas
        hc = hidalgo_configs
        with_corrgram = True
        with_weather = False
        with_spreadmap = True
        D_removal = False
    elif region == 'CAsouth':
        inp = data.input.california
        outp = data.output.california
        hc = casouth_configs
        cropped = False
        D_removal = True
        with_corrgram = not extend_run
        with_weather = True
    elif region == 'FL' or region == 'FLsouth':
        inp = data.input.florida
        outp = data.output.florida
        hc = florida_configs
        cropped = False
        data_type = ['hlb']
        with_weather = False
        D_removal = True
        with_spreadmap = True
    #
    cf = hc.configs['hlb_only'][cf_no]
    latent_dt = cf['latent_dt']
    if region == 'TX':
        init_source = 'infer'
        latent_dt = 0
    #
    cf0 = hc.configs['hlb_only'][0]
    if extend_run and cf_no != 0 and 'FL' not in region:
        tF = cf0['tF']
        tFobs = cf['tFobs']
        snap_points = [cf['tFobs'], cf['tFobs'] + 365, cf0['tFobs'], tF - 366]
        if region == 'TX':
            snap_points = [hc.tFobs_estimation] + snap_points
    else:
        tF = cf['tF']
        tFobs = cf['tFobs']
        snap_points = [tFobs] if tF > tFobs else []
    if extend_run and 'FL' in region:
        for t in np.arange(snap_points[-1], tF, 365):
            snap_points += [t]
    #
    # Create options
    #
    cprefix = 'c{}_'.format(chain_no) if param_region is not None else ''
    app_opts = inp.AppOptions(
        region=region, csize=hc.resol, mlib=SECD, data_type=data_type,
        n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
        obs_models=obs_models, data_tau=hc.data_tau, cscale=hc.cscale,
        with_weather=with_weather, vector_cov=cf['vector_cov'],
        latent_init_period=latent_dt, init_source=init_source,
        plant_thres=cf['plant_thres'], n_dcomp=n_dcomp, condor_run=condor_run,
        urban_fraction=cf['urban_fraction'], nw_f=cf['nw_f'],
        tFobs=tFobs, tF=tF, with_data=False, adir=adir,
        qparams=[0, qtrunc], tparams=[ttrunc], t0=cf['t0'], cprefix=cprefix,
    )
    if data_map:
        outp.make_data_maps(app_opts, cropped=cropped)
        assert False
    #
    # Load parameter estimates
    #
    param_list = ['alpha', 'mu', 'beta', 'epsilon', 'eta', 'varepsilon',
                  'drate', 'sigma', 'dprob']
    if param_region is not None:
        param_cf_no = cf_no if param_cf_no < 0 else param_cf_no
        # from posterior estimates
        app_opts.paramdir = make_outdir(
            run_type, exp_str, param_wt, param_cf_no, '{}_1.0km'.format(
                param_region), with_weather=with_weather)
        param_dist = sstruct.PopLearn.load_params(
            app_opts.paramdir, param_list, prefix=cprefix)
        # adjust parameters
        param_dist['gamma'] = np.ones_like(param_dist['beta']) * cf['gamma']
        param_dist['epsilon'] = param_dist['epsilon'] * eps_ratio
        param_dist['beta'] = param_dist['beta'] * beta_ratio
        if eta >= 0:
            print "eta", eta
            param_dist['eta'][:] = eta
        if region != 'TX' and region != 'Hidalgo':
            param_dist['varepsilon'] = param_dist['varepsilon'][:, 0]
        # set varepsilon_W to 0
        if 'FL' in region:
            param_dist['varepsilon'] = param_dist['varepsilon'] * 0
            param_dist['epsilon'] = param_dist['epsilon'] * 1
        # merge I to D
        if with_retropred == 2:
            param_dist_orig = copy.deepcopy(param_dist)
            dp_shp = [param_dist_orig['dprob'].shape[0]] +\
                list(cf['dprob'].shape)
            param_dist_orig['dprob'] = np.reshape(
                param_dist_orig['dprob'], dp_shp)
        else:
            param_dist['drate'] = 1. / (
                1. / param_dist['sigma'] + 1. / param_dist['drate'])
            if not D_removal:
                param_dist['dprob'][:] = 1
    else:
        # from initial values
        param_dist = {
            'epsilon': [cf['epsilon']], 'alpha': [cf['alpha']],
            'beta': [cf['beta']], 'varepsilon': [cf['varepsilon']],
            'gamma': [cf['gamma']], 'sigma': [cf['sigma']],
            'dprob': [cf['dprob']], 'eta': [cf['eta']], 'mu': [cf['mu']]}
    #
    # Set simulation configurations
    #

    # assert False
    sim_tau = 1  # ?
    sim_opts = sstruct.SimOptions(
        outdir=app_opts.outdir, n_comp=SECD.n_comp,
        n_iter=n_iter_sim, n_block=n_block_sim, n_skipped_samp=n_skipped_samp,
        primary_intros=cf['primary_intros'], with_single=False, n_dcomp=n_dcomp,
        draw_data=False, det_by_data=with_retropred, tau=sim_tau,
        chunk_no=chunk_no,
    )
    smod_opts = mstruct.ModelOptions(
        param_dist=param_dist, param_wbins=cf['param_wbins'],
        dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
        eta_fn=cf['eta_fn'], knorm=cf['knorm'],
    )
    smod_opts_copy = copy.deepcopy(smod_opts)
    if with_retropred == 2:
        smod_opts.param_dist = param_dist_orig

    # snap_points = [1752, 1997, 2362, 2727, 3092, 3458, 3823, 4188]#you can manually set snap times here if you want to specify which snapshots to store (lawrence)

    snap_times, ignore = outp.make_snap_times(
        app_opts, snap_dt=snap_dt, n_snaps=n_snaps,
        snap_points=snap_points, cropped=cropped)

    print "extend_run", extend_run
    print('snapshots:', snap_times)
    print app_opts.t0, app_opts.tFobs, app_opts.tF
    sim_opts.snap_times = snap_times
    #
    print 'with_retropred', with_retropred
    if with_retropred == 1:
        # backward model
        app_opts.tF = tFobs
        postmodel = inp.load_popmodel(
            app_opts, smod_opts, True, 'data', traj_update=False)
        # forward model
        app_opts.t0 = tFobs
        app_opts.tF = tF
        app_opts.init_dt = 0
        priormodel = inp.load_popmodel(
            app_opts, smod_opts_copy, False, qradius=qradius, tradius=tradius)
        posts_dir = make_outdir(
            run_type, exp_str, param_wt, cf_no, '{}_1.0km'.format(
                region), eps_ratio, hc=hc) + '_retropred_{}{}'.format(
                    exp_str, param_region)
        posts_dir += '_br{}{}'.format(1 - 1.1 * ttrunc, retro_suffix)\
            if ttrunc > 0 else retro_suffix
        print "posts_dir", posts_dir
        psim = sstruct.PopSimRetro(
            postmodel, priormodel, sim_opts, posts_dir=posts_dir)
        app_opts.t0 = cf['t0']
        app_opts.init_dt = latent_dt
    elif with_retropred == 2:
        # pre-obs model (SECI with data)
        app_opts.mlib = SECI
        app_opts.tF = tFobs
        app_opts.with_data = True
        premodel = inp.load_popmodel(app_opts, smod_opts, False)
        # post-obs model
        app_opts.mlib = SECD
        app_opts.t0 = tFobs
        app_opts.tF = tF
        app_opts.init_dt = 0
        app_opts.with_data = False
        sufmodel = inp.load_popmodel(app_opts, smod_opts_copy, False,
                                     qradius=qradius, tradius=tradius)
        #
        sim_opts.draw_data = [True, False]
        sim_opts.n_comp = [SECI.n_comp, SECD.n_comp]
        psim = sstruct.PopSimMixed(premodel, sufmodel, sim_opts)
        app_opts.t0 = cf['t0']
        app_opts.init_dt = latent_dt
    else:
        app_opts.t0 = tFobs if region != 'TX' else cf['t0']
        app_opts.tFobs = tFobs + .5
        app_opts.tF = tF
        if 'FL' not in app_opts.region:
            app_opts.init_dt = 0
        #
        smodel = inp.load_popmodel(
            app_opts, smod_opts, False, qradius=qradius, tradius=tradius)
        #
        outp.write_config_file(app_opts, sim_opts, 'sim')
        psim = sstruct.PopSim(smodel, sim_opts)
        app_opts.t0 = cf['t0']
        app_opts.init_dt = latent_dt
    #
    # Run simulations
    #
    print("into run")
    psim.run(prefix=cprefix)
    print("outof run")
    outp.make_sim_maps(
        app_opts, prim_intros=cf['primary_intros'][1], snap_dt=snap_dt,
        n_snaps=n_snaps, with_spreadmap=with_spreadmap,
        snap_points=snap_points, corrgram=with_corrgram, cropped=cropped,
        dpc_wtd=True
    )
    print("out of plot")
    # with latent
    if not with_retropred and region == 'TX' and False:
        app_opts.init_dt = cf['latent_dt']
        smodel = inp.load_popmodel(app_opts, smod_opts_copy, False)
        app_opts.cprefix = cprefix + 'with_latent_'
        psim = sstruct.PopSim(smodel, sim_opts)
        psim.run(prefix=app_opts.cprefix)
        outp.make_sim_maps(
            app_opts, prim_intros=cf['primary_intros'][1], n_snaps=n_snaps,
            snap_points=snap_points, snap_dt=snap_dt,
            with_spreadmap=with_spreadmap, dpc_wtd=True)
    if with_movie and run_type == 'long':
        app_opts.dtau = ['Week', 7]
        outp.make_sim_movies(app_opts, hobs=True)


if __name__ == '__main__':
    args = list(sys.argv)
    if len(args) != 22:
        print len(args)
        for i in xrange(len(args)):
            print i, args[i]
        print "Usage: python prospred_hlb.py ",
        print "<run_type> <param_wt> <chain_no> <cf_no> <eps_ratio>",
        print "<region> <param_region> <exp_str> <eta> <beta_ratio>",
        print "<qtrunc> <qradius> <ttrunc> <tradius>",
        print "<with_retropred> <retro_suffix> <n_iter_sim>",
        print "<n_block_sim> <n_skipped_samp> <chunk_no>",
        print "<param_cf_no>"
    else:
        main(args[1], args[2], int(args[3]), int(args[4]), float(args[5]),
             args[6], args[7], args[8], eta=float(args[9]),
             beta_ratio=float(args[10]), qtrunc=float(args[11]),
             qradius=float(args[12]), ttrunc=float(args[13]),
             tradius=float(args[14]), with_retropred=int(args[15]),
             retro_suffix=args[16], n_iter_sim=int(args[17]),
             n_block_sim=int(args[18]), n_skipped_samp=int(args[19]),
             chunk_no=int(args[20]), param_cf_no=int(args[21]))
