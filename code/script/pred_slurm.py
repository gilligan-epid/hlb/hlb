import os
import context
import fit_slurm as fc
import hidalgo_configs
import casouth_configs
import cv_configs
import florida_configs
import numpy as np

acp_deathrate = 1. / 90


def make_wdir(rtype, k, par_wt, er, eta, br, q, qrad, tr, trad,
              qV, qradV, trV, tradV, dname, param_exp_str, hc, with_weather,
              pred_type, par_region, dir_suffix):
    " Output directory "
    wdir = fc.make_outdir(
        rtype, param_exp_str, par_wt, k, dname, er, hc=hc,
        with_weather=with_weather)
    wdir += '_{}{}'.format(pred_type, par_region)
    if eta >= 0:
        wdir += '_eta{}'.format(eta)
    if br != 1:
        wdir += '_br{}'.format(br)
    if q > 0:
        wdir += '_qtrunc{}'.format(q)
    if qrad != 0:
        wdir += '_qrad{}'.format(qrad)
    if tr > 0:
        wdir += '_ttrunc{}'.format(tr)
    if trad != 0:
        wdir += '_trad{}'.format(trad)
    if qV > 0:
        wdir += '_qtruncV{}'.format(qV)
    if qradV != 0:
        wdir += '_qradV{}'.format(qradV)
    if trV > 0:
        wdir += '_ttruncV{}'.format(trV)
    if tradV != 0:
        wdir += '_tradV{}'.format(tradV)
    wdir += dir_suffix
    return wdir


def make_wargs(rtype, k, par_wt, er, eta, br, q, qrad, tr, trad,
               qV, qradV, trV, tradV, ci, pred_type, region, par_region,
               delta_ratio, param_cf_no, n_iter_sim, n_block_sim,
               n_skipped_samp, chunk_no, param_exp_str, with_retropred,
               dir_suffix):
    " Arguments "
    wargs = '{}.py {} {} {} {} {} {} {} '.format(
        pred_type, rtype, par_wt, ci, k, er, region, par_region)
    if pred_type == 'prospred_hlb' or 'only' in pred_type:
        wargs += param_exp_str
    else:
        wargs += str(delta_ratio)
    wargs += ' {} {}'.format(eta, br)
    if 'prospred' in pred_type:
        wargs += ' {} {} {} {}'.format(q, qrad, tr, trad)
        if pred_type == 'prospred_acp_hlb':
            wargs += ' {} {} {} {}'.format(qV, qradV, trV, tradV)
        wargs += ' {} {} {} {} {}'.format(
            with_retropred, dir_suffix, n_iter_sim, n_block_sim,
            n_skipped_samp)
    else:
        wargs += ' {} {}'.format(n_iter_sim, n_block_sim)
    wargs += ' {} {}'.format(chunk_no, param_cf_no)
    return wargs


def make_slurm_submit_file(
        run_types, param_wts, n_chains, config_nos, region, par_region,
        pred_type, param_exp_str, delta_ratio=1, with_retropred=0,
        eps_ratios=[1], eta_choices=[-1.], beta_ratios=[1], n_iter_sim=100,
        n_block_sim=2, qtruncs=[0], qrads=[0.], ttruncs=[0], trads=[0.],
        qtruncsV=[0], qradsV=[0.], ttruncsV=[0], tradsV=[0.],
        slevel=2, ltime='01:00:00', mblock=1, dir_suffix='', chunk_no=0,
        n_skipped_samp=0, with_weather=None, param_cf_no=0):
    hc = hidalgo_configs if region == 'TX' else\
        casouth_configs if region == 'CAsouth' else\
        cv_configs if region == 'CentralValley' else\
        florida_configs if 'FL' in region else None
    dname = '{}_1.0km'.format(region)
    jname = '{}_{}_by_{}'.format(region, pred_type, param_exp_str)
    n_jobs = len(run_types) * len(config_nos) * len(param_wts) *\
        len(eps_ratios) * len(eta_choices) * len(beta_ratios) *\
        len(qtruncs) * len(qrads) * len(ttruncs) * len(trads) *\
        len(qtruncsV) * len(qradsV) * len(ttruncsV) * len(tradsV) * n_chains
    #
    with open(os.path.join(
            fc.code_dir, 'slurm', '{}.submit'.format(jname)), 'w') as f:
        fc.write_base(f, slevel, jname, ltime, n_jobs, mblock)
        j = 0
        for rtype in run_types:
            for k in config_nos:
                for par_wt in param_wts:
                    for er in eps_ratios:
                        for eta in eta_choices:
                            for br in beta_ratios:
                                for q in qtruncs:
                                    for qrad in qrads:
                                        for tr in ttruncs:
                                            for trad in trads:
                                                for qV in qtruncsV:
                                                    for qradV in qradsV:
                                                        for trV in ttruncsV:
                                                            for tradV in tradsV:
                                                                wdir = make_wdir(
                                                                    rtype, k, par_wt, er, eta,
                                                                    br, q, qrad, tr, trad,
                                                                    qV, qradV, trV, tradV,
                                                                    dname, param_exp_str, hc,
                                                                    with_weather, pred_type,
                                                                    par_region, dir_suffix)
                                                                for ci in range(n_chains):
                                                                    wargs = make_wargs(
                                                                        rtype, k, par_wt, er,
                                                                        eta, br, q, qrad, tr,
                                                                        trad, qV, qradV, trV,
                                                                        tradV, ci, pred_type,
                                                                        region, par_region,
                                                                        delta_ratio, param_cf_no,
                                                                        n_iter_sim, n_block_sim,
                                                                        n_skipped_samp, chunk_no,
                                                                        param_exp_str, with_retropred, dir_suffix)
                                                                    fc.write_jcase(
                                                                        j, wdir, wargs, f, n_jobs)
                                                                    j += 1
        #
        f.write('\n')
        f.write('echo $output_dir\n')
        f.write('mkdir -p $output_dir\n')
        f.write('cd $output_dir\n')
        f.write('cp -rf {}/hlb/code .\n'.format(fc.code_dir))
        f.write('python $python_arguments\n')


if __name__ == '__main__':
    run_types, param_wts = ['long'], [.01]
    # Texas
    param_cf_no = 0
    config_nos = [1]
    param_region = 'TX'
    region = 'TX'
    pred_types = ['prospred_hlb']  # ['pred_hlb_only', 'prospred_hlb']
    exp_strs = ['hlb_only']
    eta_choices = [-1]  # [.2, -1]
    with_retropred, dir_suffix = 0, '_updated_wtd_pcf{}_extend_run2'.format(
        param_cf_no)
    # with_retropred, dir_suffix = 2, '_mixed' -- doesn't work
    for pred_type in pred_types:
        for param_exp_str in exp_strs:
            make_slurm_submit_file(
                run_types, param_wts, fc.n_chains, config_nos, region,
                param_region, pred_type, param_exp_str, n_iter_sim=100,
                eta_choices=eta_choices, slevel=2, ltime='12:00:00',
                with_weather=False, dir_suffix=dir_suffix,
                with_retropred=with_retropred, n_block_sim=5,
                param_cf_no=param_cf_no)
    """
    make_slurm_submit_file(
        run_types, param_wts, fc.n_chains, config_nos, region, param_region,
        'retropred_hlb_only', param_exp_str, n_iter_sim=10, ltime='02:00:00')
    """
    # Southern CA - HLB
    region = 'CAsouth'
    n_chains = 3
    param_cf_no = 1
    ttruncs, trads = [0.], [0.]
    "Retrospective simulation"
    config_nos = [1]  # [1, 2, 3, 4]
    n_iter_sim, n_block_sim, chunk_no = 10, 1, 0
    dir_suffix = '_s{}_cf{}param_test'.format(int(
        n_iter_sim * n_block_sim * (chunk_no + 1)), param_cf_no)
    make_slurm_submit_file(
        run_types, param_wts, n_chains, config_nos, region,
        param_region, 'retropred_hlb_only', 'hlb_only', n_iter_sim=n_iter_sim,
        n_block_sim=n_block_sim, slevel=2, ltime='36:00:00',
        dir_suffix=dir_suffix, chunk_no=chunk_no, param_cf_no=param_cf_no,
        beta_ratios=[1 - 1.1 * x for x in ttruncs])
    "Forward predictions with control"
    config_nos = [1]
    qtruncs, qrads = [0.2], [8]  # [0.1], [8]
    with_retropred, n_skipped_samp = 1, n_iter_sim * n_block_sim
    chunk_no = 0
    dir_suffix = '_s480_cf1param'
    # Different radiuses of HLB quarantines (with tree removal)
    # crads = list(np.arange(1, 8)) + list(np.arange(9, 15))
    make_slurm_submit_file(
        run_types, param_wts, n_chains, config_nos, region, param_region,
        'prospred_hlb', 'hlb_only', dir_suffix=dir_suffix,
        with_retropred=with_retropred, param_cf_no=param_cf_no,
        qtruncs=qtruncs, qrads=qrads, ttruncs=ttruncs, trads=trads,
        n_iter_sim=n_iter_sim, n_block_sim=n_block_sim / 2, chunk_no=chunk_no,
        n_skipped_samp=n_skipped_samp, slevel=2, ltime='36:00:00',
    )  # skip the first chunk of retropred samples
    # Without tree removal upon detection - DONE

    # Florida
    region = 'FL'
    n_chains = 3
    config_nos = [0]
    eta_choices = [.5, .8]
    chunk_no = 0
    dir_suffix = '_s50_chunk{}'.format(chunk_no)
    make_slurm_submit_file(
        run_types, param_wts, n_chains, config_nos, region,
        param_region, 'prospred_hlb', 'hlb_only', n_iter_sim=2,
        n_block_sim=25, slevel=2, ltime='36:00:00', mblock=3,
        eta_choices=eta_choices, chunk_no=chunk_no, dir_suffix=dir_suffix)

    # Central Valley - ACP
    region = 'CentralValley'
    n_chains = 1
    ttruncs, trads = [0.85, 0.9, 0.8], [0.4]  # [0, 0.4]
    "Retrospective simulation for ACP"
    config_nos = [2]
    n_iter_sim, n_block_sim, chunk_no = 100, 5, 0
    dir_suffix = '_s{}'.format(int(n_iter_sim * n_block_sim * (chunk_no + 1)))
    make_slurm_submit_file(
        run_types, param_wts, n_chains, config_nos, region,
        param_region, 'retropred_acp', 'acptrap', n_iter_sim=n_iter_sim,
        n_block_sim=n_block_sim, slevel=2, ltime='36:00:00',
        dir_suffix=dir_suffix, chunk_no=chunk_no,
        beta_ratios=[1 - 1.1 * x for x in ttruncs])
    "Prospective simulation for ACP with control"
    n_iter_sim, n_block_sim, n_skipped_samp = 100, 4, 100
    qtruncs, qrads = [0.1], [8]  # [0, 8]
    chunk_no = 0
    dir_suffix = '_s500_cf{}param'.format(param_cf_no)
    make_slurm_submit_file(
        run_types, param_wts, n_chains, config_nos, region, param_region,
        'prospred_acp', 'acptrap', dir_suffix=dir_suffix, with_retropred=1,
        qtruncs=qtruncs, qrads=qrads, ttruncs=ttruncs, trads=trads,
        n_iter_sim=n_iter_sim, n_block_sim=n_block_sim / 2, chunk_no=chunk_no,
        n_skipped_samp=n_skipped_samp, slevel=2, ltime='36:00:00',
        param_cf_no=param_cf_no,
    )  # skip the first chunk of retropred samples
    "Prospective simulation for HLB with control"
    config_nos = [1, 2]
    qtruncsV, qradsV = [0.1], [8]
    ttruncsV, tradsV = [0.8], [0.4]
    qtruncsH, qradsH = [0], [0]
    ttruncsH, tradsH = [0], [0]
    make_slurm_submit_file(
        run_types, param_wts, n_chains, config_nos, region, param_region,
        'prospred_acp_hlb', 'acp_hlb', dir_suffix=dir_suffix, with_retropred=1,
        qtruncs=qtruncsH, qrads=qradsH, ttruncs=ttruncsH, trads=tradsH,
        qtruncsV=qtruncsV, qradsV=qradsV, ttruncsV=ttruncsV, tradsV=tradsV,
        n_iter_sim=n_iter_sim, n_block_sim=n_block_sim / 2, chunk_no=chunk_no,
        n_skipped_samp=n_skipped_samp, slevel=2, ltime='36:00:00',
        param_cf_no=param_cf_no,
    )
    #
    # NOT NECESSARY
    """
    # Southern CA - ACP
    param_exp_str = 'acptrap'
    n_chains = 1
    #
    make_slurm_submit_file(
        run_types, param_wts, n_chains, [1], region,
        param_region, 'retropred_acp', param_exp_str, eta_choices=[-1],
        delta_ratio=casouth_configs.acp_deathrate, n_iter_sim=50,
        slevel=3, ltime='12:00:00')
    make_slurm_submit_file(
        run_types, param_wts, n_chains, [1, 2], region,
        param_region, 'prospred_acp', param_exp_str,
        delta_ratio=casouth_configs.acp_deathrate,
        ctruncs=[0.5, 0.8], crads=[0.4],
        n_iter_sim=50, slevel=3, ltime='06:00:00', with_retropred=1)"""
