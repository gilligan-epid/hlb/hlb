import context
import data.input.texas
import data.output.texas
import data.input.california
import data.output.california
import idm.model.struct as mstruct
import idm.model.SECI_WWc as mlib
import idm.simulation.struct as sstruct
import hidalgo_configs
import casouth_configs
import fit_slurm as hcd

import sys
import numpy as np
import copy
np.seterr(divide='ignore')

data_type = ['plant', 'psyllid']
obs_models = ['Noulli', 'Noulli']
n_dcomp = 2


def main(run_type, param_wt, chain_no, cf_no, region, to_fit=True,
         condor_run=True, adir=None, data_map=False, fit_all=False,
         n_iter_sim=100, n_block_sim=2, chunk_no=0):

    """
    Parameter fitting
     1. Write configurations to file
     2. Construct learn models
     3. Run MCMC
     4. Make parameter plots

    """
    if region == 'TX' or region == 'Hidalgo':
        inp = data.input.texas
        outp = data.output.texas
        hc = hidalgo_configs
    elif region == 'CAsouth':
        inp = data.input.california
        outp = data.output.california
        hc = casouth_configs
    else:
        assert False, 'not implemented yet'
    #
    cf = hc.configs['hlb_acp'][cf_no]
    cprefix = 'c{}_'.format(chain_no)
    param_list = [
        'alpha', 'mu', 'beta', 'epsilon', 'dprob', 'eta', 'varepsilon',
        'delta', 'sigma', 'drate']
    param_wbins = cf['param_wbins']
    param_wbins['xi'] = True
    xi = cf['gamma']
    #
    if fit_all and (region == 'TX' or region == 'Hidalgo'):
        epsilon_update, beta_update, alpha_update = True, True, True
        eta_update, delta_update, xi_update = True, True, True
        dprob_update, drate_update, sigma_update = True, True, True
        varepsilon_update = cf['varepsilon_update']
        node_update = True
    elif not fit_all:
        epsilon_update, beta_update, alpha_update = False, False, False
        eta_update, delta_update, xi_update = False, True, False
        dprob_update, drate_update, sigma_update = True, False, False
        varepsilon_update = False
        node_update = True
    else:
        epsilon_update, beta_update, alpha_update = True, False, False
        eta_update, delta_update, xi_update = True, False, False
        dprob_update, drate_update, sigma_update = False, False, False
        varepsilon_update = False
        node_update = True
    tune = {'alpha': 0.2, 'epsilon': 0.5, 'dprob': 0.1, 'eta': 0.1,
            'varepsilon': 0.5, 'delta': 0.5, 'mu': 0.2, 'xi': 0.2}
    fsize_known = False
    #
    app_opts = inp.AppOptions(
        region=region, csize=hc.resol, mlib=mlib, data_type=data_type,
        n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
        obs_models=obs_models, data_tau=hc.data_tau, cscale=hc.cscale,
        with_weather=cf['with_weather'], vector_cov=cf['vector_cov'],
        latent_init_period=cf['latent_dt'], init_source='data',
        plant_thres=cf['plant_thres'], n_dcomp=n_dcomp, condor_run=condor_run,
        cprefix=cprefix, urban_fraction=cf['urban_fraction'], nw_f=cf['nw_f'],
        tF=cf['tFobs'], adir=adir, psyllid_thres=cf['psyllid_thres'],
    )
    if to_fit:
        n_iter_learn, n_block_learn, n_burnin = hcd.mcmc_iters[run_type]
        if data_map:
            outp.make_data_maps(app_opts)
            assert False
        param0 = {
            'epsilon': cf['epsilon'], 'alpha': cf['alpha'], 'beta': cf['beta'],
            'gamma': cf['gamma'], 'sigma': cf['sigma'], 'dprob': cf['dprob'],
            'eta': cf['eta'], 'varepsilon': cf['varepsilon'], 'mu': cf['mu'],
            'delta': cf['delta'], 'xi': xi, 'drate': -1}
        param_dist = {}
        if region != 'TX':
            paramdir = hcd.make_outdir(
                run_type, 'hlb_acp', param_wt, 0, 'TX_1.0km')
            param_dist = sstruct.PopLearn.load_params(
                paramdir, param_list, prefix=cprefix)
            dp_shp = [param_dist['dprob'].shape[0]] + list(cf['dprob'].shape)
            param_dist['dprob'] = np.reshape(param_dist['dprob'], dp_shp)
            param_dist['varepsilon'] = param_dist['varepsilon'][:, 0]
            param_dist.pop('gamma', None)
            param_dist.pop('eta', None)
            param_dist.pop('epsilon', None)
        elif not fit_all:
            paramdir = hcd.make_outdir(
                'long', 'hlb_only', param_wt, cf_no, 'TX_1.0km')
            print "paramdir", paramdir
            param_dist = sstruct.PopLearn.load_params(
                paramdir, param_list, prefix=cprefix)
            samps = param_dist['varepsilon']
            n_reps = 93 if cf_no == 1 else 119
            param_dist['varepsilon'] = np.tile(np.expand_dims(samps, axis=2),
                                               (1, 1, n_reps))
            param_dist.pop('dprob', None)
            param_dist.pop('gamma', None)
        #
        lmod_opts = mstruct.ModelOptions(
            param0=param0, param_wbins=param_wbins,
            dkernel=cf['dkernel'], knorm=cf['knorm'], param_dist=param_dist,
            freq_depend=cf['freq_depend'], fsize_known=fsize_known,
            epsilon_update=epsilon_update, beta_update=beta_update,
            alpha_update=alpha_update, node_update=node_update, tune=tune,
            n_burnin=n_burnin, param_wt=param_wt, drate_update=drate_update,
            dprob_update=dprob_update, eta_update=eta_update,
            eta_fn=cf['eta_fn'], varepsilon_update=varepsilon_update,
            delta_update=delta_update, sigma_update=sigma_update,
            xi_update=xi_update,
        )
        # outp.write_config_file(app_opts, lmod_opts, 'fit')
        lmodel = inp.load_popmodel(
            app_opts, lmod_opts, True, 'data', traj_update=True,
            from_laststate=(chunk_no > 0))
        #
        learn_opts = sstruct.SimOptions(
            outdir=app_opts.outdir, n_comp=mlib.n_comp,
            n_iter=n_iter_learn, n_block=n_block_learn, tau=1,
            n_dcomp=n_dcomp, chunk_no=chunk_no,
        )
        plearn = sstruct.PopLearn(lmodel, learn_opts)
        trajs = []
        for dtype in app_opts.dtypes:
            ignore, traj = inp.load_data(app_opts, dtype, with_traj=True)
            trajs += [traj]
        plearn.run(prefix=cprefix, param_list=param_list,
                   dp_len=len(cf['dprob'].flatten()), dtrajs=trajs)
    """
    Predictive simulation
     1. Load parameter distribution
    """
    #
    app_opts.paramdir = app_opts.outdir if to_fit else hcd.make_outdir(
        run_type, 'hlb_acp', param_wt, cf_no, '{}_1.0km'.format(region)
    ) if adir is None else app_opts.outdir
    param_dist = sstruct.PopLearn.load_params(
        app_opts.paramdir, param_list, prefix=cprefix)
    param_dist['gamma'] = np.ones_like(param_dist['beta']) * cf['gamma']
    param_dist['xi'] = np.ones_like(param_dist['beta']) * xi
    dp_shp = [param_dist['dprob'].shape[0]] + list(cf['dprob'].shape)
    param_dist['dprob'] = np.reshape(param_dist['dprob'], dp_shp)
    param_dist_plot = {
        k: param_dist[k] for k in param_list}
    param_dist_plot['pi'] = param_dist_plot.pop('dprob')
    param_dist_plot['xi'] = param_dist_plot.pop('drate')
    outp.make_param_plots(
        app_opts, param_dist_plot, cf['freq_depend'],
        trace=True, init_map=True)
    #
    smod_opts = mstruct.ModelOptions(
        param_dist=param_dist, param_wbins=param_wbins,
        dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
        eta_fn=cf['eta_fn'], knorm=cf['knorm'],
    )
    smod_opts_copy = copy.deepcopy(smod_opts)
    #
    app_opts.init_dt = 0
    app_opts.init_source = 'infer'
    app_opts.with_data = True
    sim_tau = 1  # ?
    sim_opts = sstruct.SimOptions(
        outdir=app_opts.outdir, n_comp=mlib.n_comp,
        n_iter=n_iter_sim, n_block=n_block_sim, tau=sim_tau,
        primary_intros=cf['primary_intros'], with_single=True,
        n_dcomp=n_dcomp, draw_data=True,
    )
    # outp.write_config_file(app_opts, sim_opts, 'sim')
    #
    smodel = inp.load_popmodel(app_opts, smod_opts, False)
    psim = sstruct.PopSim(smodel, sim_opts)
    psim.run(prefix=cprefix)
    outp.make_sim_maps(app_opts, corrgram=True, chunk_no=chunk_no)
    outp.make_roc_plots(app_opts, chunk_no=chunk_no)
    if run_type == 'long':
        app_opts.dtau = ['Week', 7]
        outp.make_sim_movies(app_opts, hobs=True)
    #
    app_opts.init_dt = cf['latent_dt']
    smodel = inp.load_popmodel(app_opts, smod_opts_copy, False)
    app_opts.cprefix = cprefix + 'with_latent_'
    psim = sstruct.PopSim(smodel, sim_opts)
    psim.run(prefix=app_opts.cprefix)
    outp.make_sim_maps(app_opts, corrgram=True, chunk_no=chunk_no)
    outp.make_roc_plots(app_opts, chunk_no=chunk_no)
    if False and run_type == 'long':
        app_opts.dtau = ['Week', 7]
        outp.make_sim_movies(app_opts, hobs=True)


if __name__ == '__main__':
    args = list(sys.argv)
    if len(args) != 8:
        print "Usage: python fit_hlb_acp.py ",
        print "<run_type> <param_wt> <chain_no> <cf_no> <region> <to_fit>",
        print "<chunk_no>"
    else:
        main(args[1], float(args[2]), int(args[3]), int(args[4]), args[5],
             bool(args[6]), chunk_no=int(args[7]))
