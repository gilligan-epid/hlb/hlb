"""
Configrations for Florida experiments.

Similar configure files are available for other regions.
"""

import data.const as const
import hidalgo_configs as hc
from hidalgo_configs import resol, cscale, dist_band, hdens_threshold, data_tau

import copy
import datetime


#
cf = copy.deepcopy(hc.cf)
cf['param_wbins']['varepsilon'] = False
cf['varepsilon'] = cf['varepsilon'][0]
cf['latent_dt'] = 2 * 365
cf['tFobs'] = const.tFobs('FL') + 1
cf['tF'] = const.tF('FL') + 1
cf['with_weather'] = False

cf1 = copy.deepcopy(cf)
cf1['latent_dt'] = 3 * 365

cf2 = copy.deepcopy(cf)
cf2['latent_dt'] = 1 * 365

configs = {
    'hlb_only': [cf, cf1, cf2],
}
