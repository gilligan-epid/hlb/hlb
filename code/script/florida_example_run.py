"""
Test Florida simulation runs before sending them to the cluster.

Make sure that you activate the 'idmf' virtual environment before running the
code. To activate:

module load rhel7/default-peta4
module load gcc-7.2.0-gcc-4.8.5-pqn7o2k
source /rds/project/cag1/rds-cag1-general/epidem-userspaces/van25/Envs/idmf_venv/bin/activate
"""

import context
import prospred_hlb


scenarios = ['historic', 'with_cspray', 'with_quarantine', 'cspray+quarantine']
scen_idx = 0

# Region name
region = 'FL'


# Arguments for simulations
cf_no = 0  # index of the configuration used (listed in casouth_configs)
# which set of parameter estimates to use
param_region = 'TX'
run_type, param_wt, chain_no = 'long', 0.01, 0
# parameter coefficients
eps_ratio = 1
# number of simulations
n_iter_sim, n_block_sim = 2, 1
n_samp = n_iter_sim * n_block_sim
# folder name
dp1, dp2 = 'longfit_v2.15_hlb_only_pw0.01', 'wTrue_fd1'
dir_suffix = '_s{}'.format(n_samp)
adir = '{}_cf{}_{}_prospred_hlb{}{}_{}'.format(
    dp1, cf_no, dp2, param_region, dir_suffix, scenarios[scen_idx])

if scen_idx == 0:
    eta = 0.8  # control efficiency 20%
    qtrunc, qrad = 0, 0
elif scen_idx == 1:
    eta = 0.2  # control efficiency 80%
    qtrunc, qrad = 0, 0
elif scen_idx == 2:
    eta = 0.8
    qtrunc, qrad = 0.2, 8
elif scen_idx == 3:
    eta = 0.2
    qtrunc, qrad = 0.2, 8

prospred_hlb.main(
    run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
    eta=eta, qtrunc=qtrunc, qradius=qrad, condor_run=False,
    n_iter_sim=n_iter_sim, n_block_sim=n_block_sim, with_retropred=0,
    adir=adir)
