import data.const as const
import casouth_configs as cc
from casouth_configs import resol, cscale, dist_band, hdens_threshold, data_tau

import copy
import datetime

#
cf_acp = copy.deepcopy(cc.cf_acp)
cf_acp['drate'] = 1. / 30  # monthly check
cf_acp['t0'] = 1
cf_acp['latent_dt'] = 0
cf_acp['tFobs'] = const.tFobs_acp('CentralValley') + 1
cf_acp['tF'] = const.tF_acp('CentralValley') + 1
acp_tchange = (datetime.datetime(2018, 4, 1) -
               const.date0['CentralValley']).days

cf_acp1 = copy.deepcopy(cf_acp)
cf_acp1['tFobs'] = cf_acp['tFobs'] - 365
cf_acp1['tF'] = cf_acp['tFobs']

cf_acp2 = copy.deepcopy(cf_acp)
cf_acp2['tFobs'] = cf_acp['tFobs'] - 3 * 365
cf_acp2['tF'] = cf_acp['tFobs']

cf_acp3 = copy.deepcopy(cf_acp)
cf_acp3['tFobs'] = cf_acp['tFobs'] - 2 * 365
cf_acp3['tF'] = cf_acp['tFobs']

cf_acp4 = copy.deepcopy(cf_acp3)
cf_acp4['tF'] = const.tF('CentralValley') + 1

#
cf_hlb = copy.deepcopy(cc.cf)
cf_hlb['plant_thres'] = [38]
cf_hlb['gamma_V'] = cf_acp['gamma']
cf_hlb['drate_V'] = cf_acp['drate']
cf_hlb['param_wbins'] = {'gamma': True, 'gamma_V': True}
cf_hlb['t0'] = 1
cf_hlb['latent_dt'] = 0
cf_hlb['tFobs'] = const.tFobs('CentralValley') + 1
cf_hlb['tF'] = const.tF('CentralValley') + 1
'''cf_hlb['primary_intros'] = (
    [615, 1010, 1349, 1362, 1362.1],
    [3479, 4600, 207, 1565, 1566])'''

cf_hlb1 = copy.deepcopy(cf_hlb)
cf_hlb1['tFobs'] = cf_acp1['tFobs']
cf_hlb1['primary_intros'] = (
    [2005, 2005.1, 2087, 2139, 2243],
    [1857, 2985, 3441, 4404, 4010])
# cf_hlb1['tF'] = cf_hlb1['tFobs'] + 365

cf_hlb2 = copy.deepcopy(cf_hlb)
cf_hlb2['tFobs'] = cf_acp2['tFobs']
cf_hlb2['primary_intros'] = (
    [995, 1337, 2005, 2005.1, 2087, 2139, 2243],
    [1612, 954, 1857, 2985, 3441, 4404, 4010])

cf_hlb4 = copy.deepcopy(cf_hlb2)
cf_hlb4['tFobs'] = cf_acp4['tFobs']

#
configs = {
    'acptrap': [cf_acp, cf_acp1, cf_acp2, cf_acp3, cf_acp4],
    'acp_hlb': [cf_hlb, cf_hlb1, cf_hlb2, None, cf_hlb4]
}
