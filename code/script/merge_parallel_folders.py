import context
import data.input.texas as inp
import florida_configs as hc

import numpy as np
import h5py
import os


prefix = 'longfit_v2.15'
region = 'FL'
chain_nos = [0, 1, 2]
cf_no = 0
stype = 4

if stype == 0:
    n_samp, n_chunks = 4, 50
    postfix_merge = 'prospred_hlbTX_eta0.8_s{}_no_control_no_vareps_merged'.format(
        n_samp * n_chunks)
    postfix = 'prospred_hlbTX_eta0.8_s{}_no_control_no_vareps'.format(n_samp)
elif stype == 1:
    n_samp, n_chunks = 4, 50
    postfix_merge = 'prospred_hlbTX_eta0.8_s200_with_tremoval2_no_vareps_merged'
    postfix = 'prospred_hlbTX_eta0.8_s4_with_tremoval2_no_vareps'
elif stype == 2:
    n_samp, n_chunks = 4, 50
    postfix_merge = 'prospred_hlbTX_eta0.2_s200_with_cspray_no_vareps_merged'
    postfix = 'prospred_hlbTX_eta0.2_s4_with_cspray_no_vareps'
elif stype == 3:
    n_samp, n_chunks = 2, 100
    postfix_merge = 'prospred_hlbTX_eta0.8_qtrunc0.2_qrad8_s200_with_quarantines_no_vareps_merged'
    postfix = 'prospred_hlbTX_eta0.8_qtrunc0.2_qrad8_s2_with_quarantines_no_vareps'
elif stype == 4:
    n_samp, n_chunks = 4, 50
    postfix_merge = 'prospred_hlbTX_eta0.2_qtrunc0.2_qrad8_s200_with_full_control2_no_vareps_merged'
    postfix = 'prospred_hlbTX_eta0.2_qtrunc0.2_qrad8_s4_with_full_control2_no_vareps'


model = 'hlb_only'
with_weather = False

#
adir = '{}_{}_pw0.01_cf{}_w{}_fd1_{}'.format(
    prefix, model, cf_no, with_weather, postfix_merge)
app_opts = inp.AppOptions(
    region=region, csize=hc.resol, cscale=hc.cscale, adir=adir,
    dist_band=hc.dist_band,)

# Get the list of keys and create the overall file
adir = '{}_{}_pw0.01_cf{}_w{}_fd1_{}'.format(
    prefix, model, cf_no, with_weather, postfix)
ck = 0
adir_ck = adir + '_ck{}'.format(ck)
app_opts_ck = inp.AppOptions(
    region=region, csize=hc.resol, cscale=hc.cscale, adir=adir_ck,
    dist_band=hc.dist_band,)
with h5py.File(os.path.join(
        app_opts_ck.outdir, 'c0_sim_aggregate.h5'), "r") as f0:
    keys = f0.keys()
    tt = f0['by_time/T'][:]
    S = f0['by_time/S'][:]
    for ci in chain_nos:
        with h5py.File(os.path.join(
                app_opts.outdir, 'c{}_sim_aggregate.h5'.format(ci)), "w") as f:
            f.create_group('by_time')
            f.create_dataset('by_time/T', data=tt)
            f.create_dataset('by_time/S', dtype=np.int32, shape=(
                n_samp * n_chunks, S.shape[1], S.shape[2]))
            f.create_dataset('by_time/Sw', dtype=np.float, shape=(
                n_samp * n_chunks, S.shape[1], S.shape[2]))
            for key in keys:
                if ('by_space' in key) and ('dcount' not in key):
                    ss = f0[key][:]
                    f.create_dataset(key, shape=(
                        n_samp * n_chunks, ss.shape[1]))


for ck in range(n_chunks):
    adir_ck = adir + '_ck{}'.format(ck)
    app_opts_ck = inp.AppOptions(
        region=region, csize=hc.resol, cscale=hc.cscale, adir=adir_ck,
        dist_band=hc.dist_band,)
    ii = slice(ck * n_samp, (ck + 1) * n_samp)
    for ci in chain_nos:
        with h5py.File(os.path.join(
                app_opts.outdir, 'c{}_sim_aggregate.h5'.format(ci)), "a") as f:
            with h5py.File(os.path.join(
                    app_opts_ck.outdir, 'c{}_sim_aggregate.h5'.format(ci)),
                    "r") as fc:
                f['by_time/S'][ii, :, :] = fc['by_time/S'][:]
                f['by_time/Sw'][ii, :, :] = fc['by_time/Sw'][:]
                for key in keys:
                    if ('by_space' in key) and ('dcount' not in key):
                        f[key][ii, :] = fc[key][:]
