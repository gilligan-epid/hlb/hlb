from fit_slurm import code_dir, make_outdir, write_base
import hidalgo_configs as cfs

import os


def make_slurm_pipeline_file(
        run_types, param_wts, n_chains, n_chunks, config_nos, region, exp_str,
        slevel=2, ltime='12:00:00', mblock=1, chunk_start=0, dir_suffix=''):
    dname = '{}_{}km'.format(region, cfs.resol * cfs.cscale)
    # write submit files
    j, jnames = 0, []
    for rtype in run_types:
        for k in config_nos:
            for par_wt in param_wts:
                wdir = make_outdir(rtype, exp_str, par_wt, k, dname)\
                    + dir_suffix
                for c in xrange(n_chains):
                    for ck in xrange(chunk_start, chunk_start + n_chunks):
                        jname = '{}_fit_{}_{}'.format(dname, exp_str, j)
                        wargs = 'fit_{}.py {} {} {} {} {} {} {}'.format(
                            exp_str, rtype, par_wt, c, k, region, 1, ck)
                        with open(os.path.join(
                                code_dir, 'slurm', '{}.submit'.format(
                                    jname)), 'w') as f:
                            write_base(f, slevel, jname, ltime, None, mblock)
                            f.write('output_dir={}\n'.format(wdir))
                            f.write('echo $output_dir\n')
                            f.write('mkdir -p $output_dir\n')
                            f.write('cd $output_dir\n')
                            f.write('if [ ! -d "code" ]; then\n')
                            f.write('cp -rf {}/hlb/code .\n'.format(code_dir))
                            f.write('fi\n')
                            f.write('sleep 1m\n')
                            f.write('python code/script/{}\n'.format(wargs))
                        jnames += [jname]
                        j += 1
    # write pipeline file
    with open(os.path.join(code_dir, 'slurm', '{}_{}_pipeline.sh'.format(
            dname, exp_str)), 'w') as f:
        f.write('#!/bin/bash\n')
        j = 0
        for rtype in run_types:
            for k in config_nos:
                for par_wt in param_wts:
                    for c in xrange(n_chains):
                        f.write('jid{}=$(sbatch {}.submit)\n'.format(
                            j, jnames[j]))
                        f.write("jid{}=$(echo $jid{} ".format(j, j) +
                                "| awk '{print $NF}')\n")
                        j += 1
                        for ck in xrange(1, n_chunks):
                            f.write(
                                'jid{}=$(sbatch --dependency=afterany:'.format(
                                    j) + '$jid{} {}.submit)\n'.format(
                                        j - 1, jnames[j]))
                            f.write("jid{}=$(echo $jid{} ".format(j, j) +
                                    "| awk '{print $NF}')\n")
                            j += 1


if __name__ == '__main__':
    param_wts = [.01]
    region = 'TX'
    config_nos = [5, 6]
    run_types, n_chains = ['short'], 3
    chunk_start, n_chunks = 0, 6
    dir_suffix = '_{}chunks'.format(chunk_start + n_chunks)
    for exp_str in ['hlb_only', 'hlb_acp']:
        make_slurm_pipeline_file(
            run_types, param_wts, n_chains, n_chunks, config_nos, region,
            exp_str, slevel=2, ltime='36:00:00', chunk_start=chunk_start,
            dir_suffix=dir_suffix)
