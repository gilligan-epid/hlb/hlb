import os
import context
import data.const as const
import hidalgo_configs

code_dir = os.path.join(
    "/rds", "project", "cag1", "rds-cag1-general", "epidem-userspaces",
    "van25", "CODE", "HLB")
mcmc_iters = {
    'test': [50, 3, 100],
    'short': [250, 8, 500],
    'medium': [1000, 6, 1000],
    'long': [1000, 12, 2000]
}
n_chains = 3


def make_outdir(run_type, exp_str, par_wt, cf_no, dname, eps_ratio=None,
                hc=hidalgo_configs, with_weather=None):
    estr = '_er{:g}'.format(eps_ratio)\
        if eps_ratio is not None and eps_ratio != 1 else ''
    if with_weather is None:
        with_weather = hc.configs[exp_str][cf_no]['with_weather']
    fd = hc.configs[exp_str][cf_no]['freq_depend']
    return os.path.join(const.output_path, dname,
                        '{}fit_v2.15_{}_pw{}_cf{}_w{}_fd{}{}'.format(
                            run_type, exp_str, par_wt, cf_no, with_weather,
                            int(fd), estr))


def write_jcase(j, wdir, wargs, f, n_jobs):
    if n_jobs == 1:
        f.write('python_arguments="code/script/{}"\n'.format(wargs))
        f.write('output_dir={}\n'.format(wdir))
        return
    if j > 0:
        f.write('el')
    if j == n_jobs - 1:
        f.write('se\n')
    else:
        f.write('if [ $SLURM_ARRAY_TASK_ID -eq {} ]; then\n'.format(j))
    f.write('\t')
    f.write('python_arguments="code/script/{}"\n'.format(wargs))
    f.write('\t')
    f.write('output_dir={}\n'.format(wdir))
    if j == n_jobs - 1:
        f.write('fi\n')


def write_base(f, slevel, jname, ltime, n_jobs, mblock):
    f.write('#!/bin/bash\n')
    f.write('#SBATCH -J {}\n'.format(jname))
    f.write('#SBATCH -A GILLIGAN-SL{}-CPU\n'.format(slevel))
    f.write('#SBATCH --output={}_%A_%a.out\n'.format(jname))
    f.write('#SBATCH --error={}_%A_%a.err\n'.format(jname))
    f.write('#SBATCH --nodes=1\n')
    f.write('#SBATCH --ntasks=1\n')
    f.write('#SBATCH --cpus-per-task=1\n')
    f.write('#SBATCH --time={}\n'.format(ltime))
    f.write('#SBATCH --mem={}mb\n'.format(int(5980 * mblock)))
    if n_jobs is not None:
        f.write('#SBATCH --array=0-{}\n'.format(n_jobs - 1))
    f.write('#SBATCH -p skylake\n')
    f.write('#SBATCH --mail-type=END,FAIL\n')
    f.write('#SBATCH --mail-user=vietanh0@gmail.com\n')
    f.write('\n')
    f.write('. /etc/profile.d/modules.sh\n')
    f.write('module purge\n')
    f.write('module load rhel7/default-peta4\n')
    f.write('module load gcc-7.2.0-gcc-4.8.5-pqn7o2k\n')
    f.write('source /rds/project/cag1/rds-cag1-general/' +
            'epidem-userspaces/van25/Envs/idmf_venv/bin/activate\n')
    f.write('\n')


def make_slurm_submit_file(
        run_types, param_wts, n_chains, config_nos, region, exp_str, slevel=2,
        ltime='12:00:00'):
    dname = '{}_1.0km'.format(region)
    jname = '{}_fit_{}'.format(region, exp_str)
    n_jobs = len(run_types) * len(config_nos) * len(param_wts) * n_chains
    with open(os.path.join(
            code_dir, 'slurm', '{}.submit'.format(jname)), 'w') as f:
        write_base(f, slevel, jname, ltime, n_jobs)
        j = 0
        for rtype in run_types:
            for k in config_nos:
                for par_wt in param_wts:
                    wdir = make_outdir(rtype, exp_str, par_wt, k, dname)
                    wdir += '_refit_epsilon'
                    for c in xrange(n_chains):
                        wargs = 'fit_{}.py {} {} {} {} {} {}'.format(
                            exp_str, rtype, par_wt, c, k, region, 1)
                        write_jcase(j, wdir, wargs, f, n_jobs)
                        j += 1
        #
        f.write('\n')
        f.write('echo $output_dir\n')
        f.write('mkdir -p $output_dir\n')
        f.write('cd $output_dir\n')
        f.write('cp -rf {}/hlb/code .\n'.format(code_dir))
        f.write('python $python_arguments\n')


if __name__ == '__main__':
    param_wts = [.01]
    # Texas
    region = 'TX'
    run_types = ['test']
    config_nos = [0, 1]
    for exp_str in ['hlb_only', 'hlb_acp']:
        make_slurm_submit_file(
            run_types, param_wts, n_chains, config_nos, region, exp_str,
            slevel=2, ltime='36:00:00')
    # Southern California
    region = 'CAsouth'
    run_types = ['test']
    config_nos = [0, 1]
    for exp_str in ['hlb_only', 'hlb_acp']:
        make_slurm_submit_file(
            run_types, param_wts, n_chains, config_nos, region, exp_str,
            slevel=3, ltime='12:00:00')
