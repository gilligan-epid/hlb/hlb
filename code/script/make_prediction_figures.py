import context
import data.input.texas as inp
import data.output.texas as outp
import casouth_configs
import cvreset_configs
import data.const as const
import idm.model.SECI as SECI
import idm.model.SECD_SEI as SECD_SEI

import datetime


prefix = 'longfit_v2.15'
region = 'CVreset'  # 'CAsouth' or 'CVreset'
cf_no = 0
with_weather = True
snap_dt = None
sim_labels = None
chain_nos = [0, 1, 2]

if region == 'CAsouth':
    postfix = 'prospred_hlbTX_qtrunc0.2_qrad8_s480_20200331'
    model, dtype = 'hlb_only', ['plant']
    mlib, n_dcomp = SECI, 0
    n_snaps = 0
    snap_dt = None
    hc = casouth_configs
    cis = [0, 1, 2]
    cropped = False
    figsize, fontsize = (13, 8), 18
    with_weather = True
elif region == 'CVreset':
    postfix = 'prospred_acp_hlbTX_ttrunc0.8_trad0.4_s500_20200331'
    model, dtype = 'acp_hlb', ['plant', 'psyllid']
    mlib, n_dcomp = SECD_SEI, 3
    with_weather = True
    cropped = True
    n_snaps, snap_dt = 0, None
    hc = cvreset_configs
    cis = [3, 0, 1]
    sim_labels = ['ACP infested', 'HLB exposed', 'HLB infectious']
    figsize, fontsize = (13, 10), 18

#

#
adir = '{}_{}_pw0.01_cf{}_w{}_fd1_{}'.format(
    prefix, model, cf_no, with_weather, postfix)
print adir
cf = hc.configs[model][cf_no]
#
t0, tFobs = cf['t0'], cf['tFobs']
if region == 'CAsouth':
    tF = cf['tF']
    snap_points = [tFobs] if tF > tFobs else []
elif region == 'CVreset':
    t_start = (datetime.datetime(2020, 12, 20) - const.date0[region]).days
    snap_points = list(range(t_start, cf['tF'], 4 * 365))
    t0 = t_start - 1
    tF = t_start + 10 * 365
else:
    assert False, "Unknown region"
#
app_opts = inp.AppOptions(
    region=region, csize=hc.resol, mlib=mlib, data_type=dtype,
    n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
    data_tau=hc.data_tau, cscale=hc.cscale,
    with_weather=cf['with_weather'], vector_cov=cf['vector_cov'],
    latent_init_period=cf['latent_dt'], init_source='data',
    plant_thres=cf['plant_thres'], n_dcomp=n_dcomp,
    adir=adir, tF=tF, t0=t0, tFobs=tFobs,
)
#
outp.make_spreadmap(
    app_opts, n_snaps=n_snaps, snap_dt=snap_dt, snap_points=snap_points,
    fname='{}_spreadmap'.format(region), cropped=cropped,
    figsize=figsize, fontsize=fontsize, with_tif=True, ts=None, cis=cis,
    chain_nos=chain_nos, sim_labels=sim_labels)
