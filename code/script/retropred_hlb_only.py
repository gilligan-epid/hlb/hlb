import context
import data.const as const
import data.input.texas
import data.output.texas
import data.input.california
import data.output.california
import data.input.florida
import idm.model.struct as mstruct
import idm.model.SECI as mlib
import idm.simulation.struct as sstruct
import hidalgo_configs
import casouth_configs
from fit_condor import make_outdir

import sys
import numpy as np
import copy
np.seterr(divide='ignore')

data_type = ['plant']
obs_models = ['Noulli']
n_dcomp = 0


def main(run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
         exp_str='hlb_only', eta=-1., condor_run=True, adir=None, beta_ratio=1,
         data_map=False, n_iter_sim=50, n_block_sim=2, param_cf_no=1,
         chunk_no=0):

    """
    Predictive simulation
     1. Load parameter distribution
    """
    with_corrgram = False
    draw_data = True
    cropped = True
    n_snaps = 1
    if region == 'TX' or region == 'Hidalgo':
        inp = data.input.texas
        outp = data.output.texas
        hc = hidalgo_configs
        with_corrgram = True
    if region == 'CAsouth':
        inp = data.input.california
        outp = data.output.california
        hc = casouth_configs
        cropped = False
    param_list = [
        'alpha', 'mu', 'beta', 'epsilon', 'dprob', 'eta', 'varepsilon',
        'sigma', 'drate']
    cf = hc.configs[exp_str][cf_no]
    #
    tF = cf['tF']
    tFobs = cf['tFobs']
    snap_points = [tFobs] if tF > tFobs else []
    #
    app_opts = inp.AppOptions(
        region=region, csize=hc.resol, mlib=mlib, data_type=data_type,
        n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
        obs_models=obs_models, data_tau=hc.data_tau, cscale=hc.cscale,
        with_weather=cf['with_weather'], vector_cov=cf['vector_cov'],
        latent_init_period=cf['latent_dt'], init_source='data',
        plant_thres=cf['plant_thres'], n_dcomp=n_dcomp, condor_run=condor_run,
        urban_fraction=cf['urban_fraction'], nw_f=cf['nw_f'], t0=cf['t0'],
        tF=cf['tFobs'], with_data=True, adir=adir, tFobs=cf['tFobs'],
    )
    if data_map:
        outp.make_data_maps(app_opts)
    #
    cprefix = 'c{}_'.format(chain_no) if param_region is not None else ''
    if param_region is not None:
        app_opts.cprefix = cprefix
        param_cf_no = cf_no if param_cf_no < 0 else param_cf_no
        app_opts.paramdir = make_outdir(
            run_type, exp_str, param_wt, param_cf_no, '{}_1.0km'.format(
                param_region))
        print "made out " + app_opts.paramdir
        param_dist = sstruct.PopLearn.load_params(
            app_opts.paramdir, param_list, prefix=cprefix)
        param_dist['gamma'] = np.ones_like(param_dist['beta']) * cf['gamma']
        param_dist['epsilon'] = param_dist['epsilon'] * eps_ratio
        param_dist['beta'] = param_dist['beta'] * beta_ratio
        if eta >= 0:
            param_dist['eta'][:] = eta
        if region != 'TX' and region != 'Hidalgo':
            param_dist['varepsilon'] = param_dist['varepsilon'][:, 0]
        dp_shp = [param_dist['dprob'].shape[0]] + list(cf['dprob'].shape)
        param_dist['dprob'] = np.reshape(param_dist['dprob'], dp_shp)
        ''' @TEST
        if draw_data:
            param_dist['dprob'] = np.ones_like(param_dist['dprob']) * 0.8'''
        #
        posts_dir = None
        # print posts_dir
    else:
        param_dist = {
            'epsilon': [cf['epsilon']], 'alpha': [cf['alpha']],
            'beta': [cf['beta']], 'varepsilon': [cf['varepsilon']],
            'gamma': [cf['gamma']], 'sigma': [cf['sigma']],
            'dprob': [cf['dprob']], 'eta': [cf['eta']], 'mu': [cf['mu']]}
    #
    mod_opts0 = mstruct.ModelOptions(
        param_dist=param_dist, param_wbins=cf['param_wbins'],
        dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
        fsize_known=False, eta_fn=cf['eta_fn'],
    )
    mod_opts1 = copy.deepcopy(mod_opts0)
    ''' @TEST
    mod_opts0.param_dist['sigma'] = 1. / (
        1. / mod_opts0.param_dist['sigma'] + 1. / param_dist['drate'])'''
    if region != 'TX':
        mod_opts1.param_dist['sigma'] = mod_opts0.param_dist['sigma']
    # posterior model
    postmodel = inp.load_popmodel(app_opts, mod_opts0, True, 'data',
                                  traj_update=False)
    app_opts.t0 = cf['tFobs']
    app_opts.tF = cf['tF']
    app_opts.init_dt = 0
    priormodel = inp.load_popmodel(
        app_opts, mod_opts1, False)
    #
    app_opts.t0 = cf['t0']
    app_opts.init_dt = cf['latent_dt']
    snap_times, ignore = outp.make_snap_times(
        app_opts, n_snaps=n_snaps, snap_points=snap_points, cropped=cropped)
    print 'snapshots:', snap_times
    sim_tau = 1  # ?
    sim_opts = sstruct.SimOptions(
        outdir=app_opts.outdir, n_comp=mlib.n_comp, snap_times=snap_times,
        n_iter=n_iter_sim, n_block=n_block_sim, tau=sim_tau,
        primary_intros=cf['primary_intros'], with_single=True, n_dcomp=n_dcomp,
        draw_data=draw_data, chunk_no=chunk_no,
    )
    psim = sstruct.PopSimRetro(
        postmodel, priormodel, sim_opts, posts_dir=posts_dir)
    print "chunks are " + str(sim_opts.chunk_no)
    print("into sims retro")
    psim.run(prefix=cprefix)
    print("into maps retro")
    outp.make_sim_maps(
        app_opts, prim_intros=cf['primary_intros'][1], n_snaps=n_snaps,
        snap_points=snap_points, corrgram=with_corrgram, cropped=cropped,
        dpc_wtd=True)
    print("out of maps retro")

if __name__ == '__main__':
    args = list(sys.argv)
    if len(args) != 15:
        print len(args)
        for i in xrange(len(args)):
            print i, args[i]
        print "Usage: python retropred_hlb_only.py ",
        print "<run_type> <param_wt> <chain_no> <cf_no> <eps_ratio>",
        print "<region> <param_region> <exp_str> <eta>",
        print "<beta_ratio> <n_iter_sim> <n_block_sim> <chunk_no>",
        print "<param_cf_no>"
    else:
        main(args[1], args[2], int(args[3]), int(args[4]), float(args[5]),
             args[6], args[7], args[8], float(args[9]),
             beta_ratio=float(args[10]), n_iter_sim=int(args[11]),
             n_block_sim=int(args[12]), chunk_no=int(args[13]),
             param_cf_no=int(args[14]))
