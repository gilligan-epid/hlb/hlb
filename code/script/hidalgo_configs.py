import data.const as const

import numpy as np
import copy
import datetime

# Default configuration for HLB
resol = 1000  # metre
cscale = 1e-3  # to km
dist_band = 15
hdens_threshold = 1e-3
data_tau = ['Day', 1]
cf = {}
cf['n_maxhost'] = 1
cf['dprob'] = np.array([[.9 / cf['n_maxhost']]])
cf['latent_dt'] = 365 * 3
cf['epsilon'] = 1e-3
cf['varepsilon'] = np.array([1e-3, 1e-3])
cf['varepsilon_update'] = True
cf['gamma'] = 1 / (1.25 * 365)  # from Parry et al.
cf['sigma'] = 1 / (1. * 365)  # from Parry et al.
cf['alpha'], cf['mu'], cf['beta'] = 2.5, 0.1, .4
cf['eta'] = .1
cf['eta_fn'] = 'linear'
cf['param_wbins'] = {'gamma': True, 'varepsilon': True}
cf['dkernel'] = 'exp'
cf['freq_depend'] = True
cf['knorm'] = True
cf['with_weather'] = True
cf['vector_cov'] = None
cf['primary_intros'] = ([], [])
cf['plant_thres'] = [36]  # [36], [32, 38]
cf['urban_fraction'] = 1
cf['t0'] = 1
cf['tFobs'] = const.tFobs('TX') + 1
cf['tF'] = const.tF('TX') + 1
cf['nw_f'] = 1

tFobs_estimation = (datetime.datetime(2017, 8, 1) - const.date0['TX']).days + 1

cf1 = copy.deepcopy(cf)
cf1['tFobs'] = tFobs_estimation - 365
cf1['tF'] = cf['tFobs']

# model selections
# 2. kernel normalisation
cf2 = copy.deepcopy(cf1)
cf2['freq_depend'] = False

# 3. control efficiency
cf3 = copy.deepcopy(cf1)
cf3['eta'] = 1
cf3['eta_fn'] = None

# 4. varepsilon
cf4 = copy.deepcopy(cf1)
cf4['varepsilon'] = np.ones(2) * 1e-5
cf4['varepsilon_update'] = False

# 5. power-law
cf5 = copy.deepcopy(cf1)
cf5['dkernel'] = 'pow'

cf6 = copy.deepcopy(cf)
cf6['dkernel'] = 'pow'

cf7 = copy.deepcopy(cf)
cf7['tF'] = (datetime.datetime(2015, 1, 1) - const.date0['TX']).days

cf8 = copy.deepcopy(cf)
cf8['tF'] = (datetime.datetime(2014, 1, 1) - const.date0['TX']).days

cf9 = copy.deepcopy(cf)
cf9['tF'] = (datetime.datetime(2013, 1, 1) - const.date0['TX']).days

'''cf6 = copy.deepcopy(cf)
cf6['vector_cov'] = None
cf6['urban_fraction'] = 0

cf7 = copy.deepcopy(cf6)
cf7['urban_fraction'] = 0.3

cf8 = copy.deepcopy(cf6)
cf8['urban_fraction'] = 0.7

cf9 = copy.deepcopy(cf6)
cf9['urban_fraction'] = 1'''

#
cfa = copy.deepcopy(cf)
cfa['dprob'] = np.array([[[.9 / cf['n_maxhost']]], [[.9 / cf['n_maxhost']]]])
cfa['delta'] = cfa['beta']
cfa['param_wbins']['delta'] = False
cfa['psyllid_thres'] = [38]

cfa1 = copy.deepcopy(cfa)
cfa1['tFobs'] = tFobs_estimation - 365
cfa1['tF'] = cf['tFobs']

# model selections
# 2. kernel normalisation
cfa2 = copy.deepcopy(cfa1)
cfa2['freq_depend'] = False

# 3. control efficiency
cfa3 = copy.deepcopy(cfa1)
cfa3['eta'] = 1
cfa3['eta_fn'] = None

# 4. varepsilon
cfa4 = copy.deepcopy(cfa1)
cfa4['varepsilon'] = np.ones(2) * 1e-5
cfa4['varepsilon_update'] = False

# 5. power-law kernel
cfa5 = copy.deepcopy(cfa1)
cfa5['dkernel'] = 'pow'

# 6. 
cfa6 = copy.deepcopy(cfa)
cfa6['dkernel'] = 'pow'

#
configs = {
    'hlb_only': [cf, cf1, cf2, cf3, cf4, cf5, cf6, cf7, cf8],
    'hlb_acp': [cfa, cfa1, cfa2, cfa3, cfa4, cfa5, cfa6],
}
