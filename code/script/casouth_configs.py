import data.const as const
import hidalgo_configs as hc
from hidalgo_configs import resol, cscale, dist_band, hdens_threshold, data_tau

import copy
import datetime

#
cf = copy.deepcopy(hc.cf)
cf['param_wbins']['varepsilon'] = False
cf['varepsilon'] = cf['varepsilon'][0]
cf['latent_dt'] = 1.25 * 365  # checked
# CANCELLED cf['plant_thres'] = [38]  # as recommended by Tim G.
cf['tFobs'] = const.tFobs('CAsouth') + 1
cf['tF'] = const.tF('CAsouth') + 1

cf1 = copy.deepcopy(cf)
cf1['tFobs'] = cf['tFobs'] - 365 * 2
cf1['tF'] = cf['tFobs']

cf2 = copy.deepcopy(cf)
cf2['tFobs'] = cf['tFobs'] - 365 * 1  # switched for cf1
cf2['tF'] = cf['tFobs']

cf3 = copy.deepcopy(cf)
cf3['tFobs'] = 30
cf3['tF'] = cf['tFobs']

cf4 = copy.deepcopy(cf1)  # cf1 without weather
cf4['with_weather'] = False

#
cfa = copy.deepcopy(hc.cfa)
cfa['param_wbins']['varepsilon'] = False
cfa['varepsilon'] = cfa['varepsilon'][0]
cfa['latent_dt'] = cf['latent_dt']
cfa['plant_thres'] = cf['plant_thres']
cfa['tFobs'] = cf['tFobs']
cfa['tF'] = cf['tF']

cfa1 = copy.deepcopy(cfa)
cfa1['tFobs'] = cf1['tFobs']
cfa1['tF'] = cf1['tF']

cfa2 = copy.deepcopy(cfa)
cfa2['tFobs'] = cf2['tFobs']
cfa2['tF'] = cf2['tF']

cfa3 = copy.deepcopy(cfa)
cfa3['tFobs'] = cf3['tFobs']
cfa3['tF'] = cf3['tF']

#
acp_deathrate = 1. / 90
#
cf_acp = copy.deepcopy(cf)
cf_acp['t0'] = 250
cf_acp['tF'] = const.tF_acp('CAsouth') + 1
cf_acp['tFobs'] = const.tFobs_acp('CAsouth') + 1
cf_acp['latent_dt'] = 1. * 365  # before 1st detection
cf_acp['gamma'] = 1. / 20  # 20 days
cf_acp['param_wbins'] = {'gamma': True}
acp_tchange = (
    datetime.datetime(2011, 3, 1) - const.date0_acp['CAsouth']).days

# 1. Pre-2011
cf_acp1 = copy.deepcopy(cf_acp)
cf_acp1['tFobs'] = (
    datetime.datetime(2009, 12, 31) - const.date0_acp['CAsouth']).days
cf_acp1['tF'] = acp_tchange

# 2. Pre-2011 extented
cf_acp2 = copy.deepcopy(cf_acp1)
cf_acp2['tF'] = cf_acp['tFobs']

# 3. Post-2011 retropred
cf_acp3 = copy.deepcopy(cf_acp)
cf_acp3['tFobs'] = acp_tchange + 1
cf_acp3['tF'] = cf_acp['tFobs']

#
configs = {
    'hlb_only': [cf, cf1, cf2, cf3, cf4],
    'hlb_acp': [cfa, cfa1, cfa2, cfa3],
    'acptrap': [cf_acp, cf_acp1, cf_acp2, cf_acp3],
}
