import context
import data.const as const
import data.input.california as inp
import data.output.california as outp
import idm.model.struct as mstruct
import idm.model.SECI as mlib
import idm.simulation.struct as sstruct
import casouth_configs
import cv_configs
from fit_condor import make_outdir

import sys
import numpy as np
import copy
np.seterr(divide='ignore')

data_type = ['acptrap']
obs_models = ['Noulli']
n_dcomp = 0


def main(run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
         delta_ratio=1, eta=-1., condor_run=True, adir=None, beta_ratio=1,
         data_map=False, n_iter_sim=50, n_block_sim=2, chunk_no=0,
         param_cf_no=-1):

    """
    Predictive simulation
     1. Load parameter distribution
    """
    #
    hc = casouth_configs if region == 'CAsouth' else\
        cv_configs if region == 'CentralValley' else None
    draw_data = True if cf_no > 0 else False
    n_snaps = 1
    param_list = [
        'alpha', 'mu', 'beta', 'epsilon', 'dprob', 'eta', 'varepsilon',
        'delta', 'sigma', 'drate']
    cf = hc.configs['acptrap'][cf_no]
    #
    tF = cf['tF']
    tFobs = cf['tFobs']
    snap_points = [tFobs] if tF > tFobs else []
    #
    app_opts = inp.AppOptions(
        region=region, csize=hc.resol, mlib=mlib, data_type=data_type,
        n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
        obs_models=obs_models, data_tau=hc.data_tau, cscale=hc.cscale,
        with_weather=cf['with_weather'], vector_cov=cf['vector_cov'],
        latent_init_period=cf['latent_dt'], init_source='data',
        plant_thres=cf['plant_thres'], n_dcomp=n_dcomp, condor_run=condor_run,
        urban_fraction=cf['urban_fraction'], nw_f=cf['nw_f'], t0=cf['t0'],
        tF=cf['tFobs'], with_data=True, adir=adir, tFobs=cf['tFobs'],
    )
    #
    cprefix = 'c{}_'.format(chain_no) if param_region is not None else ''
    if param_region is not None:
        app_opts.cprefix = cprefix
        param_cf_no = cf_no if param_cf_no < 0 else param_cf_no
        app_opts.paramdir = make_outdir(
            run_type, 'hlb_acp', param_wt, param_cf_no, '{}_1.0km'.format(
                param_region))
        param_dist = sstruct.PopLearn.load_params(
            app_opts.paramdir, param_list, prefix=cprefix)
        param_dist['gamma'] = np.ones_like(param_dist['beta']) * cf['gamma']
        param_dist['sigma'] = np.ones_like(param_dist['beta']) * cf['drate']
        param_dist['epsilon'][:] = 1e-5
        param_dist['varepsilon'] = param_dist['varepsilon'][:, 0] * 0
        param_dist['delta'] *= delta_ratio
        param_dist['beta'] *= param_dist['delta'] * beta_ratio
        # compute beta, accounting for knorm
        # Z = (2 * np.pi * (param_dist['alpha'] ** 2))
        # param_dist['beta'] *= param_dist['delta'] * Z * beta_ratio
        if eta >= 0:
            param_dist['eta'][:] = eta
        dp_shp = [param_dist['dprob'].shape[0]] + list(cf['dprob'].shape)
        param_dist['dprob'] = np.reshape(param_dist['dprob'][:, 0], dp_shp)
        '''if draw_data:
            param_dist['dprob'][:] = 0.8'''
        #
        posts_dir = None
        # print posts_dir
    else:
        param_dist = {
            'epsilon': [cf['epsilon']], 'alpha': [cf['alpha']],
            'beta': [cf['beta']], 'varepsilon': [cf['varepsilon']],
            'gamma': [cf['gamma']], 'sigma': [cf['sigma']],
            'dprob': [cf['dprob']], 'eta': [cf['eta']], 'mu': [cf['mu']]}
    #
    mod_opts0 = mstruct.ModelOptions(
        param_dist=param_dist, param_wbins=cf['param_wbins'],
        dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
        fsize_known=False, eta_fn=cf['eta_fn'],
    )
    mod_opts1 = copy.deepcopy(mod_opts0)
    #
    print "Building postmodel ..."
    print app_opts.t0, app_opts.tFobs, app_opts.tF
    postmodel = inp.load_popmodel(app_opts, mod_opts0, True, 'data',
                                  traj_update=False)
    app_opts.t0 = cf['tFobs']
    app_opts.tF = cf['tF']
    app_opts.init_dt = 0
    print "Building priormodel ..."
    print app_opts.t0, app_opts.tFobs, app_opts.tF
    priormodel = inp.load_popmodel(app_opts, mod_opts1, False)
    #
    app_opts.t0 = cf['t0']
    app_opts.init_dt = cf['latent_dt']
    snap_times, ignore = outp.make_snap_times(
        app_opts, n_snaps=n_snaps, snap_points=snap_points, cropped=False)
    print 'snapshots:', snap_times
    sim_tau = 1  # ?
    sim_opts = sstruct.SimOptions(
        outdir=app_opts.outdir, n_comp=mlib.n_comp, snap_times=snap_times,
        n_iter=n_iter_sim, n_block=n_block_sim, tau=sim_tau,
        primary_intros=cf['primary_intros'], with_single=True, n_dcomp=n_dcomp,
        draw_data=draw_data, chunk_no=chunk_no,
    )
    psim = sstruct.PopSimRetro(
        postmodel, priormodel, sim_opts, posts_dir=posts_dir)
    psim.run(prefix=cprefix)
    print "Make simulation maps ..."
    outp.make_sim_maps(
        app_opts, prim_intros=cf['primary_intros'][1], n_snaps=n_snaps,
        snap_points=snap_points)
    outp.make_roc_plots(app_opts, tFobs=cf['tFobs'])


if __name__ == '__main__':
    args = list(sys.argv)
    if len(args) != 15:
        print len(args)
        for i in xrange(len(args)):
            print i, args[i]
        print "Usage: python retropred_acp.py ",
        print "<run_type> <param_wt> <chain_no> <cf_no> <eps_ratio>",
        print "<region> <param_region> <delta_ratio> <eta>",
        print "<beta_ratio> <n_iter_sim> <n_block_sim>",
        print "<chunk_no> <param_cf_no>"
    else:
        main(args[1], args[2], int(args[3]), int(args[4]), float(args[5]),
             args[6], args[7], float(args[8]), float(args[9]),
             beta_ratio=float(args[10]), n_iter_sim=int(args[11]),
             n_block_sim=int(args[12]), chunk_no=int(args[13]),
             param_cf_no=int(args[14]))
