"""
Test Central Valley simulation runs before sending them to the cluster.

The script provides 2 operations:
 1. to update the survey data inputted to the model
 3. to run prospective simulation for joint ACP and HLB spread

 Make sure that you activate the 'idmf' virtual environment before running the
 code. To activate:

module load rhel7/default-peta4
module load gcc-7.2.0-gcc-4.8.5-pqn7o2k
source /rds/project/cag1/rds-cag1-general/epidem-userspaces/van25/Envs/idmf_venv/bin/activate
"""

import context
import data.prepare.california as prep
import data.input.california as inp
import hidalgo_configs as hc
import prospred_acp_hlb

import datetime

# Choose which operation to run
update_data = True
reset_data, reset_date = True, datetime.datetime(2017, 01, 01)
prospred_sim = True

# Region name
region = 'CVreset'

"""
 1A. Update the input data with new survey samples (must be run on Windows)
 The updated survey data (.shp files, projected to EPSG:3310) must be copied to
 EXPERIMENTS/HLB/RawData/Infection/ before running this.
"""
if update_data:
    app_opts = inp.AppOptions(
        region='CentralValley', csize=hc.resol, cscale=hc.cscale, adir='',
        dist_band=hc.dist_band,)
    prep.make_plant_df(app_opts)
    prep.make_psyllid_df(app_opts)
    prep.make_acptrap_df(app_opts)
    prep.compile_data(app_opts, 'plant')
    prep.compile_data(app_opts, 'psyllid')
    prep.compile_data(app_opts, 'acptrap')

"""
 1B. Discard data before the given `reset_date` due to the very hot 2016
 summer. Here we take data files from the Input/CentralValley_1.0km/ folder
 as inputs.
"""
if reset_data:
    app_opts = inp.AppOptions(
        region='CVreset', csize=hc.resol, cscale=hc.cscale, adir='',
        dist_band=hc.dist_band,)
    prep.reset_data(app_opts, 'acptrap', reset_date)
    prep.reset_data(app_opts, 'plant', reset_date)
    prep.reset_data(app_opts, 'psyllid', reset_date)


# Arguments for simulations
cf_no = 0  # index of the configuration used (listed in cvreset_configs)
# which set of parameter estimates to use
param_region = 'TX'
run_type, param_wt, chain_no = 'long', 0.01, 0
# parameter coefficients
eps_ratio = 1
# number of simulations
n_iter_sim, n_block_sim = 2, 1
n_samp = n_iter_sim * n_block_sim
# folder name
dp1, dp2 = 'longfit_v2.15_hlb_only_pw0.01', 'wTrue_fd1'

# ACP control arguments
ttrunc, tradius = 0.8, 0.4

"""
 2. Run prospective simulation from the end of the survey data
 to a point in time (specified in data/const.py). Simulations seed
 from the provided survey data.
"""
if prospred_sim:
    adir = '{}_cf{}_{}_prospred_acp_hlb{}_s{}_new_data'.format(
        dp1, cf_no, dp2, param_region, n_samp)
    prospred_acp_hlb.main(
        run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
        condor_run=False, n_iter_sim=n_iter_sim, n_block_sim=n_block_sim,
        with_retropred=0, ttruncV=ttrunc, tradiusV=tradius, adir=adir)
