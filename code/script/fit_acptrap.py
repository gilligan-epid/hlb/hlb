import context
import data.const as const
import data.input.california as inp
import data.output.california as outp
import idm.model.struct as mstruct
import idm.model.SECI as mlib
import idm.simulation.struct as sstruct
import hidalgo_configs as hc
import fit_condor as hcd

import sys
import numpy as np
np.seterr(divide='ignore')

data_type = ['acptrap']
obs_models = ['Noulli']
n_dcomp = 0


def main(run_type, param_wt, chain_no, cf_no, region, to_fit=True,
         condor_run=True, adir=None, data_map=False,
         n_iter_sim=100, n_block_sim=2, t0=1):

    """
    Parameter fitting
     1. Write configurations to file
     2. Construct learn models
     3. Run MCMC
     4. Make parameter plots
    """
    cf = hc.configs['acptrap'][cf_no]
    cf['tFobs'] = const.tFobs_acp(region) + 1
    cf['t0'] = t0
    cprefix = 'c{}_'.format(chain_no)
    param_list = ['alpha', 'mu', 'beta', 'epsilon', 'dprob', 'eta', 'varepsilon', 'sigma']
    #
    epsilon_update, beta_update, alpha_update = False, True, False
    varepsilon_update, eta_update, sigma_update = False, False, False
    dprob_update = True
    tune = {'alpha': 0.2, 'epsilon': 0.5, 'dprob': 0.1, 'eta': 0.1,
            'varepsilon': 0.5, 'mu': 0.2}
    node_update = True
    fsize_known = False
    #
    app_opts = inp.AppOptions(
        region=region, csize=hc.resol, mlib=mlib, data_type=data_type,
        n_maxhost=cf['n_maxhost'], dist_band=hc.dist_band,
        obs_models=obs_models, data_tau=hc.data_tau, cscale=hc.cscale,
        control=cf['control'], vector_cov=cf['vector_cov'],
        latent_init_period=cf['latent_dt'], init_source='data',
        plant_thres=cf['plant_thres'], n_dcomp=n_dcomp, condor_run=condor_run,
        cprefix=cprefix, urban_fraction=cf['urban_fraction'], nw_f=cf['nw_f'],
        tF=cf['tFobs'], adir=adir, t0=cf['t0'],
    )
    if to_fit:
        n_iter_learn, n_block_learn, n_burnin = hcd.mcmc_iters[run_type]
        if data_map:
            outp.make_data_maps(app_opts)
        param0 = {
            'epsilon': cf['epsilon'], 'alpha': cf['alpha'], 'beta': cf['beta'],
            'gamma': cf['gamma'], 'sigma': cf['sigma'], 'dprob': cf['dprob'],
            'eta': cf['eta'], 'varepsilon': cf['varepsilon'], 'mu': cf['mu']}
        lmod_opts = mstruct.ModelOptions(
            param0=param0, param_wbins=cf['param_wbins'],
            dkernel=cf['dkernel'],
            freq_depend=cf['freq_depend'], fsize_known=fsize_known,
            epsilon_update=epsilon_update, beta_update=beta_update,
            alpha_update=alpha_update, node_update=node_update, tune=tune,
            n_burnin=n_burnin, param_wt=param_wt, sigma_update=sigma_update,
            dprob_update=dprob_update, eta_update=eta_update,
            eta_fn=cf['eta_fn'], varepsilon_update=varepsilon_update,
        )
        outp.write_config_file(app_opts, lmod_opts, 'fit')
        lmodel = inp.load_popmodel(
            app_opts, lmod_opts, True, 'data', traj_update=True)
        #
        learn_opts = sstruct.SimOptions(
            outdir=app_opts.outdir, n_comp=mlib.n_comp,
            n_iter=n_iter_learn, n_block=n_block_learn, tau=1,
        )
        plearn = sstruct.PopLearn(lmodel, learn_opts)
        trajs = []
        for dtype in app_opts.dtypes:
            ignore, traj = inp.load_data(app_opts, dtype, with_traj=True)
            trajs += [traj]
        plearn.run(prefix=cprefix, param_list=param_list,
                   dp_len=len(cf['dprob'].flatten()), dtrajs=trajs)
    """
    Predictive simulation
     1. Load parameter distribution
    """
    #
    app_opts.init_source = 'infer'
    app_opts.with_data = True
    app_opts.paramdir = app_opts.outdir if to_fit else hcd.make_outdir(
        run_type, 'acptrap', param_wt, cf_no, '{}_1.0km'.format(region)
    ) if adir is None else app_opts.outdir
    param_dist = sstruct.PopLearn.load_params(
        app_opts.paramdir, param_list, prefix=cprefix)
    param_dist['gamma'] = np.ones_like(param_dist['beta']) * cf['gamma']
    param_dist['sigma'] = np.ones_like(param_dist['beta']) * cf['sigma']
    dp_shp = [param_dist['dprob'].shape[0]] + list(cf['dprob'].shape)
    param_dist['dprob'] = np.reshape(param_dist['dprob'], dp_shp)
    param_dist_plot = {
        k: param_dist[k] for k in param_list}
    param_dist_plot['pi'] = param_dist_plot.pop('dprob')
    outp.make_param_plots(
        app_opts, param_dist_plot, cf['freq_depend'],
        trace=True, init_map=True)
    #
    smod_opts = mstruct.ModelOptions(
        param_dist=param_dist, param_wbins=cf['param_wbins'],
        dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
        eta_fn=cf['eta_fn'],
    )
    smodel = inp.load_popmodel(app_opts, smod_opts, False)
    #
    sim_tau = 1  # ?
    sim_opts = sstruct.SimOptions(
        outdir=app_opts.outdir, n_comp=mlib.n_comp,
        n_iter=n_iter_sim, n_block=n_block_sim, tau=sim_tau,
        primary_intros=cf['primary_intros'], with_single=True, n_dcomp=n_dcomp,
    )
    outp.write_config_file(app_opts, sim_opts, 'sim')
    psim = sstruct.PopSim(smodel, sim_opts)
    psim.run(prefix=cprefix)
    outp.make_sim_maps(app_opts)
    if run_type == 'long':
        app_opts.dtau = ['Week', 7]
        outp.make_sim_movies(app_opts, hobs=True)


if __name__ == '__main__':
    args = list(sys.argv)
    if len(args) != 7:
        print "Usage: python fit_hlb_only.py ",
        print "<run_type> <param_wt> <chain_no> <cf_no> <region> <to_fit>"
    else:
        main(args[1], float(args[2]), int(args[3]), int(args[4]), args[5],
             bool(args[6]))
