"""
Test southern California simulation runs before sending them to the cluster.

The script provides 3 operations:
 1. to update the survey data inputted to the model
 2. to run retrospective simulation
 3. to run prospective simulation

 Make sure that you activate the 'idmf' virtual environment before running the
 code. To activate:

module load rhel7/default-peta4
module load gcc-7.2.0-gcc-4.8.5-pqn7o2k
source /rds/project/cag1/rds-cag1-general/epidem-userspaces/van25/Envs/idmf_venv/bin/activate
"""

import data.prepare.california as prep
import data.input.california as inp
import hidalgo_configs as hc
import prospred_hlb
import retropred_hlb_only

# Choose which operation to run
update_data = True
retropred_sim = True
prospred_sim = True

# Region name
region = 'CAsouth'

"""
 1. Update the input data with new survey samples (must be run on Windows)
 The updated survey data (.shp files, projected to EPSG:3310) must be copied to
 EXPERIMENTS/HLB/RawData/Infection/ before running this.
"""
if update_data:
    app_opts = inp.AppOptions(
        region=region, csize=hc.resol, cscale=hc.cscale, adir='',
        dist_band=hc.dist_band,)
    prep.make_plant_df(app_opts)
    prep.make_psyllid_df(app_opts)
    prep.compile_data(app_opts, 'plant', False)
    prep.compile_data(app_opts, 'psyllid', False)


# Arguments for simulations
cf_no = 0  # index of the configuration used (listed in casouth_configs)
# which set of parameter estimates to use
param_region = 'TX'
run_type, param_wt, chain_no = 'long', 0.01, 0
# parameter coefficients
eps_ratio = 1
# number of simulations
n_iter_sim, n_block_sim = 2, 1
n_samp = n_iter_sim * n_block_sim
# folder name
dp1, dp2 = 'longfit_v2.15_hlb_only_pw0.01', 'wTrue_fd1'
dir_suffix = '_s{}_new_data'.format(n_samp)

"""
 2. Infer the locations of Exposed and Infectious cells at the end of
 survey data by running retrospective simulation.
"""
if retropred_sim:
    adir = '{}_cf{}_{}_retropred_hlb_only{}{}'.format(
        dp1, cf_no, dp2, param_region, dir_suffix)
    retropred_hlb_only.main(
        run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
        condor_run=False, n_iter_sim=n_iter_sim, n_block_sim=n_block_sim,
        adir=adir)

# arguments for HLB quarantine
qtrunc, qradius = 0.2, 8

"""
 3. Run prospective simulation from the end of the survey data
 to a point in time (specified in data/const.py). Simulations start
 from the epidemic state inferred by the above retrospective simulations.
"""
if prospred_sim:
    adir = '{}_cf{}_{}_prospred_hlb_only{}{}'.format(
        dp1, cf_no, dp2, param_region, dir_suffix)
    prospred_hlb.main(
        run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
        condor_run=False, n_iter_sim=n_iter_sim, n_block_sim=n_block_sim,
        with_retropred=1, qtrunc=qtrunc, qradius=qradius,
        retro_suffix=dir_suffix, adir=adir)
