import context
import data.prepare.citrus as citrus
import data.prepare.california as prep
import data.input.california as inp
import hidalgo_configs as hc
import pred_hlb_only
import retropred_hlb_acp
import prospred_hlb
import prospred_acp
import retropred_hlb_only

region = 'CAsouth'
cf_no = 1
model = 'hlb_only'

prepare_data, prep_host = False, False
pred_sim, data_map, sim_movie = False, True, False
retropred_sim = False
prospred_sim = True

run_type, param_wt, chain_no = 'long', 0.01, 0
param_region, eps_ratio, delta_ratio = 'TX', 1, 1
beta_ratio = 1

"""
Prepare data
 1. Make residential citrus raster and extent file
 1.1. Run PolygonToRaster to produce commercial host rasters
 2. Make commercial and residential host raster
 4. Process HLB data
 5. Process psyllid data
"""
if prepare_data:
    app_opts = inp.AppOptions(
        region=region, csize=hc.resol, cscale=hc.cscale, adir='',
        dist_band=hc.dist_band,)
    if prep_host:
        # citrus.make_urban_host_raster(region, resol)
        prep.adjust_host_by_mask(app_opts)
        prep.combine_host_rasters(
            app_opts, ignore_threshold=hc.hdens_threshold)
        prep.make_plant_df(app_opts)
        prep.make_psyllid_df(app_opts)
        prep.compile_data(app_opts, 'plant', False)
        prep.compile_data(app_opts, 'psyllid', False)
        prep.adjust_host_by_data(app_opts)
    prep.compile_weather_variables(app_opts)
    prep.make_covariate_matrices(
        app_opts, ignore_threshold=hc.hdens_threshold)
    #
    prep.mark_orchards(app_opts, 1e-3)
    #
    prep.make_plant_df(app_opts)
    prep.make_psyllid_df(app_opts)
    prep.compile_data(app_opts, 'plant', False)
    prep.compile_data(app_opts, 'psyllid', False)
    # prep.make_wsuit_matrices(app_opts)


if pred_sim:
    n_iter_sim, n_block_sim = 10, 2
    '''pred_hlb_only.main(
        run_type, param_wt, chain_no, cf_no, eps_ratio, region,
        param_region, exp_str=model, condor_run=False,
        adir='{}pred_{}_cf{}_test0'.format(run_type, model, cf_no), qtrunc=0,
        data_map=data_map, n_iter_sim=n_iter_sim, n_block_sim=n_block_sim)'''
    pred_hlb_only.main(
        run_type, param_wt, chain_no, cf_no, eps_ratio, region,
        param_region, exp_str=model, condor_run=False,
        adir='{}pred_{}_cf{}_qr_test_er0.1'.format(run_type, model, cf_no),
        data_map=data_map, n_iter_sim=n_iter_sim, n_block_sim=n_block_sim,
        qtrunc=0.2, qradius=8)


if retropred_sim:
    if model == 'hlb_acp':
        retropred_hlb_acp.main(
            run_type, param_wt, chain_no, cf_no, eps_ratio, region,
            param_region, delta_ratio, condor_run=False,
            adir='{}retropred_{}_cf{}_test30'.format(run_type, model, cf_no),
            data_map=data_map, qtrunc=0, n_iter_sim=2, n_block_sim=2, qradius=0)
    elif model == 'hlb_only':
        retropred_hlb_only.main(
            'long', 0.01, 0, 2, 1, 'CAsouth', 'TX', 'hlb_only',
            n_iter_sim=2, n_block_sim=2, condor_run=False, chunk_no=1, adir='longfit_v2.15_hlb_only_pw0.01_cf2_wTrue_fd1_retropred_hlb_onlyTX_testretro4')


if prospred_sim:
    mlib = prospred_hlb if region == 'CAsouth' else prospred_acp
    n_iter_sim, n_block_sim = 2, 2
    '''prospred_hlb.main(
        run_type, param_wt, chain_no, cf_no, eps_ratio, region,
        param_region, exp_str=model, eta=-1, condor_run=False,
        adir='{}prospred_{}_cf{}_test0'.format(run_type, model, cf_no),
        data_map=data_map, n_iter_sim=n_iter_sim, n_block_sim=n_block_sim,
        beta_ratio=beta_ratio)
    prospred_hlb.main(
        run_type, param_wt, chain_no, cf_no, eps_ratio, region,
        param_region, exp_str=model, eta=-1, condor_run=False,
        adir='{}prospred_{}_cf{}_with_qr_test0'.format(run_type, model, cf_no),
        data_map=data_map, n_iter_sim=n_iter_sim, n_block_sim=n_block_sim,
        beta_ratio=beta_ratio, qtrunc=0.2, qradius=8)'''
    '''retro_suffix = '_s480'
    prospred_hlb.main(
        run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
        condor_run=False, with_movie=False, with_retropred=1,
        retro_suffix=retro_suffix,
        adir='{}_cf{}_wTrue_fd1_prospred_hlbTX_ctrunc0.1_crad8{}_test'.format(
            'longfit_v2.15_hlb_only_pw0.01', cf_no, retro_suffix)
    )'''
    mlib.main(
        run_type, param_wt, chain_no, cf_no, eps_ratio, region, param_region,
        condor_run=False, data_map=False, param_cf_no=1, n_iter_sim=2,
        with_retropred=0, extend_run=True, retro_suffix='_s480_cf1param',
        adir='{}prospred_{}_cf{}_2020_05_13'.format(run_type, model, cf_no),
        qtrunc=0, qradius=0,
    )
