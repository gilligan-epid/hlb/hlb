import fit_slurm as fc
import pred_slurm as ps
import casouth_configs
import cv_configs
import cvreset_configs
import florida_configs

import os
import numpy as np


def make_slurm_pipeline_file(
        run_types, param_wts, n_chains, n_chunks, config_nos, region,
        par_region, pred_types, param_exp_str, n_iter_sim=100,
        n_block_sim=2, qtruncs=[0], qrads=[0.], ttruncs=[0], trads=[0.],
        slevel=2, ltime='12:00:00', mblock=1, chunk_start=0, dir_suffix='',
        n_skipped_block=0, param_cf_no=0, er=1, eta_choices=[-1],
        with_weather=None, with_retropred=0):
    assert with_retropred == 0, "No retropred allowed"
    for pred_type in pred_types:
        if 'retropred' in pred_type:
            assert False, "Invalid prediction type. Use the sequential pipeline!"
    n_samp = n_block_sim * n_iter_sim * n_chunks
    cfs = casouth_configs if region == 'CAsouth' else\
        cv_configs if region == 'CentralValley' else\
        cvreset_configs if region == 'CVreset' else\
        florida_configs if region == 'FL' else None
    dname = '{}_{}km'.format(region, cfs.resol * cfs.cscale)
    # write submit files, one for each job

    def add_file(wdir, wargs):
        with open(os.path.join(
                fc.code_dir, 'slurm', '{}.submit'.format(
                    jname)), 'w') as f:
            fc.write_base(f, slevel, jname, ltime, None, mblock)
            f.write('output_dir={}\n'.format(wdir))
            f.write('echo $output_dir\n')
            f.write('if [ ! -d "$output_dir" ]; then\n')
            f.write('mkdir -p $output_dir\n')
            f.write('fi\n')
            f.write('cd $output_dir\n')
            f.write('if [ ! -d "code" ]; then\n')
            f.write('cp -rf {}/hlb/code .\n'.format(fc.code_dir))
            f.write('fi\n')
            f.write('sleep 1m\n')
            f.write('python code/script/{}\n'.format(wargs))

    j, jnames = 0, []
    for pred_type in pred_types:
        n_skipped_samp = int(n_skipped_block * n_iter_sim)
        n_block_sim = int((n_samp - n_skipped_samp) /
                          (n_iter_sim * n_chunks))
        qtruncs_iter, qrads_iter, trads_iter = qtruncs, qrads, trads
        for rtype in run_types:
            for k in config_nos:
                for par_wt in param_wts:
                    for q in qtruncs_iter:
                        for qrad in qrads_iter:
                            for tr in ttruncs:
                                for trad in trads_iter:
                                    for eta in eta_choices:
                                        br = 1
                                        for c in xrange(n_chains):
                                            for ck in xrange(
                                                    chunk_start, chunk_start +
                                                    n_chunks):
                                                dir_suffix_ck = dir_suffix + '_ck{}'.format(ck)
                                                ck = 0  # single chunk for each job
                                                wdir = ps.make_wdir(
                                                    rtype, k, par_wt, er, eta, br, q, qrad,
                                                    tr, trad, 0, 0, 0, 0, dname,
                                                    param_exp_str, cfs, with_weather,
                                                    pred_type, par_region, dir_suffix_ck)
                                                jname = '{}_{}_by_{}_cf{}'.format(
                                                    dname, pred_type,
                                                    param_exp_str, param_cf_no) +\
                                                    'param{}_{}'.format(dir_suffix, j)
                                                wargs = ps.make_wargs(
                                                    rtype, k, par_wt, er, eta, br,
                                                    q, qrad, tr, trad, 0, 0, 0, 0,
                                                    c, pred_type, region,
                                                    par_region, 1, param_cf_no,
                                                    n_iter_sim, n_block_sim,
                                                    n_skipped_samp, ck,
                                                    param_exp_str, with_retropred,
                                                    dir_suffix_ck)
                                                add_file(wdir, wargs)
                                                jnames += [jname]
                                                j += 1
    # write pipeline file

    def add_job(j):
        for ck in xrange(0, n_chunks):
            f.write('jid{}=$(sbatch {}.submit)\n'.format(j, jnames[j]))
            j += 1
        return j

    with open(os.path.join(
            fc.code_dir, 'slurm', '{}_{}_{}_cf{}param_pipeline.sh'.format(
                dname, param_exp_str, dir_suffix, param_cf_no)), 'w') as f:
        f.write('#!/bin/bash\n')
        j = 0
        for pred_type in pred_types:
            qtruncs_iter, qrads_iter, trads_iter = qtruncs, qrads, trads
            for rtype in run_types:
                for k in config_nos:
                    for par_wt in param_wts:
                        for q in qtruncs_iter:
                            for qrad in qrads_iter:
                                for tr in ttruncs:
                                    for trad in trads_iter:
                                        for eta in eta_choices:
                                            for c in xrange(n_chains):
                                                j = add_job(j)


if __name__ == '__main__':
    param_region, run_types, param_wts = 'TX', ['long'], [.01]
    gen_suffix = '_no_vareps'
    #
    # Florida - historic spread
    region, param_exp_str = 'FL', 'hlb_only'
    pred_types = ['prospred_hlb']
    config_nos = [0]
    eta_choices = [0.8]
    param_cf_no = 1
    mblock = 3
    n_chains, n_iter_sim, n_block_sim, n_chunks = 3, 2, 2, 50
    n_sims = n_iter_sim * n_block_sim
    dir_suffix = '_s{}_no_control{}'.format(n_sims, gen_suffix)
    make_slurm_pipeline_file(
        run_types, param_wts, n_chains, n_chunks, config_nos, region,
        param_region, pred_types, param_exp_str, param_cf_no=param_cf_no,
        eta_choices=eta_choices, with_retropred=0, n_iter_sim=n_iter_sim,
        n_block_sim=n_block_sim, slevel=2, ltime='4:00:00',
        dir_suffix=dir_suffix, mblock=mblock,
    )
    # Florida - with control measures
    config_nos = [0]
    # 1. with coordinated spraying
    eta_choices = [0.2]
    n_chains, n_iter_sim, n_block_sim, n_chunks = 3, 2, 2, 50
    n_sims = n_iter_sim * n_block_sim
    dir_suffix = '_s{}_with_cspray{}'.format(n_sims, gen_suffix)
    make_slurm_pipeline_file(
        run_types, param_wts, n_chains, n_chunks, config_nos, region,
        param_region, pred_types, param_exp_str, param_cf_no=param_cf_no,
        eta_choices=eta_choices, with_retropred=0, n_iter_sim=n_iter_sim,
        n_block_sim=n_block_sim, slevel=2, ltime='4:00:00',
        dir_suffix=dir_suffix, mblock=mblock,
    )
    # 2. with HLB quarantine
    eta_choices = [0.8]
    qtruncs, qrads = [0.2], [8]
    n_chains, n_iter_sim, n_block_sim, n_chunks = 3, 2, 1, 100
    n_sims = n_iter_sim * n_block_sim
    dir_suffix = '_s{}_with_quarantines{}'.format(n_sims, gen_suffix)
    make_slurm_pipeline_file(
        run_types, param_wts, n_chains, n_chunks, config_nos, region,
        param_region, pred_types, param_exp_str, param_cf_no=param_cf_no,
        eta_choices=eta_choices, with_retropred=0, n_iter_sim=n_iter_sim,
        n_block_sim=n_block_sim, slevel=2, ltime='36:00:00', mblock=mblock,
        dir_suffix=dir_suffix, qtruncs=qtruncs, qrads=qrads,
    )
    # 3. with tree removal
    qtruncs, qrads = [0.], [0.]
    n_chains, n_iter_sim, n_block_sim, n_chunks = 3, 2, 2, 50
    n_sims = n_iter_sim * n_block_sim
    dir_suffix = '_s{}_with_tremoval2{}'.format(n_sims, gen_suffix)
    make_slurm_pipeline_file(
        run_types, param_wts, n_chains, n_chunks, config_nos, region,
        param_region, pred_types, param_exp_str, param_cf_no=param_cf_no,
        eta_choices=eta_choices, with_retropred=0, n_iter_sim=n_iter_sim,
        n_block_sim=n_block_sim, slevel=2, ltime='4:00:00', mblock=mblock,
        dir_suffix=dir_suffix, qtruncs=qtruncs, qrads=qrads,
    )
    # 4. with all 3 measures
    eta_choices = [0.2]
    qtruncs, qrads = [0.2], [8]
    n_chains, n_iter_sim, n_block_sim, n_chunks = 3, 2, 2, 50
    n_sims = n_iter_sim * n_block_sim
    dir_suffix = '_s{}_with_full_control2{}'.format(n_sims, gen_suffix)
    make_slurm_pipeline_file(
        run_types, param_wts, n_chains, n_chunks, config_nos, region,
        param_region, pred_types, param_exp_str, param_cf_no=param_cf_no,
        eta_choices=eta_choices, with_retropred=0, n_iter_sim=n_iter_sim,
        n_block_sim=n_block_sim, slevel=2, ltime='24:00:00', mblock=mblock,
        dir_suffix=dir_suffix, qtruncs=qtruncs, qrads=qrads,
    )
