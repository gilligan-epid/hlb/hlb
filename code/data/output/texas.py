import data.const as const
import data.input.texas as inp
import idm.plotting.mapmaker as mapmaker
import idm.plotting.plotmaker as plotmaker
import idm.plotting.moviemaker as moviemaker
from idm.simulation.struct import PopSim, PopLearn
from idm.model.dispersal import exponential_fn
from idm.plotting.colormap import YlGn_colors
import idm.simulation.struct as sstruct
import idm.model.struct as mstruct
from idm.const import fkernel_fn
import data.prepare.citrus as citrus

import os
import matplotlib.pyplot as plt
import datetime
import numpy as np
import numpy.random as nr
import sys
import pandas as pd
import cPickle as pickle
import seaborn as sns
import pyproj
from matplotlib import gridspec
import h5py
import rasterio


def make_data_maps(
        app_opts, figsize=None, inlib=inp, with_movies=False, corrgram=False,
        inset=False, mapscale=100, ms_loc='ll', msize=5, snap_dt=2, cropped=True):
    mapscale = 50 if app_opts.region == 'TX' else mapscale
    bmap = const.bmap(app_opts.region)
    bmap_cropped = const.bmap(app_opts.region, cropped=cropped)
    bmap = bmap_cropped if app_opts.region == 'CentralValley' else bmap
    mcolor = YlGn_colors()
    # Host density
    grid, hdens, tup = inlib.load_host(app_opts, with_covs=True)
    susc, infs = tup[0], tup[1]
    host_index = hdens
    if 'FL' in app_opts.region:
        ms_loc = 'ur'
        mapscale = 100
        msize = 7
    mapmaker.make_heatmap(
        host_index, grid, app_opts.region, const,
        os.path.join(app_opts.outdir, 'host_density.png'),
        vmax=1, bmap=bmap, figsize=figsize, mcolor=mcolor, mapscale=mapscale,
        cb_title='host density', shade='bright', cb_loc=0, ms_loc=ms_loc)
    if inset:
        region = app_opts.region
        mapmaker.make_inset_heatmap(
            host_index, grid, ['{}full'.format(region), region], const,
            os.path.join(app_opts.outdir, 'host_density_inset.png'),
            vmax=1, figsize=figsize, mcolor=mcolor)
    # Outbreak
    for dtype in app_opts.dtypes:
        df, traj = inlib.load_data(app_opts, dtype, with_traj=True)
        # sample histogram
        if dtype == 'acptrap':
            d0 = const.date0_acp[app_opts.region]
            dFobs = const.dateFobs_acp[app_opts.region]
            yrs = np.arange(d0.year, dFobs.year + 2)
        else:
            d0 = const.date0[app_opts.region]
            dFobs = const.dateFobs[app_opts.region]
            yrs = np.arange(d0.year, dFobs.year + 1)
        dp, pos_nos, neg_nos = 0, [], []
        for yr in yrs:
            d = (datetime.datetime(yr, 12, 31) - d0).days
            tmp = df[(df.TauNo >= dp) * (df.TauNo < d)]
            pos_nos += [tmp.PosNo.sum()]
            neg_nos += [tmp.SampNo.sum() - pos_nos[-1]]
            dp = d
        plotmaker.make_barplot(yrs, [pos_nos, neg_nos], os.path.join(
            app_opts.outdir, '{}_sample_histogram.png'.format(dtype)),
            labels=['pos', 'neg'])
        # range expansion
        df_pos = df[df.PosNo > 0]
        pos = bmap(df_pos.GLon.values, df_pos.GLat.values)
        mapmaker.make_scattermap(
            pos, grid, app_opts.region, const,
            os.path.join(app_opts.outdir, 'outbreak_{}_range.png'.format(
                dtype)), hvals=host_index, vmax=1, bmap=bmap, figsize=figsize,
            mcolor=mcolor, msize=msize)
        if len(traj[0]) > 0:
            plotmaker.make_trajplot(
                traj[0], traj[1], app_opts.dtau, const.date0[app_opts.region],
                os.path.join(
                    app_opts.outdir, 'outbreak_{}_range_dpc_{}.png'.format(
                        dtype, app_opts.dtau[0])))
        # make
        if corrgram:
            # Correlogram
            sw = pickle.load(open(app_opts.sw_file, "rb"))
            for binary in [True, False]:
                for omodel in ['Noulli', 'Nomial']:
                    plotmaker.make_correlogram(df, grid, sw, os.path.join(
                        app_opts.outdir, '{}_corgram_{}_b{}.png'.format(
                            dtype, omodel, int(binary))), omodel=omodel,
                        binary=binary, dplot=True)
        # Raw positives
        df_pos, df_neg = inlib.load_raw_positives(app_opts, dtype,
                                                  with_neg=True)
        pos = bmap_cropped(df_pos.Lon.values, df_pos.Lat.values)
        neg = bmap_cropped(df_neg.Lon.values, df_neg.Lat.values)\
            if df_neg is not None else None
        mapmaker.make_scattermap(
            pos, grid, app_opts.region, const,
            os.path.join(app_opts.outdir, 'outbreak_{}_point.png'.format(
                dtype)), hidx=host_index, vmax=1, bmap=bmap_cropped,
            figsize=figsize, mcolor=mcolor, neg=neg, msize=msize)
    assert False
    # Sequence map of raw positives
    ig, pos_snaps = make_snap_times(app_opts, snap_dt)
    for k in xrange(len(pos_snaps[1])):
        if app_opts.dtypes[0] == 'acptrap':
            mapmaker.make_spreadmap(
                pos_snaps[0], pos_snaps[1][k], grid, app_opts.region, const,
                os.path.join(
                    app_opts.outdir, 'outbreak_{}_spreadmap.png'.format(
                        app_opts.dtypes[k])), hidx=host_index, vmax=1,
                bmap=bmap, mcolor=mcolor, msize=2, alpha=1)
        else:
            mapmaker.make_spreadmap(
                pos_snaps[0], pos_snaps[1][k], grid, app_opts.region, const,
                os.path.join(
                    app_opts.outdir, 'outbreak_{}_spreadmap.png'.format(
                        app_opts.dtypes[k])), hidx=host_index, vmax=1,
                bmap=bmap, mcolor=mcolor, gsize=(2, 2), figsize=(15, 10),
                msize=20, alpha=1)
    # Covariates
    # maps
    tvec, sint, covs = inlib.make_covariates(
        app_opts, susc, infs, inlib.pick_covariates)
    msusc = covs[0].mean(1)
    print(msusc.min(), msusc.max())
    mapmaker.make_heatmap(msusc, grid, app_opts.region, const, os.path.join(
        app_opts.outdir, 'mean_susceptibility.png'), vmax=None, vmin=None,
        bmap=bmap, figsize=figsize)
    msusc *= hdens
    mapmaker.make_heatmap(msusc, grid, app_opts.region, const, os.path.join(
        app_opts.outdir, 'mean_susc_dens.png'), vmax=None, vmin=None,
        bmap=bmap, figsize=figsize)
    minfs = covs[1].mean(1)
    mapmaker.make_heatmap(minfs, grid, app_opts.region, const, os.path.join(
        app_opts.outdir, 'mean_infectiousness.png'), vmax=None, vmin=None,
        bmap=bmap, figsize=figsize)
    minfs *= hdens
    mapmaker.make_heatmap(minfs, grid, app_opts.region, const, os.path.join(
        app_opts.outdir, 'mean_infs_dens.png'), vmax=None, vmin=None,
        bmap=bmap, figsize=figsize)
    # plot
    fig, ax = plt.subplots()
    for i in xrange(covs[0].shape[0]):
        ax.step(tvec[:-1], covs[0][i, :], 'b', alpha=.5)
    fig.savefig(os.path.join(app_opts.outdir, 'susc_dyns.png'), dpi=200)
    # video
    if with_movies and sys.platform == 'win32':
        moviemaker.make_heatmovie(
            tvec[:-1], covs[0].T, grid, app_opts.region, const, os.path.join(
                app_opts.outdir, 'susc_dyns.avi'), vmax=1, bmap=bmap,
            hidx=hdens)
    plt.close("all")


def make_snap_times(
        app_opts, snap_dt=None, n_snaps=None, snap_points=[],
        inlib=inp, yr_range=False, cropped=True, ts=None, obs_only=False):
    print snap_dt, n_snaps, snap_points
    if snap_dt is None and n_snaps is None:
        return []
    bmap = const.bmap(app_opts.region, cropped=cropped)
    if app_opts.dtypes[0] == 'acptrap':
        date0 = const.date0_acp[app_opts.region]
    else:
        date0 = const.date0[app_opts.region]
    d0 = date0 + datetime.timedelta(app_opts.t0)
    dF = date0 + datetime.timedelta(app_opts.tF)
    dFobs = date0 + datetime.timedelta(app_opts.tFobs)
    dF_data = dFobs if obs_only else dF
    tF_data = app_opts.tFobs if obs_only else app_opts.tF
    print d0, dF, dFobs
    if snap_dt is not None:
        assert n_snaps is None
        yrs = np.arange(d0.year, dF.year + 1)
        # get positives for each year
        pos_lists = [[] for k in xrange(len(app_opts.dtypes))]
        for k in xrange(len(app_opts.dtypes)):
            df = inlib.load_raw_positives(app_opts, app_opts.dtypes[k])
            for i in xrange(len(yrs)):
                tmp = df[df.Year == yrs[i]]
                pos_lists[k] += [bmap(tmp.Lon.values, tmp.Lat.values)]
        snap_times = []
        data_snaps = [[pos_lists[k][0]] for k in xrange(len(pos_lists))]
        yr_strs = [yrs[0]]
        for i in xrange(0, len(yrs), snap_dt):
            snap_times += [(datetime.datetime(yrs[i], 12, 31) - d0).days]
            if i == 0:
                continue
            for k in xrange(len(pos_lists)):
                if yrs[i] <= dF_data.year:
                    pos_agg = list(pos_lists[k][i])
                    for j in xrange(1, snap_dt):
                        pos_agg[0] = np.append(
                            pos_agg[0], pos_lists[k][i - j][0])
                        pos_agg[1] = np.append(
                            pos_agg[1], pos_lists[k][i - j][1])
                    data_snaps[k] += [pos_agg]
                else:
                    data_snaps[k] += [None]
            if yr_range:
                yr_strs += ['{} - {}'.format(
                    yrs[i - (snap_dt - 1)], yrs[i] % 100)]
            else:
                yr_strs += ['Dec {}'.format(yrs[i])]
        snap_times, snap_strs = np.array(snap_times[:-1]), yr_strs
    else:
        months, yrs = [d0.month], [d0.year]
        if ts is None:
            tp_min = app_opts.tF if len(snap_points) == 0 else snap_points[0]
            ts = np.linspace(app_opts.t0, tp_min, n_snaps + 2)
        # ts = [1, 1030, 1699]
        for t in ts[1:-1]:
            d = date0 + datetime.timedelta(t)
            months += [d.month]
            yrs += [d.year]
        for t in snap_points:
            d = date0 + datetime.timedelta(t)
            months += [d.month]
            yrs += [d.year]
        months += [dF.month]
        yrs += [dF.year]
        #
        snap_times, month_strs = [], []
        for i in xrange(len(months)):
            m, y = months[i], yrs[i]
            if m == 12:
                y += 1
                m = 1
            else:
                m += 1
            d = datetime.datetime(y, m, 1) + datetime.timedelta(-1)
            snap_times += [(d - const.date0[app_opts.region]).days]
            month_str = d.strftime('%b %Y') if 'FL' not in app_opts.region else d.strftime('%Y')
            month_strs += [month_str]
        print months, yrs, snap_times
        print month_strs
        #
        data_snaps = [[] for k in xrange(len(app_opts.dtypes))]
        for k in xrange(len(app_opts.dtypes)):
            df_pos, df_neg = inlib.load_raw_positives(
                app_opts, app_opts.dtypes[k], with_neg=True)
            for t in snap_times:
                if t <= tF_data + 31:
                    tmp_pos = df_pos[df_pos.DayNo < t]
                    if df_neg is not None:
                        tmp_neg = df_neg[df_neg.DayNo < t]
                        h = [list(bmap(
                            tmp_pos.Lon.values, tmp_pos.Lat.values)),
                             list(bmap(
                                 tmp_neg.Lon.values, tmp_neg.Lat.values))]
                    else:
                        h = [list(bmap(
                            tmp_pos.Lon.values, tmp_pos.Lat.values)), [[], []]]
                    data_snaps[k] += [h]
                else:
                    data_snaps[k] += [None]
        snap_times, snap_strs = np.array(snap_times[:-1]), month_strs
    # prune out for repetitives
    n_snap = len(snap_times)
    snap_times, idx = np.unique(snap_times, return_index=True)
    if n_snap > len(snap_times):
        snap_strs = [snap_strs[i] for i in idx] + [snap_strs[-1]]
        for k in xrange(len(app_opts.dtypes)):
            data_snaps[k] = [data_snaps[k][i] for i in idx] + [
                data_snaps[k][-1]]
    return snap_times, (snap_strs, data_snaps)


def make_sim_maps(
        app_opts, prim_intros=[], yr0=2010, n_yr=8, corrgram=False,
        slib=PopSim, inlib=inp, mapscale=50, ms_loc='ll', cg_dband=30.,
        snap_dt=None, n_snaps=None, snap_points=[], cropped=True,
        with_tobs_mark=True, with_spreadmap=True, chunk_no=0, dpc_wtd=False,
        n_step_yr=1):
    grid, hdens = inlib.load_host(app_opts)
    # DPC
    date0 = const.date0_acp[app_opts.region] if app_opts.dtypes[0]\
        == 'acptrap' else const.date0[app_opts.region]
    dd0 = (datetime.datetime(yr0, 1, 1) - date0).days
    locs = np.arange(0, n_yr, n_step_yr)
    xticks = [locs, [str(l + yr0) for l in locs]]
    print app_opts.outdir
    sims = slib.load_sims(app_opts.outdir, 'by_time', prefix=app_opts.cprefix)
    T, S, D = sims['T'], sims['S'], sims['D']
    if dpc_wtd:
        sims = slib.load_sims(app_opts.outdir, 'by_time_wtd',
                              prefix=app_opts.cprefix)
        Sw, Dw = sims['S'], sims['D']
    T = (T - dd0) / 365.
    if app_opts.n_dcomp > 0:
        S[:, :, :- app_opts.n_dcomp] = np.cumsum(S[
            :, :, -1 - app_opts.n_dcomp::-1], 2)[:, :, ::-1]
        S[:, :, - app_opts.n_dcomp:] = np.cumsum(S[
            :, :, :- 1 - app_opts.n_dcomp:-1], 2)[:, :, ::-1]
        if dpc_wtd:
            Sw[:, :, :- app_opts.n_dcomp] = np.cumsum(Sw[
                :, :, -1 - app_opts.n_dcomp::-1], 2)[:, :, ::-1]
            Sw[:, :, - app_opts.n_dcomp:] = np.cumsum(Sw[
                :, :, :- 1 - app_opts.n_dcomp:-1], 2)[:, :, ::-1]
    else:
        S = np.cumsum(S[:, :, ::-1], 2)[:, :, ::-1]
        if dpc_wtd:
            Sw = np.cumsum(Sw[:, :, ::-1], 2)[:, :, ::-1]
    labels = app_opts.mlib.compartments
    if D is not None:
        S = np.concatenate([S, D[:, :, None]], axis=2)
        labels += ['D']
        if dpc_wtd:
            Sw = np.concatenate([Sw, Dw[:, :, None]], axis=2)
    #
    df, traj = inlib.load_data(
        app_opts, app_opts.dtypes[0], with_traj=True)
    traj[0] = (traj[0] - dd0) / 365.
    trajs = [traj]
    for dtype in app_opts.dtypes[1:]:
        ignore, traj = inlib.load_data(app_opts, dtype, with_traj=True)
        traj[0] = (traj[0] - dd0) / 365.
        trajs += [traj]
    data_labels = []
    for dtype in app_opts.dtypes:
        if dtype == 'plant' or dtype == 'hlb':
            data_labels += ['Reported HLB']
        elif dtype == 'psyllid':
            data_labels += ['Reported inconclusive ACP']
        elif dtype == 'acptrap':
            data_labels += ['Trapped ACP']
    tmark = (app_opts.tFobs - dd0) / 365. if with_tobs_mark else None
    plotmaker.make_fillplot(
        T, S, os.path.join(app_opts.outdir, '{}sim_dpc_ck{}.png'.format(
            app_opts.cprefix, chunk_no)), data=trajs, xticks=xticks,
        labels=labels, data_labels=data_labels, tmark=tmark,
        n_cells=len(hdens))
    if dpc_wtd:
        ignore, traj_wtd = inlib.load_data(
            app_opts, app_opts.dtypes[0], with_traj=True, traj_type=1)
        traj_wtd[0] = (traj_wtd[0] - dd0) / 365.
        plotmaker.make_fillplot(
            T, Sw, os.path.join(
                app_opts.outdir, '{}sim_dpc_wtd_ck{}.png'.format(
                    app_opts.cprefix, chunk_no)), data=[traj_wtd],
            xticks=xticks, labels=labels, data_labels=data_labels, tmark=tmark,
            n_cells=hdens.sum())
    # assert False
    # Correlogram
    ci = app_opts.mlib.n_comp - app_opts.n_dcomp - 1
    gfs = [3, 6]
    if corrgram:
        sw = pickle.load(open(os.path.join(
            const.input_path, app_opts.dname, 'sw_{}km.pkl'.format(
                cg_dband)), "rb"))
        sims = slib.load_sims(app_opts.outdir, 'by_space_dcount',
                              prefix=app_opts.cprefix)
        if sims is not None:
            for gf in gfs:
                plotmaker.make_correlogram(df, grid, sw, os.path.join(
                    app_opts.outdir, '{}scorr_gf{}_dcount_ck{}.png'.format(
                        app_opts.cprefix, gf, chunk_no)), sims=sims,
                    omodel='Noulli', binary=False, gf=gf)
                if dpc_wtd:
                    plotmaker.make_correlogram(df, grid, sw, os.path.join(
                        app_opts.outdir, '{}scorr_gf{}_dcount_wtd_ck{}.png'.format(
                            app_opts.cprefix, gf, chunk_no)), sims=sims,
                        omodel='Noulli', binary=False, gf=gf,
                        tree_wtd=True, hdens=hdens)
        sims = slib.load_sims(app_opts.outdir, 'by_space_{}'.format(ci),
                              prefix=app_opts.cprefix)
        for gf in gfs:
            plotmaker.make_correlogram(df, grid, sw, os.path.join(
                app_opts.outdir, '{}scorr_gf{}_{}_ck{}.png'.format(
                    app_opts.cprefix, gf, app_opts.mlib.compartments[ci],
                    chunk_no)),
                sims=sims, omodel='Noulli', binary=False, gf=gf)
            if dpc_wtd:
                plotmaker.make_correlogram(df, grid, sw, os.path.join(
                    app_opts.outdir, '{}scorr_gf{}_{}_wtd_ck{}.png'.format(
                        app_opts.cprefix, gf, app_opts.mlib.compartments[ci],
                        chunk_no)), tree_wtd=True, hdens=hdens,
                    sims=sims, omodel='Noulli', binary=False, gf=gf)
    # Spatial risk maps
    bmap = const.bmap(app_opts.region, cropped=cropped)
    df_init = df[(df.PosNo > 0) & (df.TauNo < app_opts.t0)]
    df_tmp = df.drop_duplicates('Gi')
    for gi in prim_intros:
        df_init = df_init.append(df_tmp[df_tmp.Gi == gi])
    for ci in xrange(app_opts.mlib.n_comp):
        sims = slib.load_sims(app_opts.outdir, 'by_space_{}'.format(ci),
                              prefix=app_opts.cprefix)
        risk_index = sims.mean(0)
        comp = app_opts.mlib.compartments[ci]
        mapmaker.make_heatmap(
            risk_index, grid, app_opts.region, const,
            os.path.join(app_opts.outdir, '{}{}_riskmap_ck{}.png'.format(
                app_opts.cprefix, comp, chunk_no)), vmax=1, bmap=bmap,
            hidx=hdens, mapscale=mapscale, ms_loc=ms_loc)
        if 'Vector' in comp:
            df_vec = inlib.load_data(app_opts, 'psyllid')
            print "init vector time", app_opts.t0 - app_opts.init_dt
            df_vec = df_vec[df_vec.TauNo < app_opts.t0 - app_opts.init_dt]
            if app_opts.pthres is not None:
                df_vec = df_vec[df_vec.PosNo > 0]
            init = bmap(df_vec.GLon.values, df_vec.GLat.values)
        else:

            init = bmap(df_init.GLon.values, df_init.GLat.values)
        mapmaker.make_scattermap(
            [[], []], grid, app_opts.region, const, os.path.join(
                app_opts.outdir, '{}{}_riskmap_with_init_ck{}.png'.format(
                    app_opts.cprefix, comp, chunk_no)),
            hvals=risk_index, vmax=1, bmap=bmap,
            init=init, hidx=hdens)
    sims = slib.load_sims(app_opts.outdir, 'by_space_dcount',
                          prefix=app_opts.cprefix)
    with_dcount = False
    if sims is not None:
        mapmaker.make_heatmap(
            sims.mean(0), grid, app_opts.region, const,
            os.path.join(app_opts.outdir, '{}dcount_riskmap_ck{}.png'.format(
                app_opts.cprefix, comp, chunk_no)), vmax=None, bmap=bmap,
            hidx=hdens, mapscale=mapscale, ms_loc=ms_loc)
        with_dcount = True
    with_dcount = False  # Temporary fix
    # Spread maps
    if with_spreadmap and (snap_dt is not None or n_snaps is not None):
        snap_times, data_snaps = make_snap_times(
            app_opts, snap_dt, n_snaps, snap_points, cropped=cropped)
        print snap_times
        cis = [0, 1, 2]  # Exposed and Cryptic only
        sim_snaps = [[] for ci in xrange(
            len(cis) + int(with_dcount))]
        labels = []
        for ci in cis:
            print ci
            for k in xrange(len(snap_times)):
                sims = slib.load_sims(
                    app_opts.outdir, 'by_space_{}_t{}'.format(
                        ci, snap_times[k]), prefix=app_opts.cprefix)
                sim_snaps[ci] += [sims.mean(0)]
                print snap_times[k], (sim_snaps[ci][-1] > 0).sum()
                mapmaker.make_heatmap(
                    sim_snaps[ci][-1], grid, app_opts.region, const,
                    os.path.join(app_opts.outdir, '{}{}_riskmap_{}.png'.format(
                        app_opts.cprefix, app_opts.mlib.compartments[ci], k)),
                    vmax=1, bmap=bmap, hidx=hdens, mapscale=mapscale,
                    ms_loc=ms_loc)
            sims = slib.load_sims(app_opts.outdir, 'by_space_{}'.format(ci),
                                  prefix=app_opts.cprefix)
            sim_snaps[ci] += [sims.mean(0)]
            labels += [app_opts.mlib.compartments[ci]]
        if with_dcount:
            for k in xrange(len(snap_times)):
                sims = slib.load_sims(
                    app_opts.outdir, 'by_space_dcount_t{}'.format(
                        snap_times[k]), prefix=app_opts.cprefix).clip(max=1)
                sim_snaps[-1] += [sims.mean(0)]
            sims = slib.load_sims(app_opts.outdir, 'by_space_dcount',
                                  prefix=app_opts.cprefix).clip(max=1)
            sim_snaps[-1] += [sims.mean(0)]
            labels += ['Predicted data']
        mapmaker.make_sim_spreadmap(
            data_snaps[0], sim_snaps, data_snaps[1], grid, app_opts.region,
            const, os.path.join(app_opts.outdir, '{}spreadmap_ck{}.png'.format(
                app_opts.cprefix, chunk_no)), hidx=hdens, vmax=1, bmap=bmap,
            figsize=(18, 10), msize=5, alpha=1, labels=labels, fontsize=18,
            data_labels=['positive {}'.format(d) for d in app_opts.dtypes])
    plt.close("all")


def make_sim_movies(app_opts, iI=None, clist=[0, 1],
                    hobs=True, srun=False, slib=PopSim, inlib=inp, nf=None):
    tau = app_opts.dtau[1]
    grid, hdens = inlib.load_host(app_opts)
    bmap = const.bmap(app_opts.region)
    T = slib.load_sims(
        app_opts.outdir, 'by_time', prefix=app_opts.cprefix)['T']
    T = np.append(T[::tau], T[-1])
    T = T.astype(int)
    # construct obs
    if hobs:
        pos_list = []
        for dtype in app_opts.dtypes:
            df = inlib.load_raw_positives(app_opts, dtype)
            pos = [[[], []]]
            for i in xrange(1, len(T)):
                points = df[(df.DayNo <= T[i]) & (df.DayNo > T[i - 1])]
                if points.empty:
                    pos += [[[], []]]
                    continue
                points = bmap(points.Lon.values, points.Lat.values)
                pos += [[points[0], points[1]]]
            pos_list += [pos]
        # mean infection intensity
        iI = app_opts.mlib.iI if iI is None else iI
        S = slib.load_sims(app_opts.outdir, 'mean_full_{}'.format(iI),
                           prefix=app_opts.cprefix)
        S = np.vstack([S[::tau, :], S[-1, :]])
        moviemaker.make_heat_vs_data_movie(
            T, S, pos_list[0], grid, app_opts.region, const,
            os.path.join(app_opts.outdir, '{}{}_mean_vs_data_month.avi'.format(
                app_opts.cprefix, app_opts.mlib.compartments[iI])), vmin=0.01,
            vmax=1, bmap=bmap, hidx=hdens, old_dt=int(60 / tau) + 1, nf=nf)
        if len(pos_list) > 1:
            assert app_opts.dtypes[1] == 'psyllid'
            ci = app_opts.mlib.n_comp - 1
            S = slib.load_sims(app_opts.outdir, 'mean_full_{}'.format(ci),
                               prefix=app_opts.cprefix)
            S = np.vstack([S[::tau, :], S[-1, :]])
            moviemaker.make_heat_vs_data_movie(
                T, S, pos_list[0], grid, app_opts.region, const,
                os.path.join(app_opts.outdir, '{}{}_mean_vs_data.avi'.format(
                    app_opts.cprefix, app_opts.mlib.compartments[ci])),
                vmin=0.01, vmax=1, bmap=bmap, hidx=hdens,
                old_dt=int(60 / tau) + 1, nf=nf)

    # Exposed and Cryptic
    for ci in clist:
        S = slib.load_sims(app_opts.outdir, 'mean_full_{}'.format(ci),
                           prefix=app_opts.cprefix)
        S = np.vstack([S[::tau, :], S[-1, :]])
        fname = '{}{}_mean.avi'.format(
            app_opts.cprefix, app_opts.mlib.compartments[ci])
        moviemaker.make_heatmovie(
            T, S, grid, app_opts.region, const,
            os.path.join(app_opts.outdir, fname),
            vmax=1, bmap=bmap, hidx=hdens, nf=nf)
        print fname, "DONE."
    # single runs
    if srun:
        pass


def get_inf_prob(x, alpha, beta, eta, delta, freq_depend):
    hR = beta * exponential_fn(x, alpha, norm=True)
    if freq_depend:
        yR = 1 - np.exp(- hR * 365)
        yC = 1 - np.exp(- hR * eta * 365)\
            if eta is not None else None
        yV = 1 - np.exp(- hR * delta * 365)\
            if delta is not None else None
    else:
        rdR = 0.035
        yR = 1 - np.exp(- hR * rdR * 365)
        yC = 1 - np.exp(- hR * (eta ** 2) * 365)\
            if eta is not None else None
        yV = 1 - np.exp(- hR * rdR * delta * 365)\
            if delta is not None else None
    return [yR, yC, yV]


def get_inf_prob_spectrum(x, alpha, beta, eta, hj, fr=1000, kf='linear'):
    fc = np.linspace(0, 1, fr)
    if kf == 'linear':
        kj = eta + (1 - eta) * (1 - fc)
    elif kf == 'sigmoid':
        kj = eta + (1 - eta) / (1 + np.exp(- 10 * (0.5 - fc)))
    else:
        assert False, kf
    print eta, kj.min(), kj.max()
    # assert False
    hR = beta * hj * kj[:, None] * exponential_fn(x, alpha, norm=True)
    return 1 - np.exp(- hR * 365)


def make_disperse_compare_plots(
        app_opts, param_dists, labels, freq_depend, n_iter=None, axon=None,
        dmax=10, figsize=None):
    wD = ('delta' in param_dists[0])
    n_samp = len(param_dists[0]['alpha'])
    n_iter = n_samp if n_iter is None else n_iter
    assert n_iter <= n_samp, (n_iter, n_samp)
    js = nr.choice(n_samp, n_iter, replace=False)
    #
    fig, ax = plt.subplots(
        2 + int(wD), len(param_dists), figsize=figsize,
        sharex=True, sharey=True)
    axon = np.ones(ax.shape) if axon is None else axon
    for i in xrange(axon.shape[0]):
        for j in xrange(axon.shape[1]):
            if axon[i, j] == 0:
                ax[i, j].axis('off')
    x = np.linspace(0, dmax, 1000)
    for i, pdist in enumerate(param_dists):
        yR_samps, yC_samps, yV_samps = [], [], []
        for j in js:
            delta = pdist['delta'][j] if wD else None
            yR, yC, yV = get_inf_prob(
                x, pdist['alpha'][j], pdist['beta'][j], pdist['eta'][j, 1],
                delta, freq_depend)
            yR_samps += [yR[:, None]]
            yC_samps += [yC[:, None]]
            if wD:
                yV_samps += [yV[:, None]]
        ax[0, i].set_title(labels[i])
        if axon[0, i]:
            yC_samps = np.array(yC_samps)
            plotmaker.make_fillplot(x, yC_samps, None, ax=ax[0, i])
            # ym = yC_samps[:, :, 0].mean(0)
            # y = x[np.argmax(ym <= 0.01)]
            # ax[0, i].axvline(y, ymax=0.35, color='k', linewidth=1)
        if axon[1, i]:
            yR_samps = np.array(yR_samps)
            plotmaker.make_fillplot(x, yR_samps, None, ax=ax[1, i])
            # ym = yR_samps[:, :, 0].mean(0)
            # y = x[np.argmax(ym <= 0.01)]
            # ax[1, i].axvline(y, ymax=0.35, color='k', linewidth=1)
            ax[1, i].set_xlabel('km')
        if wD and axon[2, i]:
            plotmaker.make_fillplot(x, np.array(yV_samps), None, ax=ax[2, i])
    ax[0, 0].set_ylabel('From an orchard site')
    ax[1, 0].set_ylabel('From an urban site')
    # plt.suptitle("Probability of dispersal over distance per year")
    plt.tight_layout()
    fig.savefig(os.path.join(
        app_opts.outdir, '{}disperse_distribution_compare.png'.format(
            app_opts.cprefix)), dpi=100)


def make_param_compare_plots(
        app_opts, param_dists, labels, freq_depend, dmax=10):
    # dispersal plot
    fig, ax = plt.subplots(2, 1, sharex=True)
    x = np.linspace(0, dmax, 1000)
    for i, pdist in enumerate(param_dists):
        alpha = pdist['alpha'].mean()
        beta = pdist['beta'].mean()
        eta = pdist['eta'].mean()
        delta = pdist['delta'].mean() if 'delta' in pdist else None
        yR, yC, yV = get_inf_prob(x, alpha, beta, eta, delta, freq_depend)
        ax[0].plot(x, yR, label=labels[i])
        if yC is not None:
            ax[1].plot(x, yC, label=labels[i])
        if yV is not None:
            ax[1].plot(x, yV, label='{} vector'.format(labels[i]))
    ax[0].set_title('Residential')
    ax[0].legend()
    ax[1].set_title('Commercial')
    ax[1].legend()
    # plt.suptitle('Probability of dispersal over distance per year')
    plt.tight_layout()
    fig.savefig(os.path.join(
        app_opts.outdir, '{}disperse_prob_compare.png'.format(
            app_opts.cprefix)), dpi=100)
    # box plots
    pkeys = param_dists[0].keys()
    n_par = len(pkeys)
    a = 3 if n_par % 3 == 0 else 2 if n_par % 2 == 0 else 1
    fig, ax = plt.subplots(a, n_par / a, sharex=True)
    for k in xrange(n_par):
        samps = [pdist[pkeys[k]] for pdist in param_dists]
        i, j = k / a, k % a
        ax[i, j].boxplot(samps, labels=labels)
        ax[i, j].set_title(r'$\{}$'.format(pkeys[k]))
    plt.suptitle('Posterior distributions')
    plt.tight_layout()
    fig.savefig(os.path.join(
        app_opts.outdir, '{}pdist_compare.png'.format(
            app_opts.cprefix)), dpi=100)


def make_param_plots(app_opts, param_dist, freq_depend, dmax=10,
                     trace=False, init_map=False, rescale=True):
    alpha = param_dist['alpha'].mean()
    beta = param_dist['beta'].mean()
    delta = param_dist['delta'].mean() if 'delta' in param_dist else None
    eta = param_dist['eta'].mean()
    #
    x = np.linspace(0, dmax, 1000)
    yR, yC, yV = get_inf_prob(x, alpha, beta, eta, delta, freq_depend)
    dx = x[1] - x[2]
    zR = yR.cumsum() * dx
    zC = yC.cumsum() * dx if yC is not None else None
    zV = yV.cumsum() * dx if yV is not None else None
    #
    # dispersal probability per year
    fig, ax = plt.subplots()
    plt.tight_layout()
    plt.plot(x, yR, label='HLB from a residential tree')
    if yC is not None:
        plt.plot(x, yC, label='HLB from an orchard tree')
    if yV is not None:
        plt.plot(x, yV, label='ACP')
    plt.xlabel("km", fontsize=12)
    plt.title("Probability of dispersal over distance per year")
    plt.legend()
    fig.savefig(os.path.join(app_opts.outdir, '{}disperse_prob.png'.format(
        app_opts.cprefix)), dpi=100)
    # dispersal cumulative probability
    fig, ax = plt.subplots()
    plt.plot(x, zR / zR[-1], label='HLB residential')
    plt.plot(x, zC / zC[-1], label='HLB commercial')
    if zV is not None:
        plt.plot(x, zV / zC[-1], label='ACP')
    plt.ylim([0, 1.05])
    plt.xlabel("km", fontsize=12)
    plt.title("Cumulative probability of dispersal per year")
    plt.legend()
    i = np.argmax(zR >= 0.95)
    ax.annotate('95% HLB within {:1.1f}km'.format(x[i]),
                xy=(x[i], zR[i]), xytext=(x[i] + 2, zR[i] - 0.25),
                horizontalalignment='center', arrowprops=dict(
                    facecolor='black', shrink=0.05, width=1, headwidth=5))
    fig.savefig(os.path.join(app_opts.outdir, '{}disperse_cumprob.png'.format(
        app_opts.cprefix)), dpi=100)

    # trace plots
    if trace:
        params = param_dist.keys()
        fig, ax = plt.subplots(
            nrows=len(params), figsize=(7.5, len(params) * 2.))
        plt.tight_layout()
        for i, par in enumerate(params):
            print par, param_dist[par].shape
            ax[i].plot(param_dist[par].flatten(), label=r'$\{}$'.format(par))
            ax[i].legend(loc='best')
        fig.savefig(os.path.join(app_opts.outdir, '{}traceplot.png'.format(
            app_opts.cprefix)), dpi=100)
    # initial Exposed and Cryptic sites
    if init_map:
        grid, hdens = inp.load_host(app_opts)
        init_samps = PopLearn.load_sims(
            app_opts.paramdir, 'at_0', prefix=app_opts.cprefix)
        for ci in [0, 1]:
            vals = init_samps[:, :, ci].mean(0)
            mapmaker.make_heatmap(
                vals, grid, app_opts.region, const, os.path.join(
                    app_opts.outdir, '{}{}_initmap.png'.format(
                        app_opts.cprefix, app_opts.mlib.compartments[ci])),
                vmax=1, bmap=const.bmap(app_opts.region), hidx=hdens)


def load_params(app_opts, param_list, chain_nos):
    param_dists, init_samps = [], []
    for c in chain_nos:
        cstr = 'c{}_'.format(c)
        param_dists += [sstruct.PopLearn.load_params(
            app_opts.paramdir, param_list, prefix=cstr)]
        init_samps += [sstruct.PopLearn.load_sims(
            app_opts.paramdir, 'at_0', prefix=cstr)]
    init_samps = np.concatenate(init_samps, axis=0)
    nc = len(param_dists)
    param_dist = {}
    for p in param_list:
        param_dist[p] = np.concatenate([
            param_dists[i][p] for i in xrange(nc)], axis=0)
    return param_dist, init_samps


def make_param_pdist_plot(
        app_opts, cf=None, chain_nos=[0, 1, 2], rescale=False, bw='scott',
        with_prior=False):
    if with_prior:
        assert cf is not None
        param0 = {
            'epsilon': cf['epsilon'], 'alpha': cf['alpha'], 'beta': cf['beta'],
            'gamma': cf['gamma'], 'sigma': cf['sigma'], 'dprob': cf['dprob'],
            'eta': cf['eta'], 'varepsilon': cf['varepsilon'][1], 'mu': cf['mu']}
    if len(app_opts.dtypes) > 1:
        param0['delta'] = cf['delta']
    smod_opts = mstruct.ModelOptions(
        param0=param0, param_wbins={
            'gamma': False, 'varepsilon': False, 'delta': False},
        dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
    )
    smodel = inp.load_popmodel(app_opts, smod_opts, False)
    # load data
    param_dist, init_samps = load_params(app_opts, param_list, chain_nos)
    vareps = param_dist.pop('varepsilon', None)
    assert vareps is not None
    param_dist['varepsilon_1'] = vareps[:, 0]
    param_dist['varepsilon_2'] = vareps[:, 1]
    param_dist['eta'] = param_dist['eta']
    # if len(app_opts.dtypes) > 1:
    #     param_dist['dprob'] = param_dist['dprob'][:, 0]
    if rescale:
        alphas = param_dist['alpha']
        n_samp = len(alphas)
        Z = np.array([fkernel_fn[cf['dkernel']](0, alphas[i], norm=True)
                      for i in xrange(n_samp)])
        param_dist['beta'] = param_dist['beta'] / Z
        param_dist['eta'] = (1 - param_dist['eta']) * 100
        # param_dist['dprob'] = param_dist['dprob'] * 100
        param_dist['beta'] = param_dist['beta']
    for k in param_dist.keys():
        print k, param_dist[k].shape
    param_df = pd.DataFrame(param_dist)
    units = {k: '' for k in param_dist.keys()}
    units['alpha'] = '(km)'
    units['eta'] = '(%)'
    # units['dprob'] = '(%)'
    units['beta'] = '(day$^{-1}$)'
    units['varepsilon_1'] = '(day$^{-1}$)'
    units['varepsilon_2'] = '(day$^{-1}$)'
    units['epsilon'] = '(day$^{-1}$)'
    plotmaker.make_param_histogram(param_df, smodel.params, os.path.join(
        app_opts.outdir, 'param_pdist_p{}.png'.format(int(with_prior))),
        pnames=['alpha', 'beta', 'eta', 'epsilon', 'varepsilon_1',
                'varepsilon_2'], gsize=(2, 3), units=units, eps_scale=4,
        with_prior=with_prior)
    plotmaker.make_cornerplot(param_df, os.path.join(
        app_opts.outdir, 'param_cornerplot.png'))
    # initial Exposed
    d0 = const.date0[app_opts.region] + datetime.timedelta(
        days=smodel.pop.tvec[0])
    mapmaker.make_heatmap(
        init_samps[:, :, 0].mean(0), smodel.pop.grid, app_opts.region, const,
        os.path.join(app_opts.outdir, 'Exposed_initmap.png'), vmax=1,
        bmap=const.bmap(app_opts.region), hidx=smodel.pop.hdens,
        title=d0.strftime('%B, %Y'), cb_title='exposure probability')


def load_param_df(app_opts, param_list, chain_nos, rescale,
                  border_only=False):
    # get samples from all chains
    pdist_list = []
    for c in chain_nos:
        cstr = 'c{}_'.format(c)
        pdist_list += [sstruct.PopLearn.load_params(
            app_opts.paramdir, param_list, prefix=cstr)]
    # merge samples
    pdist = {}
    for par in param_list:
        if pdist_list[0][par] is None:
            print 'here'
            pdist[par] = None
            continue
        pdist[par] = np.concatenate([
            pdist_list[ic][par] for ic in xrange(len(chain_nos))], axis=0)
    # separate varepsilon
    vareps = pdist.pop('varepsilon', None)
    if vareps is not None:
        if not border_only:
            pdist['varepsilon_W'] = vareps[:, 0]
            pdist['varepsilon_B'] = vareps[:, 1]
        else:
            pdist['varepsilon'] = vareps[:, 1]  # border_only
    # eta
    if 'eta' in pdist.keys() and pdist['eta'] is not None:
        pdist['eta'] = (1 - pdist['eta']) * 100
    # sigma
    if 'sigma' in pdist.keys() and pdist['sigma'] is not None:
        pdist['sigma'] = 1. / pdist['sigma'] + 1. / pdist['drate']
    # delta
    if 'delta' in pdist.keys() and pdist['delta'] is not None:
        pdist['delta'] = pdist['delta'] * pdist['beta']
    # dprob
    if 'dprob' in pdist.keys():
        pdist['dprob'] = pdist['dprob'] * 100
        if len(pdist['dprob'].shape) > 1:
            pdist['dprob'] = pdist['dprob'][:, 0]
    # rescale if required
    if rescale:
        Z = 2 * np.pi * (pdist['alpha'] * pdist['alpha'])
        pdist['mu'] = pdist['mu'] * Z
    return pd.DataFrame(pdist)


def make_param_pdist_compare_plot(
        app_opts_list, pnames, gsize, chain_nos=[0, 1, 2], rescale=False,
        bw='scott', labels=None, with_prior=False, cf=None, fname=None):
    fname = 'param_pdist_compare' if fname is None else fname
    if with_prior:
        assert cf is not None
        param0 = {
            'epsilon': cf['epsilon'], 'alpha': cf['alpha'], 'beta': cf['beta'],
            'gamma': cf['gamma'], 'sigma': cf['sigma'], 'dprob': cf['dprob'],
            'eta': cf['eta'], 'varepsilon': cf['varepsilon'][1],
            'mu': cf['mu']}
        if len(app_opts_list[-1].dtypes) > 1:
            param0['delta'] = cf['delta']
        smod_opts = mstruct.ModelOptions(
            param0=param0, param_wbins={
                'gamma': False, 'varepsilon': False, 'delta': False},
            dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
        )
        smodel = inp.load_popmodel(app_opts_list[-1], smod_opts, False)
    param_list = []
    for p in pnames:
        if len(p.split('_')) > 1:
            p = p.split('_')[0]
            if p in param_list:
                continue
        param_list += [p]
        if p == 'sigma':
            param_list += ['drate']
    units = {}
    for i, par in enumerate(pnames):
        if par == 'alpha':
            units[par] = '(km)'
        elif par == 'eta' or par == 'dprob':
            units[par] = '(%)'
        elif par == 'mu':
            units[par] = ''
        elif par == 'sigma':
            units[par] = '(days)'
        else:
            units[par] = '(day$^{-1}$)'
    eps_scale = 4
    #
    param_df_list = [load_param_df(app_opts, param_list, chain_nos, rescale)
                     for app_opts in app_opts_list]
    params = smodel.params if with_prior else None
    plotmaker.make_param_histogram(param_df_list, params, os.path.join(
        const.output_path, app_opts_list[0].dname, '{}.png'.format(fname)),
        gsize=gsize, units=units, eps_scale=eps_scale, with_prior=with_prior,
        with_hist=False, bw=bw, pnames=pnames, figsize=(12, 8), fontsize=16,
        labels=labels)


def make_kfield_map(
        app_opts, cf, chain_nos=[0, 1, 2], fname=None, inf_points='init'):
    fname = 'kfield' if fname is None else fname
    # load param posterior
    param_list = ['epsilon', 'alpha', 'beta', 'sigma', 'dprob',
                  'eta', 'varepsilon', 'mu']
    if len(app_opts.dtypes) > 1:
        param_list += ['delta']
    param_dist, init_samps = load_params(app_opts, param_list, chain_nos)
    # construct model from posterior medians
    param_meds = {par: np.percentile(param_dist[par], 50, axis=0)
                  for par in param_list}
    param_meds['gamma'] = cf['gamma']
    smod_opts = mstruct.ModelOptions(
        param0=param_meds, param_wbins=cf['param_wbins'],
        dkernel=cf['dkernel'], freq_depend=cf['freq_depend'],
    )
    smodel = inp.load_popmodel(app_opts, smod_opts, False)
    #
    fig, ax = plt.subplots()
    d = np.linspace(0, 15, 100)
    plt.plot(d, np.exp(-d / smodel.params['alpha'].value))
    fig.savefig(os.path.join(const.output_path, 'alpha_span.png'))
    assert False
    # force of infection map
    state = np.zeros((app_opts.mlib.n_comp, smodel.pop.n_pop))
    state[0, :] = 1
    if inf_points == 'random':
        ri = [100, 655, 1500]
    else:
        cr = init_samps.mean(0).sum(1)
        ri = np.argwhere(cr > 0.75)
    state[1, ri] = 1
    state[0, ri] = 0
    pop = smodel.pop
    Cf = pop.covs[1][:, 0] * state[1:, :].sum(0)
    e2_r = pop.covs[0][:, 0] * smodel.params['beta'].value * Cf * pop.Ksp
    e1_r = smodel.params['epsilon'].value * np.ones_like(e2_r)
    rr = e1_r.sum()
    print 'random', rr
    wi = (pop.base[0] == 0)
    e1_r[wi] += pop.covs[0][wi, 0] * smodel.params['varepsilon'].value[0, 0]
    bi = (pop.base[0] == 1)
    e1_r[bi] += pop.covs[0][bi, 0] * smodel.params['varepsilon'].value[1, 0]
    rr_all = e1_r.sum()
    print 'within', (pop.covs[0][wi, 0] * smodel.params['varepsilon'].value[0, 0]).sum()
    print 'border', (pop.covs[0][wi, 0] * smodel.params['varepsilon'].value[0, 0]).sum()
    e1_r = state[0, :] * e1_r
    e2_r = state[0, :] * e2_r
    er = e2_r + e1_r
    vmin, vmax = 2e-5, er.max()
    bmap = const.bmap(app_opts.region, cropped=True)
    grid, hdens = pop.grid, pop.hdens
    orig = pyproj.Proj(init='epsg:4629')
    dest = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    gx, gy = [], []
    for gi in ri:
        xi, yi = grid.ixy[0][gi], grid.ixy[1][gi]
        gx += [grid.bins[0][xi]]
        gy += [grid.bins[1][yi]]
    rlon, rlat = pyproj.transform(dest, orig, gx, gy)
    pos = bmap(rlon, rlat)
    mapmaker.make_heatmap(
        e2_r, grid, app_opts.region, const, os.path.join(
            app_opts.outdir, '{}_sec.png'.format(fname)), bmap=bmap,
        hidx=hdens, vmin=vmin, vmax=vmax, cb_loc=1, cb_fontsize=16)
    mapmaker.make_heatmap(
        e1_r, grid, app_opts.region, const, os.path.join(
            app_opts.outdir, '{}_prim.png'.format(fname)), bmap=bmap,
        hidx=hdens, vmin=vmin, vmax=vmax, cb_loc=1, cb_fontsize=16)
    mapmaker.make_heatmap(
        er, grid, app_opts.region, const, os.path.join(
            app_opts.outdir, '{}_all.png'.format(fname)), bmap=bmap,
        hidx=hdens, vmax=vmax, vmin=vmin, cb_loc=1,
        cb_title='inf. pressure', init=pos)


def make_disperse_prob_spectrum_map(
        app_opts, param_list, cf, chain_nos=[0, 1, 2], dmax=15):
    # load parameter posterior
    param_dist, init_samps = load_params(app_opts, param_list, chain_nos)
    # posterior medians
    param_meds = {par: np.percentile(param_dist[par], 50, axis=0)
                  for par in param_list}
    #
    x = np.linspace(0, dmax, 1000)
    for kf in ['linear', 'sigmoid']:
        slist = [get_inf_prob_spectrum(
            x, param_meds['alpha'], param_meds['beta'],
            param_meds['eta'][0], hj, kf=kf) for hj in [1., 0.1]]
        plotmaker.make_dprob_spectrum(x, slist, os.path.join(
            app_opts.outdir, 'dprob_{}.png'.format(kf)), labels=[
                'A', 'B'], cb_title='grove fraction')


def make_map_layers(app_opts, oi):
    grid, hdens = inp.load_host(app_opts)
    sims = PopSim.load_sims(app_opts.outdir, 'by_space_{}'.format(oi))
    risk = np.zeros(grid.shape)
    risk[grid.ixy[:, 0], grid.ixy[:, 1]] = sims.mean(0)
    # read header
    fname = os.path.join(const.raw_path, 'Host', 'TexasCitrus',
                         'Hidalgo_Extent_{}.txt'.format(app_opts.resol))
    with open(fname) as f:
        header = f.read().rstrip()
    #
    fname = os.path.join(app_opts.outdir, 'infection_risk.txt')
    np.savetxt(fname, risk.T, fmt='%.18g', header=header, comments='')


def write_config_file(app_opts, opts, ctype):
    fname = os.path.join(app_opts.outdir, 'config_{}.txt'.format(ctype))
    with open(fname, 'w') as f:
        f.write("=== APPLICATION ===\n")
        for key in ['n_maxhost', 't0', 'tF', 'init_dt', 'dtypes', 'dthres',
                    'with_weather', 'vector_cov', 'init_source']:
            f.write("{}:\t{}\n".format(key, vars(app_opts)[key]))
        f.write("\n")

        f.write("=== MODEL ===\n")
        opts = vars(opts)
        for key in opts:
            if type(opts[key]) == dict:
                f.write("{}:\n".format(key))
                for a in opts[key]:
                    if key == 'param_dist':
                        f.write("\t{}[0]:\t{}\n".format(
                            a, opts[key][a][0]))
                    else:
                        f.write("\t{}:\t{}\n".format(a, opts[key][a]))
            else:
                f.write("{}:\t{}\n".format(key, opts[key]))
        f.write("\n")


def make_roc_plots(
        app_opts, tFobs=None, inlib=inp, slib=PopSim, dmetric=1, chunk_no=0):
    labels = app_opts.mlib.compartments[
        :app_opts.mlib.n_comp - app_opts.n_dcomp]
    risk_indices = []
    for ci in xrange(app_opts.mlib.n_comp - app_opts.n_dcomp):
        # print app_opts.outdir, app_opts.cprefix
        sims = slib.load_sims(app_opts.outdir, 'by_space_{}'.format(ci),
                              prefix=app_opts.cprefix)
        risk_indices += [sims.mean(0)]
    sims = slib.load_sims(app_opts.outdir, 'by_space_dcount',
                          prefix=app_opts.cprefix)
    if sims is not None:
        risk_indices += [sims.mean(0)]
        labels += ['Detectable']
    #
    risk_indices = np.vstack(risk_indices).T
    dataF = np.zeros(risk_indices.shape[0])
    df = inlib.load_data(app_opts, app_opts.dtypes[0])
    df = df[df.TauNo < app_opts.tF]
    if dmetric == 0:
        # presence = at least 1 positive sample (or day)
        df0 = df[df.PosNo > 0]
        pos_inds = df0.Gi.unique()
        dataF[pos_inds] = 1
    elif dmetric == 1:
        # presence prob = number of positive days
        for i in xrange(len(dataF)):
            df1 = df[df.Gi == i]
            df1 = df1.groupby(['TauNo']).agg({'PosNo': max})
            dataF[i] = df1.PosNo.values.clip(max=1).sum()
        dmean = dataF[dataF > 0].mean()
        print dataF.max(), dataF.min(), dmean
        dataF = (dataF > dmean).astype(int)
    elif dmetric == 2:
        # presence prob = number of positive samples
        df = df[df.TauNo < app_opts.tF]
        for i in xrange(len(dataF)):
            df1 = df[df.Gi == i]
            dataF[i] = df1.PosNo.values.sum()
        dmean = dataF[dataF > 0].median()
        print dataF.max(), dataF.min(), dmean
        dataF = (dataF > dmean).astype(int)
    print "No. positive cells:", dataF.sum()
    plotmaker.make_roc_plot(
        risk_indices, dataF, os.path.join(
            app_opts.outdir, '{}roc_upto_tF_m{}_ck{}.png'.format(
                app_opts.cprefix, dmetric, chunk_no)), labels=labels)
    if tFobs is not None:
        assert tFobs < app_opts.tF, (tFobs, app_opts.tF)
        # account for samples collected in the period (tFobs, tF) only
        df1 = df[df.TauNo >= tFobs]
        surv_inds = df1.Gi.unique()
        """df_old = df[df.TauNo < tFobs]
        inds = df_old.Gi.unique()
        data_new = dataF.copy()
        for j in inds:
            if dataF[j] > 0:
                data_new[j] = -1
        # data_new[inds] = -1
        surv_inds = (data_new >= 0)"""
        obs = np.zeros_like(dataF)
        if dmetric == 0:
            obs[df1[df1.PosNo > 0].Gi.unique()] = 1
        elif dmetric == 1:
            for j in xrange(len(obs)):
                # j = surv_inds[i]
                df2 = df1[df1.Gi == j]
                df2 = df2.groupby(['TauNo']).agg({'PosNo': max})
                obs[j] = df2.PosNo.values.clip(max=1).sum()
            dmean = obs[obs > 0].mean()
            print dmean
            obs = (obs > dmean).astype(int)
        elif dmetric == 2:
            for j in xrange(len(obs)):
                # j = surv_inds[i]
                df2 = df1[df1.Gi == j]
                obs[j] = df2.PosNo.values.sum()
            dmean = obs[obs > 0].mean()
            print dmean
            obs = (obs > dmean).astype(int)
        risk, obs = risk_indices[surv_inds], obs[surv_inds]
        print "No. positive cells:", obs.sum()
        plotmaker.make_roc_plot(risk, obs, os.path.join(
            app_opts.outdir, '{}roc_from_tFobs_to_tF_m{}_ck{}.png'.format(
                app_opts.cprefix, dmetric, chunk_no)), labels=labels)


def make_multimodel_roc_plot(
        app_opts_list, labels, dcount, inlib=inp, slib=PopSim, ci=2,
        fname='roc_upto_tFobs', chain_nos=[0, 1, 2], title=None,
        after_tobs=False, dmetric=1):
    #
    grid, hdens = inlib.load_host(app_opts_list[0])
    risk_indices = []
    for i, app_opts in enumerate(app_opts_list):
        print i, app_opts.tF
        sims = []
        print app_opts.outdir
        for c in chain_nos:
            cprefix = 'c{}_'.format(c)
            dcount_check = dcount[i] if type(dcount) == list else dcount
            if dcount_check:
                sims += [slib.load_sims(
                    app_opts.outdir, 'by_space_dcount', prefix=cprefix)]
                assert sims[-1] is not None
            else:
                sims += [slib.load_sims(
                    app_opts.outdir, 'by_space_{}'.format(ci), prefix=cprefix)]
        sims = np.concatenate(sims, axis=0)
        risk_indices += [sims.mean(0)]
    risk_indices = np.vstack(risk_indices).T
    #
    dataF_list = []
    for app_opts in app_opts_list:
        dataF = np.zeros(risk_indices.shape[0])
        df = inlib.load_data(app_opts, app_opts.dtypes[0])
        df = df[df.TauNo < app_opts.tF]
        if after_tobs:
            print "Before tobs", df.shape
            df = df[df.TauNo >= app_opts.tFobs]
            print "After tobs", df.shape
        if dmetric == 0:
            # presence prob = 1 positive sample
            df1 = df[df.PosNo > 0]
            dataF[df1.Gi.unique()] = 1
        elif dmetric == 1:
            # presence prob = number of positive days
            for i in xrange(len(dataF)):
                df1 = df[df.Gi == i]
                df1 = df1.groupby(['TauNo']).agg({'PosNo': max})
                dataF[i] = df1.PosNo.values.clip(max=1).sum()
            dmean = dataF[dataF > 0].mean()
            print dataF.max(), dataF.min(), dmean
            dataF = (dataF > dmean).astype(int)
        elif dmetric == 2:
            # presence prob = 1 positive sample
            for i in xrange(len(dataF)):
                df1 = df[df.Gi == i]
                df1 = df1.groupby(['TauNo']).agg({'PosNo': max})
                dataF[i] = df1.PosNo.values.clip(max=1).sum()
            dataF = (dataF > 0).astype(int) * hdens
            dmean = dataF[dataF > 0].mean()
            print dataF.max(), dataF.min(), dmean
            dataF = (dataF > dmean).astype(int)
        dataF_list += [dataF]
    #
    outfile = os.path.join(
        const.output_path, app_opts_list[0].dname,
        '{}{}.png'.format(app_opts.cprefix, fname))
    plotmaker.make_roc_plot(
        risk_indices, dataF_list, outfile, labels=labels, title=title)


def make_roc_range_plot(
        app_opts, inlib=inp, slib=PopSim, ci=2,
        fname='roc_upto_tFobs', chain_nos=[0, 1, 2], title=None,
        after_tobs=False, dmetric=0):
    print app_opts.outdir
    sims = []
    for c in chain_nos:
        cprefix = 'c{}_'.format(c)
        sims += [slib.load_sims(
            app_opts.outdir, 'by_space_{}'.format(ci), prefix=cprefix)]
    sims = np.concatenate(sims, axis=0)
    #
    dataF = np.zeros(sims.shape[1])
    df = inlib.load_data(app_opts, app_opts.dtypes[0])
    df = df[df.TauNo < app_opts.tF]
    if after_tobs:
        print "Before tobs", df.shape
        df = df[df.TauNo >= app_opts.tFobs]
        print "After tobs", df.shape
    if dmetric == 0:
        # presence prob = 1 positive sample
        df1 = df[df.PosNo > 0]
        dataF[df1.Gi.unique()] = 1
    elif dmetric == 1:
        # presence prob = number of positive days
        for i in xrange(len(dataF)):
            df1 = df[df.Gi == i]
            df1 = df1.groupby(['TauNo']).agg({'PosNo': max})
            dataF[i] = df1.PosNo.values.clip(max=1).sum()
        dmean = dataF[dataF > 0].mean()
        print dataF.max(), dataF.min(), dmean
        dataF = (dataF > dmean).astype(int)
    #
    plotmaker.make_roc_cloud(sims, dataF, os.path.join(
        const.output_path, app_opts.dname, '{}{}.png'.format(
            app_opts.cprefix, fname)))


# reference
city_dict = {
    90: 'La Grulla', 168: 'Lyford', 169: 'Sebastian', 170: 'Raymondville',
    222: 'Rio Grande City', 305: 'Edinburg', 306: 'Mission',
    307: 'Sullivan City', 330: 'Edcouch-Elsa', 403: 'Harlingen-San Benito',
    522: 'East Cameron', 523: 'Rio Hondo', 524: 'Brownsville',
    525: 'Los Fresnos-Laureles', 689: 'Hargill', 690: 'McAllen-Pharr',
    691: 'Southeast Hidalgo', 738: 'Lasara', 739: 'San Perlita'}


def make_initstate_plot(app_opts, fname=None, chain_nos=[0, 1, 2]):
    fname = 'city_init_state' if fname is None else fname
    # read in city raster
    city = np.loadtxt(os.path.join(
        const.input_path, app_opts.dname, 'Cities_raster.txt'),
        skiprows=6).T
    city = city[city > 0]
    # init state samples
    init_samps = []
    for c in chain_nos:
        init_samps += [sstruct.PopLearn.load_sims(
            app_opts.paramdir, 'at_0', prefix='c{}_'.format(c))]
    init_samps = np.concatenate(init_samps, axis=0)
    n_samp = init_samps.shape[0]
    #
    city_name, max_risk, comps = np.array([]), np.array([]), np.array([])
    for k in city_dict.keys():
        idx = (city == k)
        # exposed
        city_name = np.append(city_name, [city_dict[k]] * n_samp)
        max_risk = np.append(max_risk, init_samps[:, idx, 0].sum(1))
        comps = np.append(comps, ['Exposed'] * n_samp)
        # cryptic + symptomatic
        city_name = np.append(city_name, [city_dict[k]] * n_samp)
        max_risk = np.append(max_risk, init_samps[:, idx, 1:].sum(2).sum(1))
        comps = np.append(comps, ['Infectious + Detectable'] * n_samp)
    city_df = pd.DataFrame({
        'TX cities': city_name, '# infected sites': max_risk,
        'epidemic state': comps})
    fig, ax = plt.subplots()
    sns.boxplot(x='TX cities', y='# infected sites', hue='epidemic state',
                data=city_df, showfliers=False)
    plt.xticks(rotation=90)
    plt.tight_layout()
    fig.savefig(os.path.join(
        const.output_path, app_opts.dname, '{}.png'.format(fname)))


def make_predstate_plot(app_opts_list, chain_nos=[0, 1, 2]):
    # read in city raster
    city = np.loadtxt(os.path.join(
        const.input_path, app_opts_list[0].dname, 'Cities_raster.txt'),
        skiprows=6).T
    city = city[city > 0]
    # prediction state samples
    city_name, mean_risk, model = [], [], []
    for j, app_opts in enumerate(app_opts_list):
        pfield = 'by_space_{}'.format(app_opts.mlib.iI)
        pred_samps = []
        print app_opts.outdir
        for c in chain_nos:
            pred_samps += [sstruct.PopSim.load_sims(
                app_opts.outdir, pfield, prefix='c{}_'.format(c))]
        pred_samps = np.concatenate(pred_samps, axis=0)
        print pred_samps.shape
        pred_samps = pred_samps.mean(0)
        for k in city_dict.keys():
            city_name += [city_dict[k]]
            mean_risk += [pred_samps[city == k].mean()]
            model += [str(j)]
    city_df = pd.DataFrame({
        'city': city_name, 'mean_risk': mean_risk, 'model': model})
    fig, ax = plt.subplots()
    sns.barplot(x='city', y='mean_risk', hue='model', data=city_df)
    plt.xticks(rotation=90)
    plt.tight_layout()
    fig.savefig(os.path.join(
        const.output_path, app_opts.dname, 'city_predmean.png'))


def make_cmi_input_files(
        app_opts, smod_opts, inlib=inp, crop_lowdens=True, prim_frac=1):
    outdir = os.path.join(const.proj_path, 'cmi', 'Simulator', app_opts.dname)
    # make popmodel
    pmodel = inlib.load_popmodel(app_opts, smod_opts, False)
    pmodel.init.update(pmodel.init.prior)
    inds = (pmodel.pop.hdens > 0)
    if crop_lowdens:
        outdir += '_0.01'
        inds = (pmodel.pop.hdens > 0.01)
    # write host density
    header = citrus.read_header(app_opts.region, app_opts.resol)
    grid = pmodel.pop.grid
    ix, iy = grid.ixy
    ix, iy = ix[inds], iy[inds]
    raster = np.zeros(grid.shape) - 1
    raster[ix, iy] = pmodel.pop.hdens[inds]
    np.savetxt(os.path.join(outdir, 'L_0_HOSTDENSITY.txt'), raster.T,
               fmt='%.18g', header=header, comments='')
    # write initial state files
    state = pmodel.init.value[inds, :]
    print state.sum(0)
    susc = np.ones(state.shape[0])
    for k in xrange(pmodel.n_comp):
        raster = np.zeros(grid.shape) - 1
        raster[ix, iy] = state[:, k]
        susc -= state[:, k]
        assert np.all(susc >= 0)
        print pmodel.compartments[k].upper()
        np.savetxt(os.path.join(outdir, 'L_0_{}T_{}.txt'.format(
            app_opts.init_source, pmodel.compartments[k].upper())),
            raster.T, fmt='%.18g', header=header, comments='')
        idx = (state[:, k] > 0)
        print 'compartment', k, ix[idx], iy[idx]
    raster = np.zeros(grid.shape) - 1
    raster[ix, iy] = susc
    np.savetxt(os.path.join(outdir, 'L_0_{}T_SUSCEPTIBLE.txt'.format(
        app_opts.init_source)), raster.T, fmt='%.18g', header=header,
        comments='')
    # write susceptibility and infectiousness files
    raster = np.zeros(grid.shape) - 1
    raster[ix, iy] = pmodel.pop.covs[0][inds, 0] / pmodel.pop.hdens[inds]
    np.savetxt(os.path.join(outdir, 'L_0_SUSCEPTIBILITY.txt'.format(
        app_opts.init_source)), raster.T, fmt='%.18g', header=header,
        comments='')
    infs = pmodel.pop.covs[1][:, 0] / (pmodel.pop.hdens * (
        pmodel.pop.R + pmodel.params['mu'].value))
    Zi = infs[inds].max()
    print 'infs normalization', Zi
    raster[ix, iy] = infs[inds]
    np.savetxt(os.path.join(outdir, 'L_0_INFECTIVITY.txt'.format(
        app_opts.init_source)), raster.T, fmt='%.18g', header=header,
        comments='')
    # print parameter values
    for pname in pmodel.param_list:
        print pname, pmodel.params[pname].value
        if pname == 'beta':
            print pmodel.params[pname].value * Zi
    # heterogeneous primary rates
    if app_opts.region == 'TX':
        rates = pmodel.params['epsilon'].value / pmodel.pop.covs[0][:, 0] +\
            pmodel.params['varepsilon'].expand_by_space(0, pmodel.pop)
    else:
        rates = pmodel.params['epsilon'].value / pmodel.pop.covs[0][:, 0] +\
            pmodel.params['varepsilon'].value[0]
    rates = rates[inds] * prim_frac
    # rates *= pmodel.pop.hdens[inds].min()
    # rates = rates / pmodel.pop.covs[0][inds, 0]
    #
    raster = np.zeros(grid.shape) - 1
    raster[ix, iy] = rates
    np.savetxt(os.path.join(outdir, 'L_0_PRIMARY_{}.txt'.format(prim_frac)),
               raster.T, fmt='%.18g', header=header, comments='')
    #
    print "total primary rates",
    print rates.sum()
    ny = grid.shape[1]
    with open(os.path.join(
            outdir, 'DepositionKernelPoints.txt'), 'w') as f:
        f.write('KernelPoints {}\n'.format(len(rates)))
        for ri in xrange(len(rates)):
            i, j = ix[ri], iy[ri]
            f.write('{} {} {}\n'.format(
                i + .5, (ny - j - 1) + .5, rates[ri]))
    # P_0_ClimateData_0.txt
    gamma = pmodel.params['gamma'].value[0]
    with open(os.path.join(outdir, 'P_0_ClimateData_0.txt'), 'w') as f:
        f.write('States {}\n'.format(len(gamma)))
        for i in xrange(len(gamma)):
            f.write('{} {}\n'.format(pmodel.pop.tvec[i] / 365., gamma[i]))


def make_spreadmap(
        app_opts, chain_nos=[0, 1, 2], cis=[0, 1, 2], slib=PopSim, fname=None,
        inlib=inp, snap_dt=None, n_snaps=None, snap_points=[], cropped=True,
        figsize=None, fontsize=12, with_yearmap=False, with_tif=False,
        ts=None, sim_labels=None):
    fname = 'spreadmap' if fname is None else fname
    assert snap_dt is not None or n_snaps is not None
    grid, hdens = inlib.load_host(app_opts)
    if with_tif:
        with rasterio.open(os.path.join(
                const.input_path, app_opts.dname,
                'L_0_HOSTDENSITY.tif')) as src:
            kwargs = src.meta
    bmap = const.bmap(app_opts.region, cropped=cropped)
    #
    snap_times, data_snaps = make_snap_times(
        app_opts, snap_dt, n_snaps, snap_points, cropped=cropped, ts=ts,
        obs_only=True)
    print snap_times
    # print len(data_snaps[1][0]), len(data_snaps[1][0][0][0])
    if app_opts.region == 'CentralValley' or app_opts.region == 'CVreset':
        data_snaps = (data_snaps[0], [])

    sim_snaps = [[] for ci in xrange(len(cis))]
    labels = []
    print app_opts.outdir
    for i, ci in enumerate(cis):
        snap_fname = '{}_riskmap'.format(app_opts.mlib.compartments[ci])\
                     if (ci < 2 or app_opts.region == 'CVreset') else 'Detectable_riskmap'
        region_name = app_opts.region
        if 'CV' in app_opts.region:
            region_name = 'CentralValley'
        for k in xrange(len(snap_times)):
            sims = []
            for j in chain_nos:
                cprefix = 'c{}_'.format(j)
                sims += [slib.load_sims(
                    app_opts.outdir, 'by_space_{}_t{}'.format(
                        ci, snap_times[k]), prefix=cprefix)]
            sims = np.concatenate(sims, axis=0)
            sim_snaps[i] += [sims.mean(0)]
            print snap_times[k], (sim_snaps[i][-1] > 0).sum()
            mapmaker.make_heatmap(
                sim_snaps[i][-1], grid, app_opts.region, const,
                os.path.join(app_opts.outdir, '{}_{}.png'.format(
                    snap_fname, k)), vmax=1, bmap=bmap, hidx=hdens)
            if with_tif:
                raster = np.zeros(shape=grid.shape, dtype=kwargs['dtype']) - 1
                raster[grid.ixy[0], grid.ixy[1]] = sim_snaps[i][-1]
                kwargs['nodata'] = -1
                with rasterio.open(os.path.join(
                        app_opts.outdir, '{}_{}_{}.tif'.format(
                            region_name, snap_fname, data_snaps[0][k])),
                        'w', **kwargs) as dst:
                    dst.write(raster.T, 1)
        # final time
        sims = []
        for j in chain_nos:
            cprefix = 'c{}_'.format(j)
            sims += [slib.load_sims(
                app_opts.outdir, 'by_space_{}'.format(ci, snap_times[k]),
                prefix=cprefix)]
        sims = np.concatenate(sims, axis=0)
        sim_snaps[i] += [sims.mean(0)]
        k += 1
        mapmaker.make_heatmap(
            sim_snaps[i][-1], grid, app_opts.region, const,
            os.path.join(app_opts.outdir, '{}_riskmap_{}.png'.format(
                app_opts.mlib.compartments[ci], k)),
            vmax=1, bmap=bmap, hidx=hdens)
        if with_tif:
            raster = np.zeros(shape=grid.shape, dtype=kwargs['dtype']) - 1
            raster[grid.ixy[0], grid.ixy[1]] = sim_snaps[i][-1]
            kwargs['nodata'] = -1
            with rasterio.open(os.path.join(
                    app_opts.outdir, '{}_{}_{}.tif'.format(
                        region_name, snap_fname, data_snaps[0][k])),
                    'w', **kwargs) as dst:
                dst.write(raster.T, 1)
        ci_str = 'Infectious' if ci == 1 else app_opts.mlib.compartments[ci]
        labels += [ci_str]
    if app_opts.region == 'CentralValley':
        data_snaps = (data_snaps[0][-4:], data_snaps[1])
        for ci in cis:
            sim_snaps[ci] = sim_snaps[ci][4:6] + sim_snaps[ci][:2]
    labels[-1] = 'Detected'
    if 'FL' in app_opts.region:
        labels[-1] = 'Detectable'
    sim_labels = labels if sim_labels is None else sim_labels
    data_labels = ['{} samples'.format(d) for d in app_opts.dtypes]\
        if len(app_opts.dtypes) > 1 else ['Survey Data']
    mapmaker.make_sim_spreadmap(
        data_snaps[0], sim_snaps, data_snaps[1], grid, app_opts.region,
        const, os.path.join(app_opts.outdir, '{}.png'.format(fname)),
        hidx=hdens, vmax=1, bmap=bmap, figsize=figsize, msize=5, alpha=1,
        labels=sim_labels, fontsize=fontsize, data_labels=data_labels)
    if with_yearmap:
        mapmaker.make_sim_yearmaps(
            data_snaps[0], sim_snaps, data_snaps[1], grid, app_opts.region,
            const, os.path.join(app_opts.outdir, fname),
            hidx=hdens, vmax=1, bmap=bmap, figsize=figsize, msize=5, alpha=1,
            labels=sim_labels, fontsize=fontsize, data_labels=data_labels)


def make_correlogram_plot(
        app_opts, gf=3, with_latent=True, chain_nos=[0, 1, 2], inlib=inp,
        fname=None, colors=None, title=None, ci=2, ymax=0.5, data_r=1, sim_r=1,
        tree_wtd=False, binary=False, data_label=['Survey data'], fontsize=12):
    comp = app_opts.mlib.compartments[ci] if ci is not None else 'Detected'
    sw = pickle.load(open(os.path.join(
        const.input_path, app_opts.dname, 'sw_30.0km.pkl'), "rb"))
    grid, hdens = inlib.load_host(app_opts)
    df = inlib.load_data(app_opts, app_opts.dtypes[0])
    fname_outfile = 'combined_correlogram_Noulli_b0_{}_gf{}'.format(
        comp, gf) if fname is None else fname
    #
    sims_by_space, MI = None, None
    chain_str = ''.join([str(x) for x in chain_nos])
    fname = os.path.join(
        app_opts.outdir, 'MI_{}_gf{}_wt{}_b{}_c{}.h5'.format(
            comp, gf, int(tree_wtd), int(binary), chain_str))
    if os.path.isfile(fname):
        with h5py.File(fname) as f:
            MI = f['scores'][:]
    else:
        sims_by_space = []
        for c in chain_nos:
            cprefix = 'c{}_with_latent_'.format(c) if with_latent\
                      else'c{}_'.format(c)
            print cprefix
            if ci is not None:
                sims_by_space += [PopSim.load_sims(
                    app_opts.outdir, 'by_space_{}'.format(ci), prefix=cprefix)]
            else:
                sims_by_space += [PopSim.load_sims(
                    app_opts.outdir, 'by_space_dcount', prefix=cprefix)]
        sims_by_space = np.concatenate(sims_by_space, axis=0)
    # correlogram
    labels = ['Detected'] if ci == 2 else ['Infectious']
    MI = plotmaker.make_correlogram(df, grid, sw, os.path.join(
        const.output_path, app_opts.dname, '{}.png'.format(fname_outfile)),
        sims=sims_by_space, MI=MI, omodel='Noulli', binary=binary, gf=gf,
        colors=colors, ymin=-0.15, ymax=ymax, title=title, data_r=data_r,
        labels=labels + data_label, fontsize=fontsize, sim_r=sim_r,
        tree_wtd=tree_wtd, hdens=hdens)
    # save
    if not os.path.isfile(fname):
        print "Saving MI scores ..."
        with h5py.File(fname, "w") as f:
            f.create_dataset('scores', data=MI)


def make_dpc_plot(
        app_opts_list, with_latent=True, chain_nos=[0, 1, 2], inlib=inp,
        fontsize=12, gsize=None, fname=None, tFobs_trunc=False, title=None,
        with_tobs_mark=False, dpc_wtd=False, comp_ids=[0, 1, 2], yrF=None,
        labels=['Exposed', 'Infectious', 'Detected'],
        data_label=['Survey data'], no_data=False):
    fname = 'combined_sim_dpc' if fname is None else fname
    sims_list = []
    ii = None
    grid, hdens = inlib.load_host(app_opts_list[0])
    for app_opts in app_opts_list:
        T, sims_by_time = [], []
        for c in chain_nos:
            cprefix = 'c{}_with_latent_'.format(c) if with_latent\
                      else'c{}_'.format(c)
            print app_opts.outdir
            tag = 'by_time_wtd' if dpc_wtd else 'by_time'
            sims = PopSim.load_sims(
                app_opts.outdir, tag, prefix=cprefix)
            T = sims['T'].astype(int)
            if ii is None:
                ii = np.argmax(T > app_opts.tFobs) if tFobs_trunc\
                    else len(T) - 1
            T = T[:ii]
            S = sims['S']
            if app_opts.n_dcomp > 0:
                S[:, :, :- app_opts.n_dcomp] = np.cumsum(S[
                    :, :, -1 - app_opts.n_dcomp::-1], 2)[:, :, ::-1]
                S[:, :, - app_opts.n_dcomp:] = np.cumsum(S[
                    :, :, :- 1 - app_opts.n_dcomp:-1], 2)[:, :, ::-1]
            else:
                S = np.cumsum(S[:, :, ::-1], 2)[:, :, ::-1]
            if sims['D'] is not None:
                S = np.concatenate([
                    S[:, :, :2], sims['D'][:, :ii, None]], axis=2)
            sims_by_time += [S[:, :ii, comp_ids]]
        sims_list += [np.concatenate(sims_by_time, axis=0)]
    # dpc
    traj_type = 1 if dpc_wtd else 0
    df, traj = inlib.load_data(
        app_opts, app_opts.dtypes[0], with_traj=True, traj_type=traj_type)
    date0 = const.date0[app_opts.region]
    yr0 = (date0 + datetime.timedelta(T[0])).year + 1
    yrF = (date0 + datetime.timedelta(T[-1])).year if yrF is None else yrF
    ddif = (datetime.datetime(yr0, 1, 1) - date0).days
    T = (T - ddif) / 365.
    traj[0] = (traj[0] - ddif) / 365.
    n_yr = yrF - yr0 + 1
    xlocs = np.arange(0, n_yr, 2) if n_yr > 5 else np.arange(0, n_yr)
    lstr = [str(y + yr0) for y in xlocs]
    #
    fig = plt.figure()
    gsize = (len(sims_list), 1) if gsize is None else gsize
    gs = gridspec.GridSpec(gsize[0], gsize[1])
    ax = [plt.subplot(gs[i, j]) for i in xrange(gsize[0])
          for j in xrange(gsize[1])]
    for i, S in enumerate(sims_list):
        tmark = (app_opts_list[i].tFobs - ddif) / 365. if with_tobs_mark\
            else None
        n_cells = hdens.sum() if dpc_wtd else len(hdens)
        if no_data:
            data = []
        colors = plotmaker.make_fillplot(
            T, S, None, ax=ax[i], data=data, xticks=[xlocs, lstr],
            labels=labels, n_cells=n_cells, data_labels=data_label,
            tmark=tmark, ticksize=fontsize)
        if i == 0:
            ax[i].legend(loc='upper left', fontsize=fontsize)
        if i % gsize[0] == gsize[0] - 1:
            pass
            # ax[i].set_xlabel('Year', fontsize=fontsize)
        ax[i].set_ylabel('Prevalance', fontsize=fontsize)
        # ax[i].set_ylim(-100, 1500)
    if title is not None:
        plt.title(title, fontsize=fontsize + 2)
    else:
        plt.title('Epidemic progress vs. Survey data',
                  fontsize=fontsize + 2)
    plt.tight_layout()
    fig.savefig(os.path.join(
        app_opts.outdir, '{}.png'.format(fname)), dpi=100)
    return colors


def make_gof_plot(
        app_opts_time, app_opts_space, gf=3, with_latent=True,
        chain_nos=[0, 1, 2], inlib=inp, fontsize=12, with_title=False,
        with_tobs_mark=False):
    fig, ax = plt.subplots(1, 2, figsize=(11, 6))
    # dpc
    T, sims_by_time = [], []
    for c in chain_nos:
        cprefix = 'c{}_with_latent_'.format(c) if with_latent\
                  else'c{}_'.format(c)
        print app_opts_time.outdir
        sims = PopSim.load_sims(
            app_opts_time.outdir, 'by_time', prefix=cprefix)
        T = sims['T'].astype(int)
        S = sims['S']
        S = np.cumsum(S[:, :, ::-1], 2)[:, :, ::-1]
        if sims['D'] is not None:
            S = np.concatenate([
                S[:, :, :2], sims['D'][:, :, None]], axis=2)
        sims_by_time += [S]
    sims_by_time = np.concatenate(sims_by_time, axis=0)
    #
    df, traj = inlib.load_data(
        app_opts_time, app_opts_time.dtypes[0], with_traj=True)
    date0 = const.date0[app_opts_time.region]
    yr0 = (date0 + datetime.timedelta(T[0])).year + 1
    yrF = (date0 + datetime.timedelta(T[-1])).year
    ddif = (datetime.datetime(yr0, 1, 1) - date0).days
    T = (T - ddif) / 365.
    traj[0] = (traj[0] - ddif) / 365.
    n_yr = yrF - yr0 + 1
    xlocs = np.arange(0, n_yr, 2) if n_yr > 5 else np.arange(0, n_yr)
    lstr = [str(y + yr0) for y in xlocs]
    #
    tmark = app_opts_time.tFobs if with_tobs_mark else None
    colors = plotmaker.make_fillplot(
        T, S, None, ax=ax[0], data=[traj], xticks=[xlocs, lstr],
        labels=['Exposed', 'Infectious', 'Detectable'],
        data_labels=['Reported positives'], tmark=tmark)
    ax[0].legend(loc='upper left', fontsize=fontsize)
    ax[0].set_xlabel('Year', fontsize=fontsize)
    ax[0].set_ylabel('Infection area (km2)', fontsize=fontsize)
    if with_title:
        ax[0].set_title('A', fontsize=fontsize + 4)
    # plt.sca(ax[0])
    # plt.xticks(xlocs, lstr)
    # correlogram
    sw = pickle.load(open(os.path.join(
        const.input_path, app_opts_space.dname, 'sw_30.0km.pkl'), "rb"))
    grid, hdens = inlib.load_host(app_opts_space)
    df = inlib.load_data(app_opts_space, app_opts_space.dtypes[0])
    ci = 2  # app_opts.mlib.n_comp - app_opts.n_dcomp - 1
    #
    sims_by_space, MI = None, None
    fname = os.path.join(app_opts_space.outdir, 'MI_{}_gf{}.h5'.format(
        app_opts_space.mlib.compartments[ci], gf))
    print fname
    if os.path.isfile(fname):
        print "HERE 1"
        with h5py.File(fname) as f:
            MI = f['scores'][:]
    else:
        print "HERE 2"
        sims_by_space = []
        for c in chain_nos:
            cprefix = 'c{}_with_latent_'.format(c) if with_latent\
                      else'c{}_'.format(c)
            sims_by_space += [PopSim.load_sims(
                app_opts_space.outdir, 'by_space_{}'.format(ci),
                prefix=cprefix)]
        sims_by_space = np.concatenate(sims_by_space, axis=0)
        print sims_by_space.shape
        sims_by_space = sims_by_space[:10, :]
    # correlogram
    mi_colors = [colors[ci], colors[-1]]
    MI = plotmaker.make_correlogram(
        df, grid, sw, None, sims=sims_by_space, MI=MI,
        omodel='Noulli', binary=False, gf=gf, ax=ax[1], fontsize=fontsize,
        colors=mi_colors, labels=['Infectious', 'Reported positives'])
    if with_title:
        ax[1].set_title('B', fontsize=fontsize + 4)
    # save
    if not os.path.isfile(fname):
        print "Saving MI scores ..."
        with h5py.File(fname, "w") as f:
            f.create_dataset('scores', data=MI)
    #
    plt.tight_layout()
    fig.savefig(os.path.join(
        app_opts_space.outdir, 'combined_gof_plot-{}-gf{}.png'.format(
            app_opts_space.mlib.compartments[ci], gf)))
    return mi_colors


def make_sim_combined_movie(
        app_opts, gsize, chain_nos=[0, 1, 2], cis=[0, 1, 2], with_data=True,
        slib=PopSim, inlib=inp, cropped=False, msize=5, old_dt=30):
    grid, hdens = inlib.load_host(app_opts)
    bmap = const.bmap(app_opts.region, cropped=cropped)
    # Time line
    tau = app_opts.dtau[1]
    T = slib.load_sims(
        app_opts.outdir, 'by_time', prefix='c{}_'.format(chain_nos[0]))['T']
    T = np.append(T[::tau], T[-1])
    T = T.astype(int)
    # Simulations
    labels, sims = [], []
    for ci in cis:
        Sc = 0
        for k in chain_nos:
            S = slib.load_sims(app_opts.outdir, 'mean_full_{}'.format(ci),
                               prefix='c{}_'.format(k))
            Sc += np.vstack([S[::tau, :], S[-1, :]])
        sims += [Sc * 1. / len(chain_nos)]
        lb = app_opts.mlib.compartments[ci] if ci != 1 else "Infectious"
        labels += [lb]
    # Data
    if with_data:
        labels += ['Survey data']
        pos_list = []
        df = inlib.load_raw_positives(app_opts, app_opts.dtypes[0])
        pos = [[[], []]]
        for i in xrange(1, len(T)):
            points = df[(df.DayNo <= T[i]) & (df.DayNo > T[i - 1])]
            if points.empty:
                pos += [[[], []]]
                continue
            points = bmap(points.Lon.values, points.Lat.values)
            pos += [[points[0], points[1]]]
        pos_list += [pos]
    # Make movie
    mfile = os.path.join(app_opts.outdir, 'combined_movie_{}d.avi'.format(tau))
    moviemaker.make_sim_combined_movie(
        T, sims, pos_list, grid, app_opts.region, const, mfile, labels,
        gsize, bmap, vmax=1, hidx=hdens, with_label=False, msize=msize,
        old_dt=old_dt)
