import data.const as const
import data.input.california as inp
import data.output.texas as oh
import idm.plotting.moviemaker as moviemaker
import idm.model.SECI as SECI
from idm.simulation.struct import PopSim, PopLearn
from idm.simulation.util import get_discrete_expand_S, discrete_T

import os
import matplotlib.pyplot as plt
import numpy as np
import h5py

figsize = (9, 3.5)


def make_data_maps(app_opts, snap_dt=1, cropped=True):
    oh.make_data_maps(app_opts, figsize=figsize, inlib=inp, snap_dt=snap_dt,
                      cropped=cropped)


def make_sim_maps(
        app_opts, prim_intros=[], yr0=2014, n_yr=6, post=False, snap_dt=None,
        corrgram=False, n_snaps=None, snap_points=[], cropped=True,
        with_spreadmap=True, dpc_wtd=False, n_step_yr=1):
    if post:
        oh.make_sim_maps(
            app_opts, prim_intros=prim_intros, yr0=yr0, n_yr=n_yr,
            slib=PopLearn, snap_dt=snap_dt, corrgram=corrgram, n_snaps=n_snaps,
            snap_points=snap_points, cropped=cropped, n_step_yr=n_step_yr,
            with_spreadmap=with_spreadmap, dpc_wtd=dpc_wtd)
    else:
        oh.make_sim_maps(
            app_opts, prim_intros=prim_intros, yr0=yr0, n_yr=n_yr, slib=PopSim,
            snap_dt=snap_dt, corrgram=corrgram, n_snaps=n_snaps,
            snap_points=snap_points, cropped=cropped, n_step_yr=n_step_yr,
            with_spreadmap=with_spreadmap, dpc_wtd=dpc_wtd)


def write_config_file(app_opts, opts, ctype):
    oh.write_config_file(app_opts, opts, ctype)


def make_param_plots(app_opts, param_dist, freq_depend, dmax=10,
                     trace=False, init_map=False, rescale=True):
    oh.make_param_plots(
        app_opts, param_dist, freq_depend, dmax=dmax, trace=trace,
        init_map=init_map, rescale=rescale)


def make_sim_movies(app_opts, hobs=True, post=False, clist=[0, 1]):
    if post:
        oh.make_sim_movies(app_opts, hobs=hobs, slib=PopLearn, clist=clist)
    else:
        oh.make_sim_movies(app_opts, hobs=hobs, slib=PopSim, clist=clist)


def make_snap_times(
        app_opts, snap_dt=None, n_snaps=None, snap_points=[],
        inlib=inp, yr_range=False, cropped=False):
    return oh.make_snap_times(
        app_opts, snap_dt, n_snaps, snap_points, inlib=inlib,
        yr_range=yr_range, cropped=cropped)


def make_compare_movie(region, resol, adirs, iout, compartments, tau,
                       adir_out, gsize, labels):
    dname = '{}_{}'.format(region, resol)
    grid, hdens = input.load_host(dname, epsg=const.epsg[region])
    bmap = const.bmap[region]
    outdir = os.path.join(const.output_path, dname, adirs[0])
    T = PopSim.load_sims(outdir, 'by_time')['T']
    T = np.append(T[::tau], T[-1])
    #
    oi = len(iout) - 1
    Slist = []
    for adir in adirs:
        outdir = os.path.join(const.output_path, dname, adir)
        S = PopSim.load_sims(outdir, 'mean_full_{}'.format(oi))
        S = np.vstack([S[::tau, :], S[-1, :]])
        Slist += [S]
    print len(Slist), Slist[0].shape
    #
    fname = '{}_mean_compare2_{}.avi'.format(compartments[iout[oi]], tau)
    outfile = os.path.join(const.output_path, dname, adir_out, fname)
    moviemaker.make_multiple_heatmovie(
        T, Slist, grid, region, const, outfile, gsize, bmap, labels,
        vmax=1, hidx=hdens)
    plt.close("all")
    print fname, "DONE."


def make_overview_movie(app_opts, iout, compartments, tau):
    yr0 = 2014
    locs = np.arange(4)
    labels = [str(l + yr0) for l in locs]
    xticks = [locs, labels]
    sims = PopSim.load_sims(app_opts.outdir, 'by_time')
    T, S_time = sims['T'], sims['S'][:, :, np.min(iout):]
    S_time = np.cumsum(S_time[:, :, ::-1], 2)[:, :, ::-1]
    outfile = os.path.join(app_opts.outdir, 'overview11_{}.avi').format(tau)
    #
    grid, hdens = input.load_host(
        app_opts.dname, epsg=const.epsg[app_opts.region])
    bmap = const.bmap[app_opts.region]
    S_space, labels = [], []
    for oi in xrange(len(iout) - 1, -1, -1):
        S = PopSim.load_sims(app_opts.outdir, 'mean_full_{}'.format(oi))
        S_space += [S]
        labels += [compartments[iout[oi]]]
    moviemaker.make_overview_movie(
        T, S_time[:, :, ::-1], S_space, tau, yr0, grid, app_opts.region,
        const, bmap, outfile, labels, xticks=xticks, hidx=hdens)


def make_roc_plots(app_opts, tFobs=None, inlib=inp, slib=PopSim, dmetric=1):
    oh.make_roc_plots(app_opts, tFobs=tFobs, inlib=inlib, slib=slib,
                      dmetric=dmetric)


def make_correlogram_plot(
        app_opts, gf=3, with_latent=False, chain_nos=[0, 1, 2], inlib=inp,
        fname=None, colors=None, title=None, ci=2):
    oh.make_correlogram_plot(
        app_opts, gf=gf, with_latent=with_latent, chain_nos=chain_nos,
        inlib=inlib, fname=fname, colors=colors, title=title, ci=ci)


def make_postsim_file(postmodel, app_opts, sim_opts, hlb_dir, acp_dir,
                      model_acp=SECI, model_hlb=SECI):
    # check if postsim file already exist
    if not os.path.exists(hlb_dir):
        os.makedirs(hlb_dir)
    pfile = '{}postsim.h5'.format(app_opts.cprefix)
    if os.path.isfile(os.path.join(hlb_dir, pfile)):
        return
    tvec = postmodel.pop.tvec
    snap_times = sim_opts.snap_times[sim_opts.snap_times < tvec[-1]]
    # load HLB data and turn into trajectories
    assert app_opts.dthres[0] == 38
    df = inp.load_data(app_opts, 'plant')
    print "HLB positives at", app_opts.dthres[0], df.shape
    s0 = np.zeros((postmodel.pop.n_pop, model_hlb.n_comp))
    ss = np.zeros(postmodel.pop.n_pop)
    T, S = [tvec[0]], []
    df = df[df.PosNo > 0]
    df = df[df.TauNo < app_opts.tFobs]
    # df0 = df[df.TauNo < app_opts.t0]
    for i, row in df.iterrows():
        if row['TauNo'] < tvec[0]:
            s0[int(row['Gi']), 0] += 1
        else:
            gi = int(row['Gi'])
            if ss[gi] == 0:
                T += [row['TauNo']]
                S += [[gi, 0]]
                ss[gi] += 1
    s0 = s0.clip(max=1)
    # T += [tvec[-1]]
    print s0.sum(0)
    print len(T)
    print zip(T[1:], S)
    T, S = np.array(T), np.array(S)
    # combine HLB with ACP
    tt = discrete_T(tvec, sim_opts.tau)
    S_hlb = get_discrete_expand_S(T, S, s0, model_hlb.rupdate, tt)
    with h5py.File(os.path.join(acp_dir, pfile)) as f:
        Sposts = f['Sposts'][:]
        '''S = f['sp_by_time'][:]
        print S[0, -1, :]
        print Sposts[301, :, :].sum(0)
        print acp_dir
        assert False'''
        n_samp = Sposts.shape[0]
        Sposts = np.concatenate(
            [np.tile(S_hlb[-1, :, :], (n_samp, 1, 1)), Sposts], axis=2)
        sp_by_time = np.concatenate(
            [np.tile(S_hlb.sum(1), (n_samp, 1, 1)), f['sp_by_time'][:]],
            axis=2)
        sp_by_space, sp_mean = {}, {}
        for ci in xrange(model_hlb.n_comp):
            sp_by_space[ci] = []
            for t in snap_times:
                it = int((t - tvec[0]) / sim_opts.tau) + 1
                sp_by_space[ci] += [np.tile(
                    S_hlb[it, :, ci:].sum(1), (n_samp, 1))]
            sp_mean[ci] = S_hlb[:, :, ci:].sum(2)
        for ci in xrange(model_acp.n_comp):
            sp_by_space[ci + model_hlb.n_comp] = []
            for t in snap_times:
                sp_by_space[ci + model_hlb.n_comp] += [
                    f['sp_by_space_{}_t{}'.format(ci, t)][:]]
            sp_mean[ci + model_hlb.n_comp] = f['sp_mean_{}'.format(ci)][:]
    # save to hlb_dir
    print "Save to hlb_dir ..."
    with h5py.File(os.path.join(hlb_dir, pfile), "w") as f:
        f.create_dataset('Sposts', data=Sposts)
        f.create_dataset('sp_by_time', data=sp_by_time)
        for ci in xrange(Sposts.shape[-1]):
            print "compartment", ci
            f.create_dataset('sp_mean_{}'.format(ci), data=sp_mean[ci])
            for i, t in enumerate(snap_times):
                print "time", i, t
                f.create_dataset('sp_by_space_{}_t{}'.format(ci, t),
                                 data=sp_by_space[ci][i])
