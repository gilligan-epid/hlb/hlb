import data.const as const
import data.input.florida as inp
import data.output.california as ca
import data.output.texas as tx
import idm.plotting.mapmaker as mapmaker
import idm.plotting.plotmaker as plotmaker
from idm.simulation.struct import PopSim

import os
import matplotlib.pyplot as plt
import datetime
import numpy as np

figsize = (15, 7.5)


def make_data_maps(app_opts, snap_dt=1, cropped=False):
    """Make data visualization maps."""
    tx.make_data_maps(app_opts, figsize=figsize, inlib=inp, snap_dt=snap_dt,
                      cropped=cropped)


def make_sim_maps(
        app_opts, prim_intros=[], yr0=2006, n_yr=8, post=False, snap_dt=None,
        corrgram=False, n_snaps=None, snap_points=[], cropped=True,
        with_spreadmap=True, dpc_wtd=False, n_step_yr=1):
    if post:
        assert False, "Not implemented"
    else:
        tx.make_sim_maps(
            app_opts, prim_intros=prim_intros, yr0=yr0, n_yr=n_yr, slib=PopSim,
            snap_dt=snap_dt, corrgram=corrgram, n_snaps=n_snaps,
            snap_points=snap_points, cropped=cropped, n_step_yr=n_step_yr,
            with_spreadmap=with_spreadmap, dpc_wtd=dpc_wtd)


def make_snap_times(
        app_opts, snap_dt=None, n_snaps=None, snap_points=[],
        inlib=inp, yr_range=False, cropped=False):
    return ca.make_snap_times(
        app_opts, snap_dt, n_snaps, snap_points, inlib=inlib,
        yr_range=yr_range, cropped=cropped)


def make_roc_plots(app_opts, tFobs=None, inlib=inp, slib=PopSim, dmetric=1):
    ca.make_roc_plots(app_opts, tFobs=tFobs, inlib=inlib, slib=slib,
                      dmetric=dmetric)


def write_config_file(app_opts, opts, ctype):
    ca.write_config_file(app_opts, opts, ctype)


def make_sim_movies(app_opts, iout, compartments, hobs=True, post=False):
    ca.make_sim_movies(app_opts, iout, compartments, hobs, post)
