"""
This file sets out the project constants.

It includes paths to data and results, modelling dates, and spatial
setups.
"""

from machine import machine_type

import os
import datetime
import pyproj
from sys import platform

from idm.plotting.mapmaker import make_basemap

if machine_type == 'work':
    if platform == 'win32':
        proj_path = os.path.join('E:/', 'EXPERIMENTS', 'HLB')
    elif platform == 'linux' or platform == 'linux2':
        proj_path = os.path.join(
            "/rds", "project", "cag1", "rds-cag1-general", "epidem-userspaces",
            "van25", "EXPERIMENTS", "HLB")
    else:
        assert False, platform
elif machine_type == 'personal':
    if platform == 'win32':
        proj_path = os.path.join('D:/', 'EXPERIMENTS', 'HLB')
    elif platform == 'linux' or platform == 'linux2':
        proj_path = "/media/scratch/lb584_scratch/projects/hlb_viet/output/"#edit this for the installation location
    else:
        assert False, platform


raw_path = os.path.join(proj_path, 'RawData')
input_path = os.path.join(proj_path, 'Input')
output_path = os.path.join(proj_path, 'Output')

#
shpfile = {
    'Hidalgo': os.path.join(raw_path, 'Basemap', 'TX_counties'),
    'TX': os.path.join(raw_path, 'Basemap', 'TX_counties'),
    'TXfull': os.path.join(raw_path, 'Basemap', 'TX_counties'),
    'CAsouth': os.path.join(raw_path, 'Basemap', 'California_County'),
    'CentralValley': os.path.join(raw_path, 'Basemap', 'California_County'),
    'CVreset': os.path.join(raw_path, 'Basemap', 'California_County'),
    'CA': os.path.join(proj_path, 'GIS', 'California_counties',
                       'CA_citrus_counties'),
    'FLsouth': os.path.join(raw_path, 'Basemap', 'Florida_orig'),
    'FL': os.path.join(raw_path, 'Basemap', 'Florida_orig')
}
#
date0_acp = {
    'CAsouth': datetime.datetime(2009, 1, 1),
    'CentralValley': datetime.datetime(2012, 1, 1),
    'CVreset': datetime.datetime(2012, 1, 1),  # must be same as CentralValley
}
dateFobs_acp = {
    'CAsouth': datetime.datetime(2013, 12, 30),  # (2011, 2, 28),
    'CentralValley': datetime.datetime(2016, 10, 30),
    'CVreset': datetime.datetime(2020, 03, 31),
}
dateF_acp = {
    'CAsouth': datetime.datetime(2014, 12, 30),
    'CentralValley': datetime.datetime(2017, 12, 30),
    'CVreset': datetime.datetime(2019, 12, 30),
}
#
date0 = {
    'Hidalgo': datetime.datetime(2011, 12, 8),
    'TX': datetime.datetime(2011, 12, 8),
    # 'CAsouth': datetime.datetime(2015, 6, 15),
    'CAsouth': datetime.datetime(2017, 12, 29),
    'CentralValley': date0_acp['CentralValley'],
    'CVreset': date0_acp['CVreset'],
    'CA': datetime.datetime(2015, 6, 15),
    'FLsouth': datetime.datetime(2005, 8, 23),
    'FL': datetime.datetime(2005, 8, 23),
}
dateFobs = {
    'Hidalgo': datetime.datetime(2017, 8, 1),
    'TX': datetime.datetime(2018, 10, 10),
    # 'CAsouth': datetime.datetime(2020, 3, 31),
    'CAsouth': datetime.datetime(2017, 12, 30),
    'CentralValley': datetime.datetime(2018, 3, 31),
    'CVreset': datetime.datetime(2020, 3, 31),
    'CA': datetime.datetime(2017, 9, 1),
    'FLsouth': datetime.datetime(2008, 12, 30),
    'FL': datetime.datetime(2005, 8, 25)
}
dateF = {
    'Hidalgo': datetime.datetime(2017, 8, 1),
    'TX': datetime.datetime(2020, 12, 30),
    # 'CAsouth': datetime.datetime(2026, 12, 30),
    'CAsouth': datetime.datetime(2018, 1, 2),
    #'CAsouth': datetime.datetime(2019, 6, 30),
    'CentralValley': datetime.datetime(2021, 12, 30),
    'CVreset': datetime.datetime(2031, 1, 10),
    'CA': datetime.datetime(2017, 9, 1),
    'FLsouth': datetime.datetime(2009, 12, 30),
    'FL': datetime.datetime(2010, 12, 1)
}

#
epsg = {
    'Hidalgo': 3083,
    'TX': 3083,
    'TXfull': 3083,
    'CAsouth': 3310,
    'CentralValley': 3310,
    'CVreset': 3310,
    'CA': 3310,
    'FLsouth': 3086,
    'FL': 3086,  # 26917
}
bounds = {
    'Hidalgo': (-98.60, 26.00, -97.85, 26.50),
    'TX': (-99.00, 25.80, -97.10, 26.70),
    'TXcropped': (-98.50, 25.85, -97.10, 26.50),
    'TXfull': (-106.60, 12.80, -85.50, 36.50),
    'CAsouth': (-120.42, 32.51, -114.13, 34.50),
    'CAsouthcropped': (-119.00, 33.60, -117.00, 34.50),
    'CentralValley': (-124.42, 34.50, -115.10, 39.50),
    'CentralValleycropped': (-122.30, 34.90, -118.50, 37.10),
    'CVreset': (-124.42, 34.50, -115.10, 39.50),
    'CVresetcropped': (-122.30, 34.90, -118.50, 37.10),
    'CA': (-124.42, 32.51, -113.13, 39.50),
    'FLsouth': (-80.70, 25.00, -79.90, 27.00),
    'FL': (-83.00, 25.00, -79.90, 30.00),
}
orig = pyproj.Proj(init='epsg:4629')
# bmap_zoomed = make_basemap(bounds['zoomed'], epsg=epsg['CA'])


def proj_bounds(region):
    """Return the projected boundary points of the given region."""
    bnds = bounds[region]
    dest = pyproj.Proj(init='epsg:{}'.format(epsg[region]))
    x0, y0 = pyproj.transform(orig, dest, bnds[0], bnds[1])
    xF, yF = pyproj.transform(orig, dest, bnds[2], bnds[3])
    return x0, y0, xF, yF


def tF(region):
    """Return the end date for HLB modelling."""
    return (dateF[region] - date0[region]).days


def tFobs(region):
    """Return the observation date."""
    return (dateFobs[region] - date0[region]).days


def tF_acp(region):
    """Return the end date for ACP modelling."""
    return (dateF_acp[region] - date0_acp[region]).days


def tFobs_acp(region):
    """Return the observation date the ACP modelling."""
    return (dateFobs_acp[region] - date0_acp[region]).days


def bmap(region, ax=None, cropped=False):
    """Construct a basemap for the region."""
    bnds = bounds[region + 'cropped'] if cropped else bounds[region]
    return make_basemap(bnds, epsg=epsg[region], ax=ax)
