import data.input.florida as inp
import data.const as const
import california as ca

import pandas as pd
import os
import numpy as np
import pyproj
import datetime
import rasterio


def combine_host_rasters(app_opts, norm=True, ignore_threshold=0.01):
    return ca.combine_host_rasters(app_opts, norm, ignore_threshold)


def adjust_host_by_mask(app_opts):
    ca.adjust_host_by_mask(app_opts)


def make_covariate_matrices(app_opts, norm=True, ignore_threshold=0.01):
    return ca.make_covariate_matrices(app_opts, norm, ignore_threshold)


def make_hlb_df(app_opts):
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    det_count = np.zeros(shape=(ny, nx), dtype='float32')
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    df = pd.read_excel(os.path.join(
        const.raw_path, 'Infection', 'Florida_HLB_20131017.xlsx'))
    out_x, out_y = [], []
    lons, lats, X, Y, Xi, Yi, Gi = [], [], [], [], [], [], []
    dayno, monthno, weekno = [], [], []
    month, year, dens, glons, glats = [], [], [], [], []
    for i, row in df.iterrows():
        lon, lat = row['Centroid_X'], row['Centroid_Y']
        x, y = pyproj.transform(const.orig, proj, lon, lat)
        gi, xyi = grid.get_index(x, y)
        if gi is None:
            out_x += [x]
            out_y += [y]
            continue
        xi, yi = xyi
        gx, gy = grid.get_xy(xi, yi)
        glon, glat = pyproj.transform(proj, const.orig, gx, gy)
        det_count[yi, xi] += 1
        lons += [lon]
        lats += [lat]
        glons += [glon]
        glats += [glat]
        X += [x]
        Y += [y]
        Xi += [xi]
        Yi += [yi]
        Gi += [gi]
        dens += [hdens[gi]]
        year += [row['Year_find']]
        d = datetime.datetime.strptime(str(row['First_found'])[:10],
                                       '%Y-%m-%d')
        assert d.year == year[-1]
        dday = (d - const.date0[app_opts.region]).days
        dayno += [dday]
        weekno += [dday / 7]
        monthno += [dday / 30]
        month += [d.month]
    df = pd.DataFrame({
        'Lon': lons, 'Lat': lats, 'X': X, 'Y': Y,
        'Xi': Xi, 'Yi': Yi, 'Gi': Gi, 'GLon': glons, 'GLat': glats,
        'DayNo': dayno, 'WeekNo': weekno, 'MonthNo': monthno,
        'Month': month, 'Year': year, 'Density': dens
    })
    df_out = pd.DataFrame({'X': out_x, 'Y': out_y})
    df_out = df_out.drop_duplicates()
    print "({}/{}) out of landscape".format(
        len(df_out), len(df.groupby(['X', 'Y'])))
    df.to_csv(os.path.join(
        const.raw_path, 'Infection', '{}_hlb_samples.csv'.format(
            app_opts.dname)), index=False)
    # snapshot of total detections
    tif_base = os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}km.tif'.format(
            app_opts.region, app_opts.csize * app_opts.cscale))
    with rasterio.open(tif_base) as src:
        kwargs = src.meta
    tif_outfile = os.path.join(
        const.raw_path, 'Infection',
        '{}_hlb_samples.tif'.format(app_opts.dname))
    with rasterio.open(tif_outfile, 'w', **kwargs) as dst:
        dst.write(det_count, 1)
    return df, grid


def compile_data(app_opts, dtype, nosample=False):
    ca.compile_data(app_opts, dtype, nosample)


# OLD


def make_commercial_raster(resol):
    # read extent
    indir = os.path.join(const.raw_path, 'Host', 'FloridaCitrus')
    xbins, ybins = read_header(os.path.join(
        indir, 'Florida_Extent_{}.txt'.format(resol)), '', n_header)
    grid = Grid([xbins, ybins], np.zeros((0, 2)))
    x0, y0, cs = grid.edges[0][0], grid.edges[1][-1], grid.csize
    lon0, lat0 = transform(proj, orig, x0, y0)
    xF, yF = grid.edges[0][-1], grid.edges[1][0]
    lonF, latF = transform(proj, orig, xF, yF)
    nx, ny = grid.shape

    def get_index(x, y):
        return
    # load dataframe
    df = pd.read_excel(os.path.join(
        indir, 'FL_commercial_citrus_block_20140518.xlsx'))
    print "no. of points", df.shape
    # construct host
    host = np.zeros((nx, ny))
    for ri, row in df.iterrows():
        lon, lat = row['Lon'], row['Lat']
        if not ((lon < lonF) & (lat < latF) & (lon > lon0) & (lat > lat0)):
            continue
        x, y = transform(orig, proj, lon, lat)
        i, j = int((x - x0)/cs), ny - 1 - int((y - y0)/cs)
        host[i, j] += row['TREES_SUM']
    print "Host max:", host.max()
    print (host == 0).sum(), (host < 0).sum()
    print "With host:", (host > 0).sum()
    print "Less than 1:", (host < 1).sum()
    print "Larger than 1:", (host > 1).sum()
    # read header
    fname = os.path.join(indir, 'Florida_Extent_{}.txt'.format(resol))
    with open(fname) as f:
        header = f.read().rstrip()
    # write to file
    fname = os.path.join(
        indir, 'Florida_Citrus_block_raster_{}.txt'.format(resol))
    np.savetxt(fname, host.T, fmt='%.18g', header=header, comments='')


def make_residential_raster(resol):
    # read extent
    indir = os.path.join(const.raw_path, 'Host', 'FloridaCitrus')
    xbins, ybins = read_header(os.path.join(
        indir, 'Florida_Extent_{}.txt'.format(resol)), '', n_header)
    grid = Grid([xbins, ybins], np.zeros((0, 2)))
    x0, y0, cs = grid.edges[0][0], grid.edges[1][-1], grid.csize
    lon0, lat0 = transform(proj, orig, x0, y0)
    xF, yF = grid.edges[0][-1], grid.edges[1][0]
    lonF, latF = transform(proj, orig, xF, yF)
    nx, ny = grid.shape

    def get_index(x, y):
        return int((x - x0)/cs), ny - 1 - int((y - y0)/cs)
    # load dataframe
    df = pd.read_csv(os.path.join(
        indir, 'FL_residential_tree_data_at_STR_scale.csv'))
    print "no. of points", df.shape
    df = df[df.C_tree > 0]
    print "no of positive points", df.shape
    # construct host
    host = np.zeros((nx, ny))
    for ri, row in df.iterrows():
        lon, lat = row['Longitude'], row['Latitude']
        if not ((lon < lonF) & (lat < latF) & (lon > lon0) & (lat > lat0)):
            continue
        x, y = transform(orig, proj, lon, lat)
        #
        area = row['Area_KM'] * 10e6
        hd = np.sqrt(area) / 2.
        x1, y1 = max(x - hd, x0), max(y - hd, y0)
        x2, y2 = min(x + hd, xF), min(y + hd, yF)
        i1, j1 = get_index(x1, y1)
        i2, j2 = get_index(x2, y2)
        assert (i1 <= i2) and (j1 >= j2)
        xp, yp = x1, y1
        for i in xrange(i1 + 1, i2 + 1):
            x = grid.edges[0][i]
            yp = y1
            for j in xrange(j1, j2, -1):
                y = grid.edges[1][j]
                assert (x > xp) and (y > yp), (x, xp, y, yp)
                cell_area = (x - xp) * (y - yp)
                assert cell_area <= cs * cs
                host[i - 1, j] += row['C_tree'] * min(1, cell_area / area)
                yp = y
            assert (x > xp) and(y2 > yp)
            cell_area = (x - xp) * (y2 - yp)
            host[i - 1, j2] += row['C_tree'] * min(1, cell_area / area)
            xp = x
        yp = y1
        for j in xrange(j1, j2, -1):
            y = grid.edges[1][j]
            cell_area = (x2 - xp) * (y - yp)
            assert cell_area <= cs * cs
            host[i2, j] += row['C_tree'] * min(1, cell_area / area)
            yp = y
    print "Host max:", host.max()
    print (host == 0).sum(), (host < 0).sum()
    print "With host:", (host > 0).sum()
    print "Less than 1:", (host < 1).sum()
    print "Larger than 1:", (host > 1).sum()
    # read header
    fname = os.path.join(indir, 'Florida_Extent_{}.txt'.format(resol))
    with open(fname) as f:
        header = f.read().rstrip()
    # write to file
    fname = os.path.join(
        indir, 'Florida_Residential_Citrus_raster_{}.txt'.format(resol))
    np.savetxt(fname, host.T, fmt='%.18g', header=header, comments='')


def make_host_raster(resol):
    """
    Prepare L_0_HOSTDENSITY.txt by combining commercial & residential rasters
    """
    indir = os.path.join(const.raw_path, 'Host', 'FloridaCitrus')
    wts = {'Florida_Citrus_block_raster_{}.txt'.format(resol): 1.0,
           'Florida_Residential_Citrus_raster_{}.txt'.format(resol): 0.013}
    host, w = 0, 0
    for f in wts:
        h = np.loadtxt(os.path.join(indir, f), skiprows=n_header)
        assert h.max() > 10, h.max()
        host += h * wts[f]
        w += wts[f]
    # read header
    fname = os.path.join(indir, 'Florida_Extent_{}.txt'.format(resol))
    with open(fname) as f:
        header = f.read().rstrip()
    # write combined host file
    fname = os.path.join(
        indir, 'Florida_combined_citrus_raster_{}.txt'.format(resol))
    np.savetxt(fname, host, fmt='%.18g', header=header, comments='')


def compile_host_raster(resol, threshold=0.01):
    indir = os.path.join(const.raw_path, 'Host', 'FloridaCitrus')
    host = np.loadtxt(os.path.join(
        indir,
        'Florida_Commercial_and_Residential_Citrus_raster_{}.txt'.format(resol)
    ), skiprows=n_header)
    host[host < threshold] = 0.
    # read header
    fname = os.path.join(indir, 'Florida_Extent_{}.txt'.format(resol))
    with open(fname) as f:
        header = f.read().rstrip()
    # write combined host file
    fname = os.path.join(const.input_path, '{}_{}'.format(region, resol),
                         'L_0_HOSTDENSITY.txt')
    np.savetxt(fname, host, fmt='%.18g', header=header, comments='')


def compile_infection_df(resol):
    dname = '{}_{}'.format(region, resol)
    grid, hdens = input.load_host(dname)
    x0, y0, cs = grid.edges[0][0], grid.edges[1][-1], grid.csize
    xF, yF = grid.edges[0][-1], grid.edges[1][0]
    nx, ny = grid.shape
    xbins, ybins = grid.bins
    #
    fname = os.path.join(const.raw_path, 'Infection',
                         'Florida_HLB_20131017.xlsx')
    df = pd.read_excel(fname)
    lons, lats, X, Y, Xi, Yi, Gi = [], [], [], [], [], [], []
    dayno, month, year = [], [], []
    n_out = 0
    for i, row in df.iterrows():
        lon, lat = row['Centroid_X'], row['Centroid_Y']
        x, y = transform(orig, proj, lon, lat)
        if not (x <= xF and y <= yF):
            continue
        xi, yi = int((x - x0)/cs), ny - 1 - int((y - y0)/cs)
        gi = grid.get_index([xi, yi])
        if gi is None:
            n_out += 1
            continue
        lons += [lon]
        lats += [lat]
        X += [x]
        Y += [y]
        Xi += [xi]
        Yi += [yi]
        Gi += [gi]
        year += [row['Year_find']]
        d = datetime.datetime.strptime(str(row['First_found'])[:10],
                                       '%Y-%m-%d')
        assert d.year == year[-1]
        dayno += [(d - const.date0[region]).days]
        month += [d.month]
    df = pd.DataFrame({
        'Longitude': lons, 'Latitude': lats, 'X': X, 'Y': Y,
        'Xi': Xi, 'Yi': Yi, 'Gi': Gi,
        'DayNo': dayno, 'Month': month, 'Year': year, 'Positive': True
    })
    df.to_csv(os.path.join(const.input_path, dname, 'hlb_positives.csv'),
              index=False)
    print "No of out-of-landscape points", n_out
    return df


def mark_orchards(app_opts, threshold=1e-3):
    ca.mark_orchards(app_opts, threshold=threshold)