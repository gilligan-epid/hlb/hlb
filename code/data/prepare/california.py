import data.input.california as inp
import data.const as const
import texas as texas
import citrus

import os
import fiona
import datetime
import pandas as pd
import rasterio
import numpy as np
import pyproj
import copy

plant_file = 'CA_plant_samples_thru_20200331_proj_3310.shp'
psyllid_file = 'CA_psyllid_samples_thru_20200331_proj_3310.shp'
trap_files = [
    'CA_trapping_samples_2008_2011_proj_3310.shp',
    'CA_trapping_samples_2012_2017_proj_3310.shp']


def combine_host_rasters(app_opts, norm=True, ignore_threshold=0.01):
    return texas.combine_host_rasters(app_opts, norm, ignore_threshold)


def adjust_host_by_mask(app_opts):
    texas.adjust_host_by_mask(app_opts)


def adjust_host_by_data(app_opts):
    texas.adjust_host_by_data(app_opts)


def make_covariate_matrices(app_opts, norm=True, ignore_threshold=0.01):
    """
    Prepare L_0_SUSCEPTIBILITY.txt and L_0_INFECTIOUSNESS.txt
    """
    header = citrus.read_header(app_opts.region, app_opts.resol)
    area = (app_opts.csize * app_opts.cscale) ** 2
    # commercial orchards
    ho = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_commercial_raster_{}.txt'.format(app_opts.region, app_opts.resol)),
        skiprows=texas.n_header) * texas.ntree * area
    # residential trees
    hr = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.txt'.format(
            app_opts.region, app_opts.resol)),
        skiprows=texas.n_header)
    if norm:
        ho = ho / (texas.ntree * area)
        hr = hr / (texas.ntree * area)
    host = ho + hr
    idx = (host > ignore_threshold)
    cwts_res = copy.copy(hr)
    cwts_orc = copy.copy(ho)
    cwts_res[idx] = cwts_res[idx] / host[idx]
    cwts_orc[idx] = cwts_orc[idx] / host[idx]
    # write to file
    outdir = os.path.join(const.input_path, app_opts.dname)
    np.savetxt(os.path.join(outdir, 'L_0_CONTROL_WTS_res.txt'), cwts_res,
               fmt='%.18g', header=header, comments='')
    np.savetxt(os.path.join(outdir, 'L_0_CONTROL_WTS_orc.txt'), cwts_orc,
               fmt='%.18g', header=header, comments='')
    # host zones by commercial vs. residential
    hzone = np.zeros_like(host)
    hzone[ho > hr] = 1
    np.savetxt(os.path.join(outdir, 'L_0_HOSTZONE.txt'), hzone,
               fmt='%d', header=header, comments='')
    """# fake suitability for now
    if app_opts.region != 'CAsouth':
        vsuit = np.zeros_like(host)
        vsuit[idx] = 1
        np.savetxt(os.path.join(outdir, 'L_0_VECTOR_SUITABILITY.txt'), vsuit,
                   fmt='%.18g', header=header, comments='')
        return"""
    # psyllid relative growth rate (max = 1)
    grid, hdens = inp.load_host(app_opts)
    df = pd.read_csv(os.path.join(
        const.raw_path, 'Weather', 'air_temperature_{}.csv'.format(
            app_opts.dname)))
    vs, nc = np.zeros(grid.n_pos), 0
    for key, col in df.iteritems():
        if key in ['Rindex', 'Gi', 'latitude', 'longitude']:
            continue
        vs += texas.lactin2_acp_fn(col.values - 273.15)
        nc += 1
    vs = vs / nc
    vsuit = np.zeros(grid.shape) - 1
    vsuit[grid.ixy[0], grid.ixy[1]] = vs
    vsuit = vsuit.T
    np.savetxt(os.path.join(outdir, 'L_0_VECTOR_SUITABILITY.txt'), vsuit,
               fmt='%.18g', header=header, comments='')


def make_wsuit_matrices(app_opts):
    # load host
    grid, hdens = inp.load_host(app_opts)
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    # load data
    df = pd.read_csv(os.path.join(
        const.raw_path, 'Weather', 'input_spatial_points_{}.csv'.format(
            app_opts.dname)))
    print df.longitude.min(), df.longitude.max()
    print df.latitude.min(), df.latitude.max()
    return
    for i, row in df.iterrows():
        x, y = pyproj.transform(
            const.orig, proj, row['longitude'], row['latitude'])
        gi = grid.get_index(x, y)
        print i, gi
        if i == 10:
            break


def make_plant_df(app_opts, no_hh=True, with_tif=False):
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    det_count = np.zeros(shape=(ny, nx), dtype='float32')
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    out_x, out_y = [], []
    fname = os.path.join(const.raw_path, 'Infection', plant_file)
    with fiona.open(fname) as shp:
        print shp.schema
        lons, lats, X, Y = [], [], [], []
        Xi, Yi, Gi, glons, glats = [], [], [], [], []
        dayno, month, year, cycle = [], [], [], []
        weekno, monthno = [], []
        ct, dens = [], []
        for f in shp:
            fp = f['properties']
            '''city = fp['City']
            if no_hh and city == 'Hacienda Heights':
                continue'''
            lon, lat = fp['Longitude'], fp['Latitude']
            if no_hh and (lon >= -117.9636 and lon <= -117.95989) and (
                    lat >= 34.004121 and lat <= 34.011032):
                continue
            x, y = f['geometry']['coordinates']
            gi, xyi = grid.get_index(x, y)
            if gi is None:
                out_x += [x]
                out_y += [y]
                continue
            xi, yi = xyi
            gx, gy = grid.get_xy(xi, yi)
            glon, glat = pyproj.transform(proj, const.orig, gx, gy)
            lon, lat = pyproj.transform(proj, const.orig, x, y)
            det_count[yi, xi] += 1
            X += [x]
            Y += [y]
            Xi += [xi]
            Yi += [yi]
            Gi += [gi]
            dens += [hdens[gi]]
            lons += [lon]
            lats += [lat]
            glons += [glon]
            glats += [glat]
            # cities += [city]
            # county += [fp['County']]
            cycle += [None]  # [fp['Cycle_No']]
            d = datetime.datetime.strptime(fp['Collected_'], '%Y-%m-%d')
            # assert d.month == fp['Month_'] and d.year == fp['Year_']
            dday = (d - const.date0[app_opts.region]).days
            dayno += [dday]
            weekno += [dday / 7]
            monthno += [dday / 30]
            month += [d.month]
            year += [d.year]
            ct += [fp['CtValue']]
    # write records to Excel file
    df = pd.DataFrame({
        'Lon': lons, 'Lat': lats, 'X': X, 'Y': Y,
        'Xi': Xi, 'Yi': Yi, 'Gi': Gi, 'GLon': glons, 'GLat': glats,
        'DayNo': dayno, 'WeekNo': weekno, 'MonthNo': monthno,
        'Month': month, 'Year': year, 'CycleNo': cycle,
        'Ct': ct, 'Density': dens,
    })
    df_out = pd.DataFrame({'X': out_x, 'Y': out_y})
    df_out = df_out.drop_duplicates()
    print "({}/{}) out of landscape".format(
        len(df_out), len(df.groupby(['X', 'Y'])))
    df.to_csv(os.path.join(
        const.raw_path, 'Infection', '{}_plant_samples.csv'.format(
            app_opts.dname)), index=False)
    # snapshot of total detections
    if with_tif:
        tif_base = os.path.join(
            const.raw_path, 'Host', 'CitrusRasters',
            '{}_residential_raster_{}km.tif'.format(
                app_opts.region, app_opts.csize * app_opts.cscale))
        with rasterio.open(tif_base) as src:
            kwargs = src.meta
        tif_outfile = os.path.join(
            const.raw_path, 'Infection',
            '{}_plant_samples.tif'.format(app_opts.dname))
        with rasterio.open(tif_outfile, 'w', **kwargs) as dst:
            dst.write(det_count, 1)
    return df, df_out


def make_psyllid_df(app_opts, no_hh=True, no_nymph=False, with_tif=False):
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    det_count = np.zeros(shape=(ny, nx), dtype='float32')
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    out_x, out_y = [], []
    fname = os.path.join(const.raw_path, 'Infection', psyllid_file)
    with fiona.open(fname) as shp:
        print shp.schema
        lons, lats, X, Y = [], [], [], []
        Xi, Yi, Gi, glons, glats, Gx, Gy = [], [], [], [], [], [], []
        dayno, month, year, cycle = [], [], [], []
        weekno, monthno = [], []
        ct, dens = [], []
        for f in shp:
            fp = f['properties']
            '''city = fp['City']
            if no_hh and city == 'Hacienda Heights':
                continue'''
            if no_nymph and fp['Insect_Sta'] == 'N':
                continue
            lon, lat = fp['Longitude'], fp['Latitude']
            if no_hh and (lon >= -117.9636 and lon <= -117.95989) and (
                    lat >= 34.004121 and lat <= 34.011032):
                continue
            x, y = f['geometry']['coordinates']
            gi, xyi = grid.get_index(x, y)
            if gi is None:
                out_x += [x]
                out_y += [y]
                continue
            xi, yi = xyi
            gx, gy = grid.get_xy(xi, yi)
            glon, glat = pyproj.transform(proj, const.orig, gx, gy)
            lon, lat = pyproj.transform(proj, const.orig, x, y)
            det_count[yi, xi] += 1
            X += [x]
            Y += [y]
            Xi += [xi]
            Yi += [yi]
            Gi += [gi]
            dens += [hdens[gi]]
            lons += [lon]
            lats += [lat]
            glons += [glon]
            glats += [glat]
            Gx += [gx]
            Gy += [gy]
            ct += [fp['CtValue']]
            cycle += [None]  # [fp['Cycle_No']]
            # county += [fp['County']]
            # cities += [city]
            d = datetime.datetime.strptime(fp['Collected_'], '%Y-%m-%d')
            # if d.month != fp['Month_'] or d.year != fp['Year_']:
            #     print (fp['Collected_'], (fp['Month_'], fp['Year_']))
            dday = (d - const.date0[app_opts.region]).days
            dayno += [dday]
            weekno += [dday / 7]
            monthno += [dday / 30]
            month += [d.month]
            year += [d.year]
    # write records to Excel file
    df = pd.DataFrame({
        'Lon': lons, 'Lat': lats, 'X': X, 'Y': Y, 'Gx': Gx, 'Gy': Gy,
        'Xi': Xi, 'Yi': Yi, 'Gi': Gi, 'GLon': glons, 'GLat': glats,
        'DayNo': dayno, 'WeekNo': weekno, 'MonthNo': monthno,
        'Month': month, 'Year': year, 'CycleNo': cycle,
        'Ct': ct, 'Density': dens,
    })
    df_out = pd.DataFrame({'X': out_x, 'Y': out_y})
    df_out = df_out.drop_duplicates()
    print "({}/{}) out of landscape".format(
        len(df_out), len(df.groupby(['X', 'Y'])))
    df.to_csv(os.path.join(
        const.raw_path, 'Infection', '{}_psyllid_samples.csv'.format(
            app_opts.dname)), index=False)
    # snapshot of total detections
    if with_tif:
        tif_base = os.path.join(
            const.raw_path, 'Host', 'CitrusRasters',
            '{}_residential_raster_{}km.tif'.format(
                app_opts.region, app_opts.csize * app_opts.cscale))
        with rasterio.open(tif_base) as src:
            kwargs = src.meta
        tif_outfile = os.path.join(
            const.raw_path, 'Infection',
            '{}_psyllid_samples.tif'.format(app_opts.dname))
        with rasterio.open(tif_outfile, 'w', **kwargs) as dst:
            dst.write(det_count, 1)
    return df, grid


def make_acptrap_df(app_opts, with_tif=False):
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    det_count = np.zeros(shape=(ny, nx), dtype='float32')
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    out_x, out_y = [], []
    lons, lats, X, Y = [], [], [], []
    Xi, Yi, Gi, glons, glats = [], [], [], [], []
    dayno, month, year = [], [], []
    weekno, monthno = [], []
    dens = []
    date_fields = ['TrapDate', 'DateCollec']
    for k, tfile in enumerate(trap_files):
        fname = os.path.join(const.raw_path, 'Infection', tfile)
        with fiona.open(fname) as shp:
            print shp.schema
            for f in shp:
                fp = f['properties']
                dstr = fp[date_fields[k]]
                if dstr is None:
                    continue
                x, y = f['geometry']['coordinates']
                gi, xyi = grid.get_index(x, y)
                if gi is None:
                    out_x += [x]
                    out_y += [y]
                    continue
                xi, yi = xyi
                gx, gy = grid.get_xy(xi, yi)
                glon, glat = pyproj.transform(proj, const.orig, gx, gy)
                lon, lat = pyproj.transform(proj, const.orig, x, y)
                det_count[yi, xi] += 1
                X += [x]
                Y += [y]
                Xi += [xi]
                Yi += [yi]
                Gi += [gi]
                dens += [hdens[gi]]
                lons += [lon]
                lats += [lat]
                glons += [glon]
                glats += [glat]
                d = datetime.datetime.strptime(dstr, '%Y-%m-%d')
                dday = (d - const.date0_acp[app_opts.region]).days
                dayno += [dday]
                weekno += [dday / 7]
                monthno += [dday / 30]
                month += [d.month]
                year += [d.year]
    # write records to Excel file
    df = pd.DataFrame({
        'Lon': lons, 'Lat': lats, 'X': X, 'Y': Y,
        'Xi': Xi, 'Yi': Yi, 'Gi': Gi, 'GLon': glons, 'GLat': glats,
        'DayNo': dayno, 'WeekNo': weekno, 'MonthNo': monthno,
        'Month': month, 'Year': year, 'Density': dens, 
    })
    df_out = pd.DataFrame({'X': out_x, 'Y': out_y})
    df_out = df_out.drop_duplicates()
    print "({}/{}) out of landscape".format(
        len(df_out), len(df.groupby(['X', 'Y'])))
    df.to_csv(os.path.join(
        const.raw_path, 'Infection', '{}_acptrap_samples.csv'.format(
            app_opts.dname)), index=False)
    # snapshot of total detections
    if with_tif:
        tif_base = os.path.join(
            const.raw_path, 'Host', 'CitrusRasters',
            '{}_residential_raster_{}km.tif'.format(
                app_opts.region, app_opts.csize * app_opts.cscale))
        with rasterio.open(tif_base) as src:
            kwargs = src.meta
        tif_outfile = os.path.join(
            const.raw_path, 'Infection',
            '{}_trapping_samples.tif'.format(app_opts.dname))
        with rasterio.open(tif_outfile, 'w', **kwargs) as dst:
            dst.write(det_count, 1)
    return df, df_out


def compile_data(app_opts, dtype, nosample=False):
    texas.compile_data(app_opts, dtype, nosample)


def reset_data(app_opts, dtype, reset_date):
    assert app_opts.region == 'CVreset'
    # discard samples before the given reset date
    dr = (reset_date - const.date0[app_opts.region]).days
    # reset compiled data
    reader = pd.ExcelFile(os.path.join(
        const.input_path, 'CentralValley_1.0km', '{}_data.xlsx'.format(dtype)))
    writer = pd.ExcelWriter(os.path.join(
        const.input_path, app_opts.dname, '{}_data.xlsx'.format(dtype)))
    df = reader.parse('Day')
    df = df[df.DayNo > dr]
    df.to_excel(writer, 'Day', merge_cells=False)
    df = reader.parse('Week')
    df = df[df.WeekNo > dr / 7]
    df.to_excel(writer, 'Week', merge_cells=False)
    df = reader.parse('Month')
    df = df[df.MonthNo > dr / 30]
    df.to_excel(writer, 'Month', merge_cells=False)
    # reset raw data
    df = pd.read_csv(os.path.join(
        const.raw_path, 'Infection',
        'CentralValley_1.0km_{}_samples.csv'.format(dtype)))
    df = df[df.DayNo > dr]
    df.to_csv(os.path.join(
        const.raw_path, 'Infection', '{}_{}_samples.csv'.format(
            app_opts.dname, dtype)))


def compile_weather_variables(app_opts):
    if app_opts.region == 'CAsouth':
        texas.compile_weather_variables(app_opts)
        return
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    # concatenate files
    df_cells = pd.read_csv(os.path.join(
        const.raw_path, 'Weather', 'PINE_fake_points_Viet_20160101',
        '_20160101', 'air_temperature.csv'))
    print df_cells.shape
    d1 = datetime.date(2016, 1, 1)
    for i in xrange(1, 366):
        dstr = (d1 + datetime.timedelta(i)).strftime('%Y%m%d')
        cols = ['{}{}00'.format(dstr, x) for x in ['00', '06', '12', '18']]
        if i == 365:
            cols = ['{}0000'.format(dstr)]
        df0 = pd.read_csv(os.path.join(
            const.raw_path, 'Weather', 'PINE_fake_points_Viet_{}'.format(dstr),
            '_{}'.format(dstr), 'air_temperature.csv'), usecols=cols)
        # print df0.shape
        df_cells = pd.concat([df_cells, df0], axis=1)
    print df_cells.shape
    #
    lons, lats = df_cells['longitude'].values, df_cells['latitude'].values
    xs, ys = pyproj.transform(const.orig, proj, lons, lats)
    assert np.abs(xs[0] - grid.bins[0][0]) * grid.cscale < grid.csize
    assert np.abs(xs[0] - xs[nx]) * grid.cscale < grid.csize
    assert np.abs(ys[0] - grid.bins[1][-1]) * grid.cscale < grid.csize
    assert np.abs(ys[0] - ys[1]) * grid.cscale < grid.csize
    xis = np.tile(np.arange(nx), (ny, 1)).flatten()
    yis = np.tile(np.arange(ny - 1, -1, -1), (nx, 1)).T.flatten()
    assert len(xis) == len(yis), (len(xis), len(yis))
    # construct indices
    ii = []
    for i in xrange(grid.n_pos):
        ix, iy = grid.ixy[0][i], grid.ixy[1][i]
        ii += [np.argwhere((xis == ix) * (yis == iy))[0][0]]
    df_ext = df_cells.loc[ii, :]
    df_ext['Gi'] = np.arange(grid.n_pos)
    df_ext.to_csv(os.path.join(
        const.raw_path, 'Weather', 'air_temperature_{}.csv'.format(
            app_opts.dname)), index=False)


def mark_orchards(app_opts, threshold=1e-3):
    texas.mark_orchards(app_opts, threshold=threshold)
