import data.const as const
import data.input.texas as inp
import citrus

import os
import numpy as np
import numpy.random as nr
import copy
import rasterio
import fiona
import datetime
import pandas as pd
import pyproj
from sklearn.gaussian_process import GaussianProcessRegressor as GPR
import sklearn.gaussian_process.kernels as kernels

# Raise recursion limit for FL
import sys
sys.setrecursionlimit(1500)

n_header = 6
ntree = 129.7 * 247.106  # mean no. trees per km2 in orchards

hlb_file = 'TX_hlb_samples_3083_20161216.shp'
plant_file = 'TX_plant_samples_thru_20181010_proj_3083.shp'
psyllid_file = 'TX_psyllid_samples_thru_20181010_proj_3083.shp'
nosample_file = 'TX_no_samples_thru_20181010_proj_3083.shp'


def combine_host_rasters(app_opts, norm=True, ignore_threshold=0.01):
    """
    Prepare L_0_HOSTDENSITY.txt by combining commercial & residential rasters
    """
    header = citrus.read_header(app_opts.region, app_opts.resol)
    area = (app_opts.csize * app_opts.cscale) ** 2
    host = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.txt'.format(
            app_opts.region, app_opts.resol)), skiprows=n_header)
    if app_opts.urban_fraction == 0 or app_opts.urban_fraction == 1:
        host *= app_opts.urban_fraction
    else:
        assert app_opts.urban_fraction > 0 and app_opts.urban_fraction < 1
        i, j = np.where(host > 0)
        n = len(i)
        rem = nr.choice(n, int(n * (1 - app_opts.urban_fraction)),
                        replace=False)
        host[i[rem], j[rem]] = 0
    np.savetxt(os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster.txt'.format(app_opts.dname)),
        host, fmt='%.18g', header=header, comments='')
    host += np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_commercial_raster_{}.txt'.format(
            app_opts.region, app_opts.resol)),
        skiprows=n_header) * ntree * area
    if norm:
        host = host / ntree
        if ignore_threshold is not None:
            host[host < ignore_threshold] = 0
    # write combined host file
    fname = 'L_0_HOSTDENSITY'
    np.savetxt(os.path.join(
        const.input_path, app_opts.dname, '{}.txt'.format(fname)),
        host, fmt='%.18g', header=header, comments='')
    # write tiff file
    tif_base = os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.tif'.format(
            app_opts.region, app_opts.resol))
    with rasterio.open(tif_base) as src:
        kwargs = src.meta
    with rasterio.open(os.path.join(
            const.input_path, app_opts.dname, '{}.tif'.format(fname)),
            'w', **kwargs) as dst:
        dst.write(host.astype(kwargs['dtype']), 1)


def adjust_host_by_mask(app_opts):
    # load host
    tif_fname = os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.tif'.format(
            app_opts.region, app_opts.resol))
    with rasterio.open(tif_fname) as src:
        host = src.read(1)
        kwargs = src.meta
    # exclude region (higher primary rate)
    he = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_exclude_regions.txt'.format('_'.join(
            app_opts.dname.split('_')[:2]))), skiprows=n_header)
    host[he > 0] = 0
    # write adjusted back to text file
    header = citrus.read_header(app_opts.region, app_opts.resol)
    np.savetxt(os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.txt'.format(
            app_opts.region, app_opts.resol)), host,
        fmt='%.18g', header=header, comments='')
    # write to tiff file
    with rasterio.open(tif_fname, 'w', **kwargs) as dst:
        dst.write(host.astype(kwargs['dtype']), 1)


def adjust_host_by_data(app_opts):
    # Load hosts
    # commercial
    chost = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_commercial_raster_{}.txt'.format(
            app_opts.region, app_opts.resol)), skiprows=n_header).T
    # residential
    rhost = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.txt'.format(
            app_opts.region, app_opts.resol)), skiprows=n_header).T
    # overall
    with rasterio.open(os.path.join(
            const.input_path, app_opts.dname, 'L_0_HOSTDENSITY.tif')) as src:
        host = src.read(1).T
        kwargs = src.meta
    hi, hj = np.nonzero(host)
    gis = np.arange(len(hi))
    #
    # List of cells with any sample recorded
    plant_df = pd.read_excel(os.path.join(
        const.input_path, app_opts.dname, 'plant_data.xlsx'),
        sheetname='Month')
    psyllid_df = pd.read_excel(os.path.join(
        const.input_path, app_opts.dname, 'psyllid_data.xlsx'),
        sheetname='Month')
    gis_withsample = np.unique(np.append(plant_df['Gi'].values,
                                         psyllid_df['Gi'].values))
    gis_nosample = np.setdiff1d(gis, gis_withsample)
    for gi in gis_nosample:
        i, j = hi[gi], hj[gi]
        if chost[i, j] == 0:
            host[i, j] = 0
            rhost[i, j] = 0
    # Write back
    # overall
    host = host.T
    header = citrus.read_header(app_opts.region, app_opts.resol)
    txt_fname = os.path.join(
        const.input_path, app_opts.dname, 'L_0_HOSTDENSITY.txt')
    np.savetxt(txt_fname, host, fmt='%.18g', header=header, comments='')
    with rasterio.open(os.path.join(
            const.input_path, app_opts.dname, 'L_0_HOSTDENSITY.tif'),
            'w', **kwargs) as dst:
        dst.write(host.astype(kwargs['dtype']), 1)
    # residential
    rhost = rhost.T
    np.savetxt(os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.txt'.format(
            app_opts.region, app_opts.resol)), rhost,
        fmt='%.18g', header=header, comments='')
    with rasterio.open(os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.tif'.format(
            app_opts.region, app_opts.resol)), 'w', **kwargs) as dst:
        dst.write(rhost.astype(kwargs['dtype']), 1)


def make_covariate_matrices(app_opts, norm=True, ignore_threshold=0.01,
                            with_kappa=True):
    """
    Prepare L_0_SUSCEPTIBILITY.txt and L_0_INFECTIOUSNESS.txt
    """
    header = citrus.read_header(app_opts.region, app_opts.resol)
    area = (app_opts.csize * app_opts.cscale) ** 2
    # commercial orchards
    ho = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_commercial_raster_{}.txt'.format(app_opts.region, app_opts.resol)),
        skiprows=n_header) * ntree * area
    # residential trees
    hr = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster.txt'.format(
            app_opts.dname)), skiprows=n_header)
    # north-west orchards (intensive control)
    hc = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_control_raster_{}.txt'.format(app_opts.region, app_opts.resol)),
        skiprows=n_header)
    # border region (higher primary rate)
    hb = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_border_raster_{}.txt'.format(app_opts.region, app_opts.resol)),
        skiprows=n_header)
    if norm:
        ho = ho / (ntree * area)
        hr = hr / (ntree * area)
    host = ho + hr
    idx = (host > ignore_threshold)
    print idx.sum()
    # control weights
    cwts_res = copy.copy(hr)
    cwts_nw = copy.copy(hc)
    cwts_orc = copy.copy(ho)
    cwts_res[idx] = cwts_res[idx] / host[idx]
    cwts_orc[idx] = cwts_orc[idx] / host[idx]
    cwts_nw[idx] = cwts_nw[idx] / host[idx]
    cwts_orc -= cwts_nw
    if not np.all(cwts_orc >= 0):
        print (cwts_nw > 0).sum()
        print cwts_orc[np.where(cwts_orc < 0)]
        # print np.argwhere(cwts_orc < 0)
        cwts_orc = cwts_orc.clip(min=0)
    # border area
    hzone = (hb > 0).astype(int)
    # psyllid relative growth rate (max = 1)
    grid, hdens = inp.load_host(app_opts)
    df = pd.read_csv(os.path.join(
        const.raw_path, 'Weather', 'air_temperature_{}.csv'.format(
            app_opts.dname)))
    vs, nc = np.zeros(grid.n_pos), 0
    for key, col in df.iteritems():
        if key in ['Rindex', 'Gi', 'latitude', 'longitude']:
            continue
        vs += lactin2_acp_fn(col.values - 273.15)
        nc += 1
    vs = vs / nc
    vsuit = np.zeros(grid.shape) - 1
    vsuit[grid.ixy[0], grid.ixy[1]] = vs
    vsuit = vsuit.T
    # write to file
    outdir = os.path.join(const.input_path, app_opts.dname)
    np.savetxt(os.path.join(outdir, 'L_0_HOSTZONE.txt'), hzone,
               fmt='%d', header=header, comments='')
    np.savetxt(os.path.join(outdir, 'L_0_CONTROL_WTS_nw.txt'), cwts_nw,
               fmt='%.18g', header=header, comments='')
    np.savetxt(os.path.join(outdir, 'L_0_CONTROL_WTS_res.txt'), cwts_res,
               fmt='%.18g', header=header, comments='')
    np.savetxt(os.path.join(outdir, 'L_0_CONTROL_WTS_orc.txt'), cwts_orc,
               fmt='%.18g', header=header, comments='')
    np.savetxt(os.path.join(outdir, 'L_0_VECTOR_SUITABILITY.txt'), vsuit,
               fmt='%.18g', header=header, comments='')


def make_hlb_df(app_opts, usda_only=False, residential_only=False):
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    det_count = np.zeros(shape=(ny, nx), dtype='float32')
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    out_x, out_y = [], []
    fname = os.path.join(const.raw_path, 'Infection', hlb_file)
    with fiona.open(fname) as shp:
        lons, lats, X, Y = [], [], [], [],
        Xi, Yi, Gi, glons, glats = [], [], [], [], []
        dayno, month, year, monthno, weekno = [], [], [], [], []
        agent, loc, dens = [], [], []
        for f in shp:
            fp = f['properties']
            ag = fp['Agency']
            lo = fp['Location']
            if usda_only and ag != 'USDA':
                continue
            if residential_only and lo != 'Residential':
                continue
            x, y, z = f['geometry']['coordinates']
            gi, xyi = grid.get_index(x, y)
            if gi is None:
                out_x += [x]
                out_y += [y]
                continue
            xi, yi = xyi
            gx, gy = grid.get_xy(xi, yi)
            glon, glat = pyproj.transform(proj, const.orig, gx, gy)
            lon, lat = pyproj.transform(proj, const.orig, x, y)
            det_count[yi, xi] += 1
            X += [x]
            Y += [y]
            Xi += [xi]
            Yi += [yi]
            Gi += [gi]
            lons += [lon]
            lats += [lat]
            glons += [glon]
            glats += [glat]
            dens += [hdens[gi]]
            agent += [ag]
            loc += [lo]
            d = datetime.datetime.strptime(fp['ServiceDat'], '%Y-%m-%d')
            month += [d.month]
            year += [d.year]
            dday = (d - const.date0[app_opts.region]).days
            dayno += [dday]
            weekno += [dday / 7]
            monthno += [dday / 30]
    df = pd.DataFrame({
        'Lon': lons, 'Lat': lats, 'X': X, 'Y': Y,
        'Xi': Xi, 'Yi': Yi, 'Gi': Gi, 'GLon': glons, 'GLat': glats,
        'DayNo': dayno, 'WeekNo': weekno, 'MonthNo': monthno,
        'Month': month, 'Year': year, 'Density': dens
    })
    df_out = pd.DataFrame({'X': out_x, 'Y': out_y})
    df_out = df_out.drop_duplicates()
    print "({}/{}) out of landscape".format(
        len(df_out), len(df.groupby(['X', 'Y'])))
    df.to_csv(os.path.join(
        const.raw_path, 'Infection', '{}_hlb_samples.csv'.format(
            app_opts.dname)), index=False)
    # snapshot of total detections
    tif_base = os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.tif'.format(app_opts.region, app_opts.resol))
    with rasterio.open(tif_base) as src:
        kwargs = src.meta
    tif_outfile = os.path.join(
        const.raw_path, 'Infection',
        '{}_hlb_samples.tif'.format(app_opts.dname))
    with rasterio.open(tif_outfile, 'w', **kwargs) as dst:
        dst.write(det_count, 1)
    return df, grid


def make_plant_df(app_opts, residential_only=False):
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    det_count = np.zeros(shape=(ny, nx), dtype='float32')
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    out_x, out_y = [], []
    fname = os.path.join(const.raw_path, 'Infection', plant_file)
    with fiona.open(fname) as shp:
        print shp.schema
        lons, lats, X, Y = [], [], [], [],
        Xi, Yi, Gi, glons, glats = [], [], [], [], []
        dayno, month, year, monthno, weekno = [], [], [], [], []
        ct, loc, dens = [], [], []
        for f in shp:
            fp = f['properties']
            lo = fp['Site']
            if residential_only and lo != 'Residential':
                continue
            x, y = f['geometry']['coordinates']
            gi, xyi = grid.get_index(x, y)
            if gi is None:
                out_x += [x]
                out_y += [y]
                continue
            xi, yi = xyi
            gx, gy = grid.get_xy(xi, yi)
            glon, glat = pyproj.transform(proj, const.orig, gx, gy)
            lon, lat = pyproj.transform(proj, const.orig, x, y)
            det_count[yi, xi] += 1
            X += [x]
            Y += [y]
            Xi += [xi]
            Yi += [yi]
            Gi += [gi]
            lons += [lon]
            lats += [lat]
            glons += [glon]
            glats += [glat]
            dens += [hdens[gi]]
            loc += [lo]
            d = datetime.datetime.strptime(fp['Sample_Dat'], '%Y-%m-%d')
            month += [d.month]
            year += [d.year]
            dday = (d - const.date0[app_opts.region]).days
            dayno += [dday]
            weekno += [dday / 7]
            monthno += [dday / 30]
            ct += [fp['cLas_ct_va']]
    df = pd.DataFrame({
        'Lon': lons, 'Lat': lats, 'X': X, 'Y': Y,
        'Xi': Xi, 'Yi': Yi, 'Gi': Gi, 'GLon': glons, 'GLat': glats,
        'DayNo': dayno, 'WeekNo': weekno, 'MonthNo': monthno,
        'Month': month, 'Year': year, 'Density': dens, 'Ct': ct,
    })
    df_out = pd.DataFrame({'X': out_x, 'Y': out_y})
    df_out = df_out.drop_duplicates()
    print "({}/{}) out of landscape".format(
        len(df_out), len(df.groupby(['X', 'Y'])))
    df.to_csv(os.path.join(
        const.raw_path, 'Infection', '{}_plant_samples.csv'.format(
            app_opts.dname)), index=False)
    # snapshot of total detections
    tif_base = os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.tif'.format(app_opts.region, app_opts.resol))
    with rasterio.open(tif_base) as src:
        kwargs = src.meta
    tif_outfile = os.path.join(
        const.raw_path, 'Infection',
        '{}_plant_samples.tif'.format(app_opts.dname))
    with rasterio.open(tif_outfile, 'w', **kwargs) as dst:
        dst.write(det_count, 1)
    return df, grid


def make_psyllid_df(app_opts, residential_only=False):
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    det_count = np.zeros(shape=(ny, nx), dtype='float32')
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    out_x, out_y = [], []
    fname = os.path.join(const.raw_path, 'Infection', psyllid_file)
    with fiona.open(fname) as shp:
        print shp.schema
        lons, lats, X, Y = [], [], [], [],
        Xi, Yi, Gi, glons, glats, Gx, Gy = [], [], [], [], [], [], []
        dayno, month, year, monthno, weekno = [], [], [], [], []
        ct, loc, dens = [], [], []
        for f in shp:
            fp = f['properties']
            lo = fp['Site']
            if residential_only and lo != 'Residential':
                continue
            x, y = f['geometry']['coordinates']
            gi, xyi = grid.get_index(x, y)
            if gi is None:
                out_x += [x]
                out_y += [y]
                continue
            xi, yi = xyi
            gx, gy = grid.get_xy(xi, yi)
            glon, glat = pyproj.transform(proj, const.orig, gx, gy)
            lon, lat = pyproj.transform(proj, const.orig, x, y)
            det_count[yi, xi] += 1
            X += [x]
            Y += [y]
            Xi += [xi]
            Yi += [yi]
            Gi += [gi]
            lons += [lon]
            lats += [lat]
            glons += [glon]
            glats += [glat]
            Gx += [gx]
            Gy += [gy]
            dens += [hdens[gi]]
            loc += [lo]
            d = datetime.datetime.strptime(fp['Sample_Dat'], '%Y-%m-%d')
            month += [d.month]
            year += [d.year]
            dday = (d - const.date0[app_opts.region]).days
            dayno += [dday]
            weekno += [dday / 7]
            monthno += [dday / 30]
            ct += [fp['cLas_ct_va']]
    df = pd.DataFrame({
        'Lon': lons, 'Lat': lats, 'X': X, 'Y': Y, 'Gx': Gx, 'Gy': Gy,
        'Xi': Xi, 'Yi': Yi, 'Gi': Gi, 'GLon': glons, 'GLat': glats,
        'DayNo': dayno, 'WeekNo': weekno, 'MonthNo': monthno,
        'Month': month, 'Year': year, 'Density': dens, 'Ct': ct,
    })
    df_out = pd.DataFrame({'X': out_x, 'Y': out_y})
    df_out = df_out.drop_duplicates()
    print "({}/{}) out of landscape".format(
        len(df_out), len(df.groupby(['X', 'Y'])))
    df.to_csv(os.path.join(
        const.raw_path, 'Infection', '{}_psyllid_samples.csv'.format(
            app_opts.dname)), index=False)
    # snapshot of total detections
    tif_base = os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.tif'.format(app_opts.region, app_opts.resol))
    with rasterio.open(tif_base) as src:
        kwargs = src.meta
    tif_outfile = os.path.join(
        const.raw_path, 'Infection',
        '{}_psyllid_samples.tif'.format(app_opts.dname))
    with rasterio.open(tif_outfile, 'w', **kwargs) as dst:
        dst.write(det_count, 1)
    return df, grid


def make_nosample_df(app_opts):
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    det_count = np.zeros(shape=(ny, nx), dtype='float32')
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    out_x, out_y = [], []
    fname = os.path.join(const.raw_path, 'Infection', nosample_file)
    with fiona.open(fname) as shp:
        print shp.schema
        lons, lats, X, Y = [], [], [], [],
        Xi, Yi, Gi, glons, glats, Gx, Gy = [], [], [], [], [], [], []
        dayno, month, year, monthno, weekno = [], [], [], [], []
        dens = []
        for f in shp:
            fp = f['properties']
            x, y = f['geometry']['coordinates']
            gi, xyi = grid.get_index(x, y)
            if gi is None:
                out_x += [x]
                out_y += [y]
                continue
            xi, yi = xyi
            gx, gy = grid.get_xy(xi, yi)
            glon, glat = pyproj.transform(proj, const.orig, gx, gy)
            lon, lat = pyproj.transform(proj, const.orig, x, y)
            det_count[yi, xi] += 1
            X += [x]
            Y += [y]
            Xi += [xi]
            Yi += [yi]
            Gi += [gi]
            lons += [lon]
            lats += [lat]
            glons += [glon]
            glats += [glat]
            Gx += [gx]
            Gy += [gy]
            dens += [hdens[gi]]
            d = datetime.datetime.strptime(fp['Sample_Dat'][:10], '%m/%d/%Y')
            month += [d.month]
            year += [d.year]
            dday = (d - const.date0[app_opts.region]).days
            dayno += [dday]
            weekno += [dday / 7]
            monthno += [dday / 30]
    df = pd.DataFrame({
        'Lon': lons, 'Lat': lats, 'X': X, 'Y': Y, 'Gx': Gx, 'Gy': Gy,
        'Xi': Xi, 'Yi': Yi, 'Gi': Gi, 'GLon': glons, 'GLat': glats,
        'DayNo': dayno, 'WeekNo': weekno, 'MonthNo': monthno,
        'Month': month, 'Year': year, 'Density': dens,
    })
    df_out = pd.DataFrame({'X': out_x, 'Y': out_y})
    df_out = df_out.drop_duplicates()
    print "({}/{}) out of landscape".format(
        len(df_out), len(df.groupby(['X', 'Y'])))
    df.to_csv(os.path.join(
        const.raw_path, 'Infection', '{}_no_samples.csv'.format(
            app_opts.dname)), index=False)
    # snapshot of total detections
    tif_base = os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}.tif'.format(app_opts.region, app_opts.resol))
    with rasterio.open(tif_base) as src:
        kwargs = src.meta
    tif_outfile = os.path.join(
        const.raw_path, 'Infection',
        '{}_no_samples.tif'.format(app_opts.dname))
    with rasterio.open(tif_outfile, 'w', **kwargs) as dst:
        dst.write(det_count, 1)
    return df, grid


def compile_data(app_opts, dtype, nosample):
    """
    Compile data frames into format readily used by the model
    """
    fname = os.path.join(const.raw_path, 'Infection',
                         '{}_{}_samples.csv'.format(
                             app_opts.dname, dtype))
    writer = pd.ExcelWriter(os.path.join(
        const.input_path, app_opts.dname, '{}_data.xlsx'.format(dtype)))
    merge_cells = False
    if dtype == 'hlb' or dtype == 'acptrap':
        cols = ['Gi', 'GLon', 'GLat',
                'DayNo', 'WeekNo', 'MonthNo']
        df = pd.read_csv(fname, usecols=cols)
        if dtype == 'acptrap':
            # merge with psyllid data
            df2 = pd.read_csv(os.path.join(
                const.raw_path, 'Infection', '{}_psyllid_samples.csv'.format(
                    app_opts.dname)), usecols=cols)
            df = df.append(df2, ignore_index=True)
        df['Ct_pos'] = 1
        #
        df_day = df.groupby(['Gi', 'DayNo']).agg({
            'Ct_pos': sum, 'GLon': 'first', 'GLat': 'first'})
        df_day.to_excel(writer, 'Day', merge_cells=merge_cells)
        df_week = df.groupby(['Gi', 'WeekNo']).agg({
            'Ct_pos': sum, 'GLon': 'first', 'GLat': 'first'})
        df_week.to_excel(writer, 'Week', merge_cells=merge_cells)
        df_month = df.groupby(['Gi', 'MonthNo']).agg({
            'Ct_pos': sum, 'GLon': 'first', 'GLat': 'first'})
        df_month.to_excel(writer, 'Month', merge_cells=merge_cells)
    elif dtype == 'psyllid_presence':
        cols = ['Gi', 'GLon', 'GLat', 'Gx', 'Gy',
                'DayNo', 'WeekNo', 'MonthNo']
        df = pd.read_csv(os.path.join(
            const.raw_path, 'Infection', '{}_psyllid_samples.csv'.format(
                app_opts.dname)), usecols=cols)
        df['SampNo'] = 1
        if nosample:
            df_nosample = pd.read_csv(os.path.join(
                const.raw_path, 'Infection', '{}_no_samples.csv'.format(
                    app_opts.dname)), usecols=cols)
            df_nosample['SampNo'] = 0
            df = pd.concat([df, df_nosample])
        #
        df_day = df.groupby(['Gi', 'DayNo']).agg({
            'SampNo': sum, 'GLon': 'first', 'GLat': 'first',
            'Gx': 'first', 'Gy': 'first'})
        df_day.to_excel(writer, 'Day', merge_cells=merge_cells)
        df_week = df.groupby(['Gi', 'WeekNo']).agg({
            'SampNo': sum, 'GLon': 'first', 'GLat': 'first',
            'Gx': 'first', 'Gy': 'first'})
        df_week.to_excel(writer, 'Week', merge_cells=merge_cells)
        df_month = df.groupby(['Gi', 'MonthNo']).agg({
            'SampNo': sum, 'GLon': 'first', 'GLat': 'first',
            'Gx': 'first', 'Gy': 'first'})
        df_month.to_excel(writer, 'Month', merge_cells=merge_cells)   
    else:
        cols = ['Gi', 'GLon', 'GLat',
                'DayNo', 'WeekNo', 'MonthNo', 'Ct']
        df = pd.read_csv(fname, usecols=cols)
        if dtype == 'plant' and nosample:
            df_nosample = pd.read_csv(os.path.join(
                const.raw_path, 'Infection', '{}_no_samples.csv'.format(
                    app_opts.dname)), usecols=cols[:-1])
            df_nosample['Ct'] = 40
            df = pd.concat([df, df_nosample])
        #
        df['Ct_32'] = df['Ct'].apply(lambda x: int(x < 32))
        df['Ct_36'] = df['Ct'].apply(lambda x: int(x < 36))
        df['Ct_38'] = df['Ct'].apply(lambda x: int(x < 38))
        df['Ct_39'] = df['Ct'].apply(lambda x: int(x < 39))
        #
        df_day = df.groupby(['DayNo', 'Gi']).agg({
            'Ct_32': sum, 'Ct': 'count', 'Ct_36': sum, 'Ct_38': sum,
            'GLon': 'first', 'GLat': 'first', 'Ct_39': sum})
        df_day.to_excel(writer, 'Day', merge_cells=merge_cells)
        df_week = df.groupby(['WeekNo', 'Gi']).agg({
            'Ct_32': sum, 'Ct': 'count', 'Ct_36': sum, 'Ct_38': sum,
            'GLon': 'first', 'GLat': 'first'})
        df_week.to_excel(writer, 'Week', merge_cells=merge_cells)
        df_month = df.groupby(['MonthNo', 'Gi']).agg({
            'Ct_32': sum, 'Ct': 'count', 'Ct_36': sum, 'Ct_38': sum,
            'GLon': 'first', 'GLat': 'first'})
        df_month.to_excel(writer, 'Month', merge_cells=merge_cells)


'''def compile_data_matrix(df, grid):
    """
    Matrix of cumulative infections over whole observation period.
    """
    df1 = df.sort_values(by='DayNo')
    t0, tF = df1[:1].DayNo.values[0], df1[-1:].DayNo.values[0]
    days = np.arange(t0, tF + 1)
    Dmat = np.zeros((grid.n_pos, len(days)))
    for i, row in df1.iterrows():
        Dmat[row['Gi'], row['DayNo'] - t0] += 1
    Dmat = Dmat.cumsum(1)
    return days, Dmat'''


def smooth_vector_presence_data(app_opts):
    grid, hdens = inp.load_host(app_opts)
    df = pd.read_excel(os.path.join(
        const.input_path, app_opts.dname, 'psyllid_presence_data.xlsx'),
        sheetname='Day')
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    aa = np.array([1000, 1000., 30.])
    gp = GPR(kernel=kernels.RBF(aa))
    Xtrain = np.vstack([df.Gx.values, df.Gy.values, df.DayNo.values]).T
    ytrain = df.SampNo.values.clip(max=1)
    gp.fit(Xtrain, ytrain)
    print gp.kernel_
    print gp.log_marginal_likelihood()
    print "DONE training", gp.get_params()
    #
    tt = np.arange(df.DayNo.min(), df.DayNo.max() + 1)
    nT = len(tt)
    Gi, Gx, Gy, glons, glats, dayno, sampno = [], [], [], [], [], [], []
    for gi in xrange(grid.n_pos):
        x, y = grid.get_xy(grid.ixy[0][gi], grid.ixy[1][gi])
        out = gp.predict(np.vstack([
            np.ones(nT, dtype=int) * x, np.ones(nT, dtype=int) * y, tt]).T)
        out = (out > .5).astype(int)
        idx = [0] + list(np.where(out[1:] != out[:-1])[0])
        nr = len(idx)
        Gi += list(np.ones(nr, dtype=int) * gi)
        Gx += list(np.ones(nr) * x)
        Gy += list(np.ones(nr) * y)
        glon, glat = pyproj.transform(proj, const.orig, x, y)
        glons += list(np.ones(nr) * glon)
        glats += list(np.ones(nr) * glat)
        dayno += list(tt[idx])
        sampno += list(out[idx])
        if gi % 100 == 0:
            print gi
    df_out = pd.DataFrame({
        'Gi': Gi, 'Gx': Gx, 'Gy': Gy, 'GLon': glons, 'GLat': glats,
        'DayNo': dayno, 'SampNo': sampno,
    })
    #
    df_out.to_excel(pd.ExcelWriter(os.path.join(
        const.input_path, app_opts.dname,
        'psyllid_presence_data_smoothed.xlsx')),
        'Day', merge_cells=False)


def compile_weather_variables(app_opts):
    grid, hdens = inp.load_host(app_opts)
    nx, ny = grid.shape
    proj = pyproj.Proj(init='epsg:{}'.format(const.epsg[app_opts.region]))
    #
    df_cells = pd.read_csv(os.path.join(
        const.raw_path, 'Weather', app_opts.dname, 'air_temperature.csv'))
    lons, lats = df_cells['longitude'].values, df_cells['latitude'].values
    xs, ys = pyproj.transform(const.orig, proj, lons, lats)
    assert np.abs(xs[0] - grid.bins[0][0]) * grid.cscale < grid.csize
    assert np.abs(xs[0] - xs[nx]) * grid.cscale < grid.csize
    assert np.abs(ys[0] - grid.bins[1][-1]) * grid.cscale < grid.csize
    assert np.abs(ys[0] - ys[1]) * grid.cscale < grid.csize
    xis = np.tile(np.arange(nx), (ny, 1)).flatten()
    yis = np.tile(np.arange(ny - 1, -1, -1), (nx, 1)).T.flatten()
    assert len(xis) == len(yis), (len(xis), len(yis))
    # construct indices
    ii = []
    for i in xrange(grid.n_pos):
        ix, iy = grid.ixy[0][i], grid.ixy[1][i]
        ii += [np.argwhere((xis == ix) * (yis == iy))[0][0]]
    df_ext = df_cells.loc[ii, :]
    df_ext['Gi'] = np.arange(grid.n_pos)
    df_ext.to_csv(os.path.join(
        const.raw_path, 'Weather', 'air_temperature_{}.csv'.format(
            app_opts.dname)), index=False)


def lactin2_acp_fn(t):
    # t = temperature
    rho, tmax, delta, lam = 0.0777, 41.97, 12.6581, -0.0914
    rmax = 0.06787345
    rmin = 0.017
    if np.isscalar(t):
        if t <= 10 or t >= 33:
            return 0.
        drate = np.exp(rho * t) - np.exp(rho * tmax - (tmax - t) / delta) + lam
        drate = max(0, (drate - rmin) / (rmax - rmin))
        return drate * (drate > 0) * (drate < 1) + (drate >= 1)
    else:
        drates = np.zeros_like(t)
        idx = (t > 10) * (t < 33)
        drates[idx] = np.exp(rho * t[idx]) - np.exp(
            rho * tmax - (tmax - t[idx]) / delta) + lam
        drates = (drates - rmin) / (rmax - rmin)
        return drates.clip(min=0, max=1)


def compile_city_id(app_opts):
    # 0. Read raster file
    ci = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards', 'TX_cities_raster.txt'),
        skiprows=n_header)
    nr, nc = ci.shape
    # 1. Process populated partial cells
    host = np.loadtxt(os.path.join(
        const.input_path, app_opts.dname, 'L_0_HOSTDENSITY.txt'),
        skiprows=n_header)
    idx = np.argwhere(host > 0)
    for ij in idx:
        i, j = ij
        if int(ci[i, j]) < ci[i, j]:
            i1, i2 = max(0, i - 1), min(nr, i + 2)
            j1, j2 = max(0, j - 1), min(nc, j + 2)
            vals = np.unique(ci[i1:i2, j1:j2])
            vals = vals[vals.astype(int) - vals == 0]
            vals = vals[vals > 0]
            if len(vals) < 2:
                i2 = min(nr, i2 + 1)
                j2 = min(nc, j2 + 1)
                vals = np.unique(ci[i1:i2, j1:j2])
                vals = vals[vals.astype(int) - vals == 0]
                vals = vals[vals > 0]
                if len(vals) < 2:
                    print i, j, ci[i, j]
                    print ci[i1:i2, j1:j2]
                    continue
            elif len(vals) > 2:
                print i, j, ci[i, j]
                print ci[i1:i2, j1:j2]
                continue
            x = (ci[i, j] - vals[1]) / (vals[0] - vals[1])
            ci[i, j] = vals[0] if x > 0.5 else vals[1]
    # 2. Save and print ID list
    header = citrus.read_header(app_opts.region, app_opts.resol)
    cip = np.zeros_like(ci)
    idx = (host > 0)
    cip[idx] = ci[idx]
    np.savetxt(os.path.join(
        const.raw_path, 'TX_cities_processed_raster.txt'),
        cip, fmt='%.18g', header=header, comments='')
    print np.unique(cip)


def mark_orchards(app_opts, threshold=1e-3, with_tiff=False):
    header = citrus.read_header(app_opts.region, app_opts.resol)
    host = np.loadtxt(os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_commercial_raster_{}.txt'.format(
            app_opts.region, app_opts.resol)),
        skiprows=n_header)
    #

    def fill_marks(m, i, j, marks):
        if i < 0 or j < 0 or i >= marks.shape[0] or j >= marks.shape[1]:
            return
        if marks[i, j] > 0 or host[i, j] <= threshold:
            return
        marks[i, j] = m
        fill_marks(m, i, j + 1, marks)
        fill_marks(m, i, j - 1, marks)
        fill_marks(m, i - 1, j, marks)
        fill_marks(m, i + 1, j, marks)
        fill_marks(m, i - 1, j - 1, marks)
        fill_marks(m, i + 1, j + 1, marks)
        fill_marks(m, i - 1, j + 1, marks)
        fill_marks(m, i + 1, j - 1, marks)
        return
    #
    ii, jj = np.where(host > threshold)
    nx, ny = host.shape
    marks = np.zeros((nx, ny))
    m = 0
    for k in xrange(len(ii)):
        i, j = ii[k], jj[k]
        if marks[i, j] > 0:
            continue
        if j > 0 and marks[i, j - 1] > 0:
            marks[i, j] = marks[i, j - 1]
        elif i > 0 and j > 0 and marks[i - 1, j - 1] > 0:
            marks[i, j] = marks[i - 1, j - 1]
        elif i > 0 and marks[i - 1, j] > 0:
            marks[i, j] = marks[i - 1, j]
        elif i > 0 and j < ny - 1 and marks[i - 1, j + 1] > 0:
            marks[i, j] = marks[i - 1, j + 1]
        else:
            m += 1
            fill_marks(m, i, j, marks)
    # write
    fname = 'L_0_orchard_marks'
    np.savetxt(os.path.join(
        const.input_path, app_opts.dname, '{}.txt'.format(fname)),
        marks, fmt='%.18g', header=header, comments='')
    # write tiff file
    if with_tiff:
        tif_base = os.path.join(
            const.raw_path, 'Host', 'CitrusRasters',
            '{}_residential_raster_{}.tif'.format(
                app_opts.region, app_opts.resol))
        with rasterio.open(tif_base) as src:
            kwargs = src.meta
        with rasterio.open(os.path.join(
                const.input_path, app_opts.dname, '{}.tif'.format(fname)),
                'w', **kwargs) as dst:
            dst.write(marks.astype(kwargs['dtype']), 1)
