import data.const as const

import rasterio
from rasterio.transform import from_origin, rowcol, xy
import os
import numpy as np


def make_urban_host_raster(region, resol, otype='tree'):
    codes = [
        122,  # Developed/Low Intensity
        123,  # Developed/Med Intensity
    ]
    ntree = 2.3  # median number of urban trees per pixel
    if region == 'TX':
        tif_infile = [os.path.join(
            const.raw_path, 'Host', 'USCropland',
            'CDL_2016_{}_proj_{}.tif'.format(reg, const.epsg[region]))
            for reg in ['Hidalgo', 'Cameron', 'Willacy', 'Starr']]
    elif region == 'CAsouth':
        tif_infile = [os.path.join(
            const.raw_path, 'Host', 'USCropland',
            'CDL_2016_{}_proj_{}.tif'.format(reg, const.epsg[region]))
            for reg in [
                'Ventura', 'SantaBarbara', 'SanDiego', 'SanBernardino',
                'Riverside', 'Orange', 'LosAngeles', 'Imperial']]
    elif region == 'CentralValley':
        tif_infile = [os.path.join(
            const.raw_path, 'Host', 'USCropland',
            'CDL_2016_{}_proj_{}.tif'.format('CA', const.epsg[region]))]
    elif region == 'FLsouth':
        tif_infile = [os.path.join(
            const.raw_path, 'Host', 'USCropland',
            'CDL_2016_{}_proj_{}.tif'.format(reg, const.epsg[region]))
            for reg in ['Miami', 'Broward', 'PalmBeach']]
    else:
        tif_infile = [os.path.join(
            const.raw_path, 'Host', 'USCropland',
            'CDL_2016_{}_proj_{}.tif'.format(region, const.epsg[region]))]
    # 0. Desired transformation
    # Data must be of uint8 for Windows quick image display
    x0, y0, xF, yF = const.proj_bounds(region)
    n_row = int(np.floor((yF - y0) / resol)) - 1
    n_col = int(np.floor((xF - x0) / resol)) - 1
    out_tf = from_origin(x0, yF, resol, resol)
    dtype = 'uint8' if otype == 'house' else 'float32'
    out_data = np.zeros(shape=(n_row, n_col), dtype=dtype)
    # 0. Write header for PolygonToRaster usage
    ignore, yll_grid = out_tf * (n_col, n_row)
    header = header_str(x0, yll_grid, n_row, n_col, resol)
    fname = os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_Extent_{}km.txt'.format(region, resol / 1000.))
    with open(fname, 'w') as f:
        f.write(header)
    # 1. Load tiff file
    for infile in tif_infile:
        with rasterio.open(infile) as src:
            data = src.read(1)
            # 2. Extract appropriate codes (122, 123)
            idx = np.empty(shape=(0, 2), dtype=int)
            for code in codes:
                idx = np.vstack([idx, np.argwhere(data == code)])
            # 3. To desired resolution
            for k in xrange(len(idx)):
                i, j = idx[k, :]
                x, y = src.xy(i, j)
                x0, x1 = x - 15, x + 15
                y0, y1 = y - 15, y + 15
                i, j = rowcol(out_tf, x, y)
                if j < 1 or i < 1 or j >= n_col - 1 or i >= n_row - 1:
                    continue
                if otype == 'house':
                    out_data[i, j] += 1
                elif otype == 'tree':
                    xb, yb = xy(out_tf, i, j)
                    xb0, xb1 = xb - resol / 2, xb + resol / 2
                    yb0, yb1 = yb - resol / 2, yb + resol / 2
                    p = 1.
                    if xb0 > x0 or xb1 < x1:
                        p_out = (max(xb0 - x0, 0) + max(x1 - xb1, 0)) * p / 30.
                        p -= p_out
                        #
                        p1 = p_out * max(yb0 - y0, 0) / 30.
                        p2 = p * max(yb0 - y0, 0) / 30.
                        p3 = p_out * max(y1 - yb1, 0) / 30.
                        p4 = p * max(y1 - yb1, 0) / 30.
                        p_out -= p1 + p3
                        p -= p2 + p4
                        #
                        j_ud = j - 1 if xb0 > x0 else j + 1
                        out_data[i - 1, j_ud] += p1 * ntree
                        out_data[i - 1, j] += p2 * ntree
                        out_data[i + 1, j_ud] += p3 * ntree
                        out_data[i + 1, j] += p4 * ntree
                        out_data[i, j_ud] += p_out * ntree
                    elif yb0 > y0 or yb1 < y1:
                        p_out = (max(yb0 - y0, 0) + max(y1 - yb1, 0)) * p / 30.
                        p -= p_out
                        #
                        p1 = p_out * max(xb0 - x0, 0) / 30.
                        p2 = p * max(xb0 - x0, 0) / 30.
                        p3 = p_out * max(x1 - xb1, 0) / 30.
                        p4 = p * max(x1 - xb1, 0) / 30.
                        p_out -= p1 + p3
                        p -= p2 + p4
                        #
                        i_ud = i - 1 if yb0 > y0 else i + 1
                        out_data[i_ud, j - 1] += p1 * ntree
                        out_data[i, j - 1] += p2 * ntree
                        out_data[i_ud, j + 1] += p3 * ntree
                        out_data[i, j + 1] += p4 * ntree
                        out_data[i_ud, j] += p_out * ntree
                    out_data[i, j] += p * ntree
                else:
                    assert False, otype
    # 4. Write urban raster as a tiff file
    tif_outfile = os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}km.tif'.format(region, resol / 1000.))
    kwargs = src.meta
    kwargs['dtype'] = dtype
    kwargs['transform'] = out_tf
    kwargs['height'] = n_row
    kwargs['width'] = n_col
    with rasterio.open(tif_outfile, 'w', **kwargs) as dst:
        dst.write(out_data, 1)
    # 5. Write urban raster as a text file
    fname = os.path.join(
        const.raw_path, 'Host', 'CitrusRasters',
        '{}_residential_raster_{}km.txt'.format(region, resol / 1000.))
    np.savetxt(fname, out_data, header=header, comments='')


def header_str(x0, y0, n_row, n_col, resol):
    str = 'ncols {}\nnrows {}\nxllcorner {}\nyllcorner {}\n'.format(
        n_col, n_row, x0, y0) +\
        'cellsize {}\nNODATA_VALUE -1'.format(resol)
    return str


def read_header(region, resol):
    fname = os.path.join(
        const.raw_path, 'Host', 'CitrusOrchards',
        '{}_Extent_{}.txt'.format(region, resol))
    with open(fname, 'r') as f:
        s = f.read()
    return s
