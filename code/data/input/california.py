import data.input.texas as ih
import data.const as const
from idm.model.struct import Population
from idm.data.struct import DiscreteSurvey
from idm.model.hyper import Quarantine, Treatment
from idm.model.hyper import PopInit

import os
import pyproj
import numpy as np
import cPickle as pickle

proj = pyproj.Proj(init='epsg:{}'.format(const.epsg['CA']))


class AppOptions(ih.AppOptions):
    pass


def load_host(app_opts, with_covs=False):
    grid, hdens = ih.load_host(app_opts)
    if not with_covs:
        return grid, hdens
    else:
        n_header = 6
        hzone = np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_HOSTZONE.txt'),
            skiprows=n_header).T[grid.ixy]
        cwts = [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_CONTROL_WTS_res.txt'),
            skiprows=n_header).T[grid.ixy]]
        cwts += [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_CONTROL_WTS_orc.txt'),
            skiprows=n_header).T[grid.ixy]]
        if 'FL' not in app_opts.region:
            vsuit = np.loadtxt(os.path.join(
                const.input_path, app_opts.dname,
                'L_0_VECTOR_SUITABILITY.txt'),
                skiprows=n_header).T[grid.ixy]
            marks = np.loadtxt(os.path.join(
                const.input_path, app_opts.dname, 'L_0_orchard_marks.txt'),
                skiprows=n_header).T[grid.ixy]
        else:
            print "No vector suitability for Florida yet"
            vsuit = np.ones_like(hzone)
            marks = np.ones_like(hzone)
        susc, infs = [np.ones_like(vsuit)], [vsuit]
        return grid, hdens, [susc, infs, hzone.astype(int), cwts, marks]


def load_data(app_opts, dtype, with_traj=False, t0=None):
    return ih.load_data(app_opts, dtype, t0=t0, with_traj=with_traj)


def load_raw_positives(app_opts, dtype, with_neg=False):
    return ih.load_raw_positives(app_opts, dtype, with_neg)


def load_popmodel(
        app_opts, mod_opts, to_learn, learn_init=None, traj_update=True,
        qradius=None, tradius=None, tstop=None, treset=None):
    """
    Population object
    """
    # load citrus landscape
    grid, hdens, tup = load_host(app_opts, with_covs=True)
    susc, infs, hzone, cwts, marks = tup
    # load covariates
    tvec, sint, covs = make_covariates(app_opts, susc, infs, pick_covariates)
    nT = len(tvec) - 1
    assert len(sint) == nT, (len(sint), nT)
    # load outbreak data
    df_list = [load_data(app_opts, dtype, t0=tvec[0])
               for dtype in app_opts.dtypes]
    # construct Population object
    base = [hzone, np.arange(nT)]
    pop = Population(tvec, hdens, base, covs, grid, app_opts.dband,
                     app_opts.n_maxhost, cwts)
    """
    Control objects
    """
    # make quarantine matrix
    quarantines = []
    if qradius is not None:
        if type(qradius) == list:
            for i, r in enumerate(qradius):
                q = Quarantine(r, pop, app_opts.qparams[i])\
                    if r > 0 else None
                quarantines += [q]
        elif qradius > 0:
            quarantines += [Quarantine(qradius, pop, app_opts.qparams)]
    # treatment
    treatments = []
    if tradius is not None:
        if type(tradius) == list:
            for i, r in enumerate(tradius):
                t = Treatment(r, pop, app_opts.tparams[i], marks, tstop,
                              treset) if r > 0 else None
                treatments += [t]
        elif tradius > 0:
            treatments += [Treatment(
                tradius, pop, app_opts.tparams, marks, tstop, treset)]

    """
    Others
    """
    if not os.path.isfile(app_opts.sw_file):
        print "Creating sw file", app_opts.sw_file
        sw = pop.get_continuous_weight(None)
        pickle.dump(sw, open(app_opts.sw_file, "wb"))
    mod_opts.sw_file = app_opts.sw_file
    # derive initial state
    init = get_initial_state(df_list, grid, app_opts, to_learn)

    def data_obj():
        assert len(app_opts.obs_models) == len(df_list), app_opts.obs_models
        fields = ['PosNo', 'IncNo', 'SampNo'] if len(app_opts.dthres) == 2\
            else ['PosNo', 'SampNo']
        if app_opts.n_dcomp > 0:
            data = [DiscreteSurvey.from_df(
                df_list[:-app_opts.n_dcomp], app_opts.obs_models[
                    :-app_opts.n_dcomp], pop, app_opts.dtau[1], fields)]
            data += [DiscreteSurvey.from_df(
                df_list[-app_opts.n_dcomp:], app_opts.obs_models[
                    -app_opts.n_dcomp:], pop, app_opts.dtau[1], fields)]
        else:
            assert len(app_opts.dtypes) == 1 and (
                app_opts.dtypes[0] == 'plant' or
                app_opts.dtypes[0] == 'acptrap')
            data = [DiscreteSurvey.from_df(
                df_list, app_opts.obs_models, pop,
                app_opts.dtau[1], fields), None]
        return data

    if mod_opts.param0:
        # adjust initial parameter values
        if mod_opts.param_wbins['gamma'] and nT > 1 and np.isscalar(
                mod_opts.param0['gamma']):
            mod_opts.param0['gamma'] = np.tile(
                2 * mod_opts.param0['gamma'] * sint[None, :], (2, 1))
        if 'W' in app_opts.mlib.name and nT > 1 and mod_opts.param_wbins[
                'delta'] and np.isscalar(mod_opts.param0['delta']):
            mod_opts.param0['delta'] = np.tile(
                mod_opts.param0['delta'] * sint[None, :], (2, 1))
            mod_opts.param_wconst['delta'] = mod_opts.param0['delta'][0, 0]
    elif mod_opts.param_dist:
        # adjust parameter values
        for pname in mod_opts.param_dist.keys():
            if 'gamma' in pname:
                if nT > 1 and mod_opts.param_wbins[pname] and\
                   np.isscalar(mod_opts.param_dist[pname][0]):
                    samps = []
                    for i in xrange(len(mod_opts.param_dist[pname])):
                        samps += [np.tile(
                            2 * mod_opts.param_dist[pname][i] * sint[None, :],
                            (2, 1))]
                    mod_opts.param_dist[pname] = samps
        if ('W' in app_opts.mlib.name or 'V' in app_opts.mlib.name)\
           and nT > 1 and mod_opts.param_wbins['delta'] and np.isscalar(
                mod_opts.param_dist['delta'][0]):
            samps = []
            for i in xrange(len(mod_opts.param_dist['delta'])):
                samps += [np.tile(
                    mod_opts.param_dist['delta'][i] * sint[None, :],
                    (2, 1))]
            mod_opts.param_dist['delta'] = samps
    else:
        assert False
    if to_learn:
        assert learn_init is not None
        data = data_obj()
        pmodel = load_learnmodel(
            app_opts, pop, data, init, mod_opts, learn_init, traj_update)
        if pmodel is None:
            assert False, 'Failed to initialise learning model'
    else:
        data = data_obj()[0] if app_opts.with_data else None
        pmodel = app_opts.mlib.PriorModel(
            pop, mod_opts, init, dataP=data, quarantines=quarantines,
            treatments=treatments)
    return pmodel


def make_qmat(app_opts, qrts, tvec):
    qdates, qwts = qrts
    nT = len(tvec) - 1
    qmat = np.zeros((len(qwts[0]), nT))
    j = 0
    t = (qdates[j] - const.date0[app_opts.region]).days
    for i in xrange(nT):
        while j < len(qdates) - 1 and tvec[i] > t:
            j += 1
            t = (qdates[j] - const.date0[app_opts.region]).days
        if j > 0:
            qmat[:, i] = qwts[j - 1] * 1
    return qmat


def get_initial_state(df_list, grid, app_opts, to_learn):
    if app_opts.mlib.name == "SECD_SEI":
        assert app_opts.init_source == 'data'
        print "SECD_SEI initial state ..."
        n_pop = grid.n_pos
        value = np.zeros((n_pop, app_opts.mlib.n_comp))
        assert len(df_list) == 2
        df_hlb, df_acp = df_list
        assert app_opts.dtypes[0] == 'plant'
        df = df_hlb[df_hlb.PosNo > 0]
        df = df[df.TauNo < app_opts.t0]  # observed symptomatics by t0
        print 'hlb', df.shape
        for i, row in df.iterrows():
            value[int(row['Gi']), 2] += 1  # set instantaneous D
            value = value.clip(max=1)
            # ad hoc adjustment
            if app_opts.init_dt > 0:
                value[:, 1] = value[:, 2]  # set C
                value[:, 2] = 0
        assert app_opts.dtypes[1] == 'acptrap'
        df = df_acp[df_acp.PosNo > 0]
        df = df[df.TauNo < app_opts.t0 - app_opts.init_dt]
        print 'acp', df.shape
        for i, row in df.iterrows():
            value[int(row['Gi']), -1] += 1  # set instantaneous D
            value = value.clip(max=1)
        # assert False
        return PopInit(value, np.array([value * 1]))
    else:
        assert len(app_opts.mlib.name.split('_')) == 1
        return ih.get_initial_state(df_list, grid, app_opts, to_learn)


def pick_covariates(month, year, app_opts, susc, infs):
    return ih.pick_covariates(month, year, app_opts, susc, infs)


def make_covariates(app_opts, susc, infs, pick_cov_fn):
    return ih.make_covariates(app_opts, susc, infs, pick_cov_fn)


def load_learnmodel(app_opts, pop, data, init, mod_opts, linit, traj_update):
    return ih.load_learnmodel(
        app_opts, pop, data, init, mod_opts, linit, traj_update)
