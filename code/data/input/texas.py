import data.const as const
from idm.model.struct import Grid, Population
from idm.model.hyper import PopInit
from idm.data.struct import DiscreteSurvey
from idm.simulation.struct import PopSim, PopLearn
#
import numpy as np
import numpy.random as nr
import os
import pandas as pd
import datetime
import pyproj
import cPickle as pickle
import rasterio
import copy

proj = pyproj.Proj(init='epsg:{}'.format(const.epsg['TX']))


class AppOptions(object):
    def __init__(self, **kwargs):
        self.mlib = kwargs.pop('mlib', None)
        self.region = kwargs.pop('region', 'Hidalgo')
        self.csize = kwargs.pop('csize', 1000)  # in metre
        self.cscale = kwargs.pop('cscale', 1e-3)
        self.n_maxhost = kwargs.pop('n_maxhost', 1)
        self.dband = kwargs.pop('dist_band', None)  # in metre
        self.t0 = kwargs.pop('t0', 1)
        self.tF = kwargs.pop('tF', const.tF(self.region) + 1)
        self.tFobs = kwargs.pop('tFobs', const.tFobs(self.region) + 1)
        self.init_dt = kwargs.pop('latent_init_period', 0)
        self.dtypes = kwargs.pop('data_type', ['hlb'])
        self.dtau = kwargs.pop('data_tau', ['Day', 1])
        assert len(self.dtau) == 2
        self.pthres = kwargs.pop('psyllid_thres', 38)
        self.dthres = kwargs.pop('plant_thres', [36])
        assert len(self.dthres) <= 2
        self.obs_models = kwargs.pop('obs_models', [])
        self.n_dcomp = kwargs.pop('n_dcomp', 0)
        self.adir = kwargs.pop('adir', None)
        self.suffix = kwargs.pop('suffix', None)
        # 0 = none; 1 = northwest only; 2 = spray only; 3 = both
        self.with_weather = kwargs.pop('with_weather', False)
        self.hdens_wsuit = kwargs.pop('hdens_wsuit', True)
        self.vector_cov = kwargs.pop('vector_cov', None)
        self.condor_run = kwargs.pop('condor_run', False)
        self.init_source = kwargs.pop('init_source', 'data')
        self.cprefix = kwargs.pop('cprefix', '')
        self.urban_fraction = kwargs.pop('urban_fraction', 1)
        self.with_data = kwargs.pop('with_data', False)
        self.paramdir = kwargs.pop('paramdir', None)
        self.nw_f = kwargs.pop('nw_f', 1)
        self.qparams = kwargs.pop('qparams', None)
        self.tparams = kwargs.pop('tparams', None)
        if len(kwargs) > 0:
            assert False, "unknown application options: {}".format(kwargs)
        #
        self.resol = '{}km'.format(self.csize * self.cscale)
        self.dname = '{}_{}'.format(
            self.region, self.resol) if self.urban_fraction == 1 else\
            '{}_{}_uf{}'.format(self.region, self.resol, self.urban_fraction)
        if self.condor_run:
            self.outdir = os.path.join('.')
        else:
            self.outdir = os.path.join(
                const.output_path, self.dname, self.adir)
        self.paramdir = self.outdir if self.paramdir is None else self.paramdir
        self.sw_file = os.path.join(
            const.input_path, self.dname,
            'sw_{}km.pkl'.format(self.dband / (self.csize * self.cscale)))
        #
        if not os.path.exists(self.outdir):
            os.makedirs(self.outdir)
        for dtype in self.dtypes:
            assert dtype in ['hlb', 'plant', 'psyllid', 'acptrap'], dtype


def load_popmodel(
        app_opts, mod_opts, to_learn, learn_init=None, traj_update=True,
        qradius=None, tradius=None, tstop=None, from_laststate=False):
    assert qradius is None or qradius == 0, 'not implemented yet'
    # load citrus landscape
    grid, hdens, tup = load_host(app_opts, with_covs=True)
    susc, infs, hzone, cwts = tup
    if app_opts.with_weather and app_opts.hdens_wsuit:
        vsuit = infs[0]
        assert vsuit.sum() < len(vsuit), (vsuit.sum(), len(vsuit))
        hdens *= vsuit
    # load covariates
    tvec, sint, covs = make_covariates(app_opts, susc, infs, pick_covariates)
    nT = len(tvec) - 1
    assert len(sint) == nT, (len(sint), nT)
    # load outbreak data
    df_list = [load_data(app_opts, dtype, t0=tvec[0])
               for dtype in app_opts.dtypes]
    # construct Population object
    base = [hzone, np.arange(nT)]
    pop = Population(tvec, hdens, base, covs, grid, app_opts.dband,
                     app_opts.n_maxhost, cwts)
    if not os.path.isfile(app_opts.sw_file):
        print "Creating sw file", app_opts.sw_file
        sw = pop.get_continuous_weight(None)
        pickle.dump(sw, open(app_opts.sw_file, "wb"))
    mod_opts.sw_file = app_opts.sw_file
    # derive initial state
    init = get_initial_state(df_list, grid, app_opts, to_learn)

    def data_obj():
        assert len(app_opts.obs_models) == len(df_list), app_opts.obs_models
        fields = ['PosNo', 'IncNo', 'SampNo'] if len(app_opts.dthres) == 2\
            else ['PosNo', 'SampNo']
        if app_opts.n_dcomp > 0:
            data = [DiscreteSurvey.from_df(
                df_list[:-1], app_opts.obs_models[:-1], pop, app_opts.dtau[1],
                fields)]
            data += [DiscreteSurvey.from_df(
                df_list[-1:], app_opts.obs_models[-1:], pop, app_opts.dtau[1],
                fields)]
        else:
            assert len(app_opts.dtypes) == 1 and app_opts.dtypes[0] == 'plant'
            data = [DiscreteSurvey.from_df(
                df_list, app_opts.obs_models, pop,
                app_opts.dtau[1], fields), None]
        return data

    if mod_opts.param0:
        if from_laststate:
            params, ignore = pickle.load(open(os.path.join(
                app_opts.outdir, '{}last_state.pkl'.format(app_opts.cprefix)),
                "rb"))
            for pname in params.keys():
                mod_opts.param0[pname] = params[pname][0] if np.isscalar(
                    mod_opts.param0[pname]) else params[pname].reshape(
                        mod_opts.param0[pname].shape)
        # adjust initial parameter values
        if mod_opts.param_wbins['gamma'] and nT > 1 and np.isscalar(
                mod_opts.param0['gamma']):
            mod_opts.param0['gamma'] = np.tile(
                2 * mod_opts.param0['gamma'] * sint[None, :], (2, 1))
        if 'W' in app_opts.mlib.name and nT > 1 and mod_opts.param_wbins['xi']\
           and np.isscalar(mod_opts.param0['xi']):
            mod_opts.param0['xi'] = np.tile(
                2 * mod_opts.param0['xi'] * sint[None, :], (2, 1))
        if 'W' in app_opts.mlib.name and nT > 1 and mod_opts.param_wbins[
                'delta'] and np.isscalar(mod_opts.param0['delta']):
            mod_opts.param0['delta'] = np.tile(
                mod_opts.param0['delta'] * sint[None, :], (2, 1))
            mod_opts.param_wconst['delta'] = mod_opts.param0['delta'][0, 0]
        for pname in ['varepsilon']:
            if mod_opts.param_wbins[pname] and len(np.unique(hzone)) > 1:
                eps = mod_opts.param0[pname]
                mod_opts.param0[pname] = np.tile(eps, (nT, 1)).T
    elif mod_opts.param_dist:
        # adjust parameter values
        if nT > 1 and mod_opts.param_wbins['gamma']\
           and np.isscalar(mod_opts.param_dist['gamma'][0]):
            samps = []
            for i in xrange(len(mod_opts.param_dist['gamma'])):
                samps += [np.tile(
                    2 * mod_opts.param_dist['gamma'][i] * sint[None, :],
                    (2, 1))]
            mod_opts.param_dist['gamma'] = samps
        if ('W' in app_opts.mlib.name or 'V' in app_opts.mlib.name)\
           and nT > 1 and mod_opts.param_wbins['xi'] and np.isscalar(
                mod_opts.param_dist['xi'][0]):
            samps = []
            for i in xrange(len(mod_opts.param_dist['xi'])):
                samps += [np.tile(
                    2 * mod_opts.param_dist['xi'][i] * sint[None, :],
                    (2, 1))]
            mod_opts.param_dist['xi'] = samps
        if ('W' in app_opts.mlib.name or 'V' in app_opts.mlib.name)\
           and nT > 1 and mod_opts.param_wbins['delta'] and np.isscalar(
                mod_opts.param_dist['delta'][0]):
            samps = []
            for i in xrange(len(mod_opts.param_dist['delta'])):
                samps += [np.tile(
                    mod_opts.param_dist['delta'][i] * sint[None, :],
                    (2, 1))]
            mod_opts.param_dist['delta'] = samps
        for pname in ['varepsilon']:
            if mod_opts.param_wbins[pname] and len(np.unique(hzone)) > 1:
                samps = []
                for i in xrange(len(mod_opts.param_dist[pname])):
                    eps = mod_opts.param_dist[pname][i]
                    samps += [np.tile(eps, (nT, 1)).T]
                mod_opts.param_dist[pname] = samps
    else:
        assert False
    if to_learn:
        assert learn_init is not None
        if from_laststate:
            learn_init = 'last_state'
            traj_update = False
        data = data_obj()
        pmodel = load_learnmodel(
            app_opts, pop, data, init, mod_opts, learn_init, traj_update)
        if pmodel is None:
            assert False, 'Failed to initialise learning model'
    else:
        data = data_obj()[0] if app_opts.with_data else None
        pmodel = app_opts.mlib.PriorModel(pop, mod_opts, init, dataP=data)
    return pmodel


def get_vector_presence(df_vec, month, year, d0, vp_last, n_pos=None):
    if df_vec is None:
        return 1., None
    #
    zprob = .1
    t1 = (datetime.datetime(year, month, 1) - d0).days
    d2 = datetime.datetime(year, month + 1, 1) if month < 12 else\
        datetime.datetime(year + 1, 1, 1)
    t2 = (d2 - d0).days
    df = df_vec[(df_vec.DayNo < t2) & (df_vec.DayNo >= t1)]
    df = df.sort_values('DayNo', ascending=True)
    if vp_last is None:
        vp_last = np.zeros(n_pos) + zprob
        df0 = df_vec[df_vec < t1]
        df0 = df0.sort_values('DayNo', ascending=True)
        df0 = df0.groupby(['Gi']).agg({'SampNo': 'last'})
        for i, row in df0.iterrows():
            if row['SampNo'] > 0:
                vp_last[int(row['Gi'])] = 1
    state, fracs, tp = vp_last, 0, t1
    for i, row in df.iterrows():
        if row['SampNo'] == 0:
            state[int(row['Gi'])] = zprob
        else:
            state[int(row['Gi'])] = 1
        fracs += state * (row['DayNo'] - tp)
        tp = row['DayNo']
    fracs += state * (t2 - tp)
    fracs = fracs / (t2 - t1)
    assert np.all(fracs > 0), np.argwhere(fracs <= 0)
    return fracs, state


def pick_covariates(month, year, app_opts, susc, infs):
    susc_none = np.ones_like(susc[0])
    infs_none = np.ones_like(infs[0])
    if app_opts.with_weather:
        S, I = susc[0], infs[0]
    else:
        S, I = susc_none, infs_none
    assert np.all(I > 0) and np.all(I <= 1)
    assert np.all(S > 0) and np.all(S <= 1)
    return S, I


def make_covariates(app_opts, susc, infs, pick_cov_fn):

    def sin2_pi(tf):
        return np.sin(np.pi * tf) ** 2
    if app_opts.dtypes[0] == 'acptrap':
        date0 = const.date0_acp[app_opts.region]
    else:
        date0 = const.date0[app_opts.region]
    t0 = app_opts.t0 - app_opts.init_dt
    d0 = date0 + datetime.timedelta(float(t0))
    dF = date0 + datetime.timedelta(float(app_opts.tF))
    m0, y0 = d0.month, d0.year
    mF, yF = dF.month, dF.year
    assert y0 < yF, (d0, dF)
    df_vec = None
    n_pos = len(susc[0])
    if app_opts.vector_cov is not None:
        if app_opts.vector_cov == 'raw':
            df_vec = pd.read_excel(os.path.join(
                const.input_path, app_opts.dname,
                'psyllid_presence_data.xlsx'), sheetname='Day')
        else:
            df_vec = pd.read_excel(os.path.join(
                const.input_path, app_opts.dname,
                'psyllid_presence_data_smoothed.xlsx'), sheetname='Day')
    s, i = pick_cov_fn(m0, y0, app_opts, susc, infs)
    vfracs, vp_last = get_vector_presence(df_vec, m0, y0, d0, None, n_pos)
    s, i = s * vfracs, i * vfracs
    tvec, sint, susmat, infmat = [t0], [], [s], [i]
    dp = (d0 - datetime.datetime(y0, 1, 1)).days
    yn = (datetime.date(y0, 12, 31) - datetime.date(y0, 1, 1)).days * 1.
    for m in xrange(m0 + 1, 13):
        d = (datetime.date(y0, m, 1) - datetime.date(y0, 1, 1)).days
        tvec += [(datetime.datetime(y0, m, 1) - date0).days]
        sint += [(sin2_pi(d / yn) + sin2_pi(dp / yn)) * 0.5]
        dp = d
        s, i = pick_cov_fn(m, y0, app_opts, susc, infs)
        vfracs, vp_last = get_vector_presence(df_vec, m, y0, d0, vp_last)
        s, i = s * vfracs, i * vfracs
        susmat += [s]
        infmat += [i]
    for y in xrange(y0 + 1, yF):
        yn = (datetime.date(y, 12, 31) - datetime.date(y, 1, 1)).days * 1.
        for m in xrange(1, 13):
            d = (datetime.date(y, m, 1) - datetime.date(y, 1, 1)).days
            tvec += [(datetime.datetime(y, m, 1) - date0).days]
            sint += [(sin2_pi(d / yn) + sin2_pi(dp / yn)) * 0.5]
            dp = d
            s, i = pick_cov_fn(m, y, app_opts, susc, infs)
            vfracs, vp_last = get_vector_presence(df_vec, m, y, d0, vp_last)
            s, i = s * vfracs, i * vfracs
            susmat += [s]
            infmat += [i]
    yn = (datetime.date(yF, 12, 31) - datetime.date(yF, 1, 1)).days * 1.
    for m in xrange(1, mF + 1):
        d = (datetime.date(yF, m, 1) - datetime.date(yF, 1, 1)).days
        t = (datetime.datetime(yF, m, 1) - date0).days
        if t >= app_opts.tF:
            break
        tvec += [t]
        sint += [(sin2_pi(d / yn) + sin2_pi(dp / yn)) * 0.5]
        dp = d
        s, i = pick_cov_fn(m, yF, app_opts, susc, infs)
        vfracs, vp_last = get_vector_presence(df_vec, m, yF, d0, vp_last)
        s, i = s * vfracs, i * vfracs
        susmat += [s]
        infmat += [i]
    tvec += [app_opts.tF]
    d = (dF - datetime.datetime(yF, 1, 1)).days
    sint += [(sin2_pi(d / yn) + sin2_pi(dp / yn)) * 0.5]
    tvec, sint = np.array(tvec), np.array(sint)
    susmat, infmat = np.array(susmat).T, np.array(infmat).T
    assert len(sint) == susmat.shape[1]
    return tvec, sint, [susmat, infmat]


def get_initial_state(df_list, grid, app_opts, to_learn):
    print "Initial state ..."
    iI = app_opts.mlib.iI
    n_pop = grid.n_pos
    value = np.zeros((n_pop, app_opts.mlib.n_comp))
    t0 = app_opts.t0 - app_opts.init_dt

    if app_opts.init_source == 'data':
        for k, dtype in enumerate(app_opts.dtypes):
            df = df_list[k]
            if dtype == 'plant' or dtype == 'hlb' or dtype == 'acptrap':
                df = df[df.PosNo > 0]
                df = df[df.TauNo < app_opts.t0]  # observed symptomatics by t0
                for i, row in df.iterrows():
                    value[int(row['Gi']), iI] += 1  # set instantaneous I
                    if 'W' in app_opts.mlib.name:
                        value[int(row['Gi']), app_opts.mlib.iW] += 1  # set W
                value = value.clip(max=1)
                # ad hoc adjustment
                if app_opts.init_dt > 0:
                    # value[:, 0] = value[:, iI]  # set E
                    value[:, 1] = value[:, iI]  # set C
                    value[:, iI] = 0
                if to_learn:
                    value[:, :iI + 1] = np.cumsum(value[:, iI::-1], 1)[:, ::-1]
            elif dtype == 'psyllid':
                print "there"
                if 'W' in app_opts.mlib.name:
                    df = df[df.PosNo > 0]
                    df = df[df.TauNo < t0]  # known infections by t0
                    print df.shape
                    for i, row in df.iterrows():
                        value[int(row['Gi']), app_opts.mlib.iW] += 1  # set W
                elif 'V' in app_opts.mlib.name:
                    # initial vector presence
                    df = df[df.TauNo < t0]
                    df = df.drop_duplicates('Gi')
                    print 'V', df.shape
                    for i, row in df.iterrows():
                        value[int(row['Gi']), app_opts.mlib.iV] += 1
        if to_learn:
            prior = np.ones_like(value) * 1e-4  # rate of positives
            return PopInit(value, prior)
        else:
            return PopInit(value, np.array([value * 1.]))
    elif app_opts.init_source == 'infer':
        if app_opts.init_dt > 0:
            prior = PopLearn.load_sims(
                app_opts.paramdir, 'init', prefix=app_opts.cprefix)
        else:
            prior = PopLearn.load_sims(
                app_opts.paramdir, 'at_0', prefix=app_opts.cprefix)
        if prior.shape[2] > app_opts.mlib.n_comp:
            prior = prior[:, :, :app_opts.mlib.n_comp]
        print "Prior", prior[0, :, :].sum(0)
        assert not to_learn
        return PopInit(value, prior)


def load_host(app_opts, with_covs=False):
    print "Loading host ..."
    n_header = 6
    fname = os.path.join(
        const.input_path, app_opts.dname, 'L_0_HOSTDENSITY.tif')
    with rasterio.open(fname) as src:
        host = src.read(1).T
        tf = src.transform
    ixy = np.nonzero(host)
    hdens = host[ixy]
    print "No. of populated cells:", len(ixy[0]), len(hdens)
    print "No. of trees:", hdens.sum() * 129.7 * 247.106
    grid = Grid(tf, host.shape, ixy,
                epsg=const.epsg[app_opts.region], cscale=app_opts.cscale)
    if not with_covs:
        return grid, hdens
    else:
        hzone = np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_HOSTZONE.txt'),
            skiprows=n_header).T[ixy]
        cwts_res = np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_CONTROL_WTS_res.txt'),
            skiprows=n_header).T[ixy]
        cwts_orc = np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_CONTROL_WTS_orc.txt'),
            skiprows=n_header).T[ixy]
        cwts_nw = np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_CONTROL_WTS_nw.txt'),
            skiprows=n_header).T[ixy]
        vsuit = np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_VECTOR_SUITABILITY.txt'),
            skiprows=n_header).T[ixy]
        #
        cwts_orc = cwts_orc + (1. / app_opts.nw_f) * cwts_nw
        cwts_total = cwts_orc + cwts_res
        if not np.all(cwts_total > 0):
            idx = np.where(cwts_total <= 0)
            print cwts_total[idx], cwts_nw[idx], cwts_res[idx], cwts_orc[idx]
            assert False
        susc, infs = [np.ones_like(vsuit)], [vsuit]
        cwts = [cwts_res, cwts_orc]
        return grid, hdens, [susc, infs, hzone.astype(int), cwts]


def load_data(app_opts, dtype, t0=None, with_traj=False, traj_type=0):
    print app_opts.dthres
    # assert False
    df = pd.read_excel(os.path.join(
        const.input_path, app_opts.dname, '{}_data.xlsx'.format(
            dtype)), sheetname=app_opts.dtau[0])
    df.rename(columns={'{}No'.format(app_opts.dtau[0]): 'TauNo'}, inplace=True)
    df = df[df.TauNo < app_opts.tF]
    out_cols = ['Gi', 'TauNo', 'GLon', 'GLat', 'PosNo', 'SampNo']
    if dtype == 'hlb' or dtype == 'acptrap':
        pos_col = 'Ct_pos'
        df['SampNo'] = df[pos_col]
    elif dtype == 'plant':
        pos_col = 'Ct_{}'.format(app_opts.dthres[0])
        if len(app_opts.dthres) == 2:
            inc_col = 'Ct_{}'.format(app_opts.dthres[1])
            df.rename(columns={inc_col: 'IncNo'}, inplace=True)
            out_cols += ['IncNo']
        df.rename(columns={'Ct': 'SampNo'}, inplace=True)
    elif dtype == 'psyllid':
        if app_opts.pthres is not None:
            pos_col = 'Ct_{}'.format(app_opts.pthres[0])
        else:
            pos_col = 'Ct_40'
            df[pos_col] = df['Ct'].values
        df.rename(columns={'Ct': 'SampNo'}, inplace=True)
        # df = df[df[pos_col] > 0]
        if t0 is not None:
            df = df[df.TauNo > t0]
    df.rename(columns={pos_col: 'PosNo'}, inplace=True)
    if with_traj:
        # range expansion trajectory
        df_pos = df[df.PosNo > 0]
        df_pos = df_pos.sort_values('TauNo', ascending=True)
        if traj_type == 0:
            df_pos = df_pos.groupby(['Gi']).agg({'TauNo': 'first'})
            tvec = np.sort(df_pos.TauNo.unique())
            svec = np.zeros_like(tvec)
            for i in xrange(len(tvec)):
                svec[i] = (df_pos.TauNo == tvec[i]).sum()
        elif traj_type == 1:
            # account for host density
            grid, hdens = load_host(app_opts)
            df_pos = df_pos.groupby(['Gi'], as_index=False).agg({
                'TauNo': 'first'})
            tvec = np.sort(df_pos.TauNo.unique())
            svec = np.zeros(len(tvec))
            for i in xrange(len(tvec)):
                dfe = df_pos[df_pos.TauNo == tvec[i]]
                for j, row in dfe.iterrows():
                    svec[i] += hdens[row['Gi']]
        #
        svec = svec.cumsum()
        tvec = np.append(tvec, app_opts.tFobs)
        svec = np.append(svec, svec[-1])
        return df[out_cols], [tvec, svec]
    else:
        return df[out_cols]


def load_raw_positives(app_opts, dtype, with_neg=False):
    cols = ['Lon', 'Lat', 'DayNo', 'Year']
    if dtype == 'hlb' or dtype == 'acptrap':
        fname = '{}_{}_samples.csv'.format(app_opts.dname, dtype)
    else:
        fname = '{}_{}_samples.csv'.format(app_opts.dname, dtype)
        cols += ['Ct']
    df = pd.read_csv(os.path.join(const.raw_path, 'Infection', fname),
                     usecols=cols)
    df = df[df.DayNo < app_opts.tF]
    if dtype == 'plant':
        df_pos = df[df.Ct < app_opts.dthres[0]]
    elif dtype == 'psyllid':
        if app_opts.pthres is not None:
            df_pos = df[df.Ct < app_opts.pthres]
    elif dtype == 'hlb':
        df_pos = df
    elif dtype == 'acptrap':
        df_pos = df
    if with_neg:
        if dtype == 'plant':
            df_neg = df[df.Ct >= app_opts.dthres[0]]
        elif dtype == 'psyllid':
            df_neg = df[df.Ct >= app_opts.pthres]
        elif dtype == 'hlb':
            df_neg = None
        elif dtype == 'acptrap':
            df_neg = None
        return df_pos, df_neg
    else:
        return df_pos


def load_learnmodel(app_opts, pop, data, init, mod_opts, linit, traj_update):
    if linit == 'last_state':
        ignore, trajs = pickle.load(open(os.path.join(
            app_opts.outdir, '{}last_state.pkl'.format(app_opts.cprefix)),
            "rb"))
        init_trajs = [trajs[c] for c in app_opts.mlib.compartments]
    elif linit == 'presim':
        '''presim_dir = os.path.join(const.output_path, app_opts.dname,
                                  'presim_{}'.format(app_opts.suffix))'''
        # try simulations
        sims = PopSim.load_sims(
            app_opts.paramdir, 'single_continuous_{}'.format('med'),
            prefix=app_opts.cprefix)
        s0, T, S = sims['s0'], sims['T'], sims['S']
        s0 = init.value
        # assert np.all(s0 == init.value), (s0.shape, init.value.shape)
        init_trajs = [[
            np.array([[T[0]], [s0[i, c]]]) for i in xrange(s0.shape[0])]
            for c in xrange(app_opts.mlib.n_comp)]
        for i in xrange(len(S)):
            j, k = S[i, :]
            update = app_opts.mlib.rupdate[k]
            assert update[k] == 1
            init_trajs[k][j] = np.hstack([init_trajs[k][j], np.array([
                [T[i + 1]], [init_trajs[k][j][1, -1] + update[k]]])])
        init_trajs = [[
            np.hstack([init_trajs[c][i], np.array([[
                T[-1]], [init_trajs[c][i][1, -1]]])])
            for i in xrange(s0.shape[0])]
            for c in xrange(app_opts.mlib.n_comp)]
    elif linit == 'data':
        # assert app_opts.n_maxhost == 1
        print "Construct learn model ..."
        t0, tF = pop.tvec[[0, -1]]
        s0 = init.value
        iI = app_opts.mlib.iI
        Idata = data[0]
        assert Idata is not None
        Iinit = [np.array([[t0, tF], [s0[j, iI], s0[j, iI]]])
                 for j in xrange(pop.n_pop)]
        countI = 0
        for j in Idata.pos_inds:
            t_pos = Idata.get_nodedata(j)[0].t_1st_pos()
            if t_pos <= t0 or t_pos > tF:
                print j, t_pos
                continue
            t_pos -= app_opts.dtau[1] * nr.rand()
            It, Is = Iinit[j]
            It = np.hstack([It[0], t_pos, It[-1]])
            assert Is[0] == 0
            Is = np.array([0, 1, 1])
            Iinit[j] = np.vstack([It, Is])
            countI += 1
        init_trajs = [Iinit]
        print "data vs. I", len(Idata.pos_inds), countI

        Vdata = data[1]
        print Vdata
        if Vdata is not None:
            iV = iI + 1
            Vinit = [np.array([[t0, tF], [s0[j, iV], s0[j, iV]]])
                     for j in xrange(pop.n_pop)]
            for j in Vdata.pos_inds:
                t_pos = Vdata.get_nodedata(j)[0].t_1st_pos()
                if t_pos <= t0 or t_pos > tF:
                    continue
                t_pos -= app_opts.dtau[1] * nr.rand()
                Vt, Vs = Iinit[j]
                Vt = np.hstack([Vt[0], t_pos, Vt[-1]])
                assert Vs[0] == 0
                Vs = np.array([0, 1, 1])
                Vinit[j] = np.vstack([Vt, Vs])
            init_trajs += [Vinit]
    pmodel = app_opts.mlib.PosteriorModel(
        pop, copy.deepcopy(mod_opts), init, data, init_trajs)
    if traj_update:
        init_logl = pmodel.joint_logl()
        print "Initial log-likelihood:", init_logl
        print "Adjust trajectories ..."
        pmodel.init_node_update()
        init_logl = pmodel.joint_logl()
        print "Initial log-likelihood:", init_logl
    return pmodel
