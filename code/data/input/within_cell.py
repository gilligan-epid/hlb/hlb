import data.input.texas as ih
import data.const as const
from idm.model.struct import Population
from idm.data.struct import DiscreteSurvey
from idm.model.hyper import Quarantine, Treatment
from idm.model.hyper import PopInit

import os
import pyproj
import numpy as np
import cPickle as pickle


class AppOptions(ih.AppOptions):
    pass


def load_host(app_opts, with_covs=False):
    pass
