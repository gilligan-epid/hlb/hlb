import data.const as const
from idm.model.struct import Grid, Population
from idm.model.hyper import PopInit
from idm.data.struct import DiscreteSurvey
from idm.simulation.struct import PopSim
#
import numpy as np
import os
import pandas as pd
import datetime
import pyproj
import cPickle as pickle
import rasterio
import copy

proj = pyproj.Proj(init='epsg:{}'.format(const.epsg['TX']))


class AppOptions(object):
    def __init__(self, **kwargs):
        self.mlib = kwargs.pop('mlib', None)
        self.region = kwargs.pop('region', 'Hidalgo')
        self.csize = kwargs.pop('csize', 1000)  # in metre
        self.cscale = kwargs.pop('cscale', 1e-3)
        self.n_maxhost = kwargs.pop('n_maxhost', 1)
        self.dband = kwargs.pop('dist_band', None)  # in metre
        self.t0 = kwargs.pop('t0', 1)
        self.tF = kwargs.pop('tF', const.tF(self.region) + 1)
        self.border_coef = kwargs.pop('border_coef', 1)
        self.dtypes = kwargs.pop('data_type', ['hlb'])
        self.dtau = kwargs.pop('data_tau', ['Day', 1])
        assert len(self.dtau) == 2
        self.pthres = kwargs.pop('psyllid_thres', 38)
        self.dthres = kwargs.pop('plant_thres', [36])
        assert len(self.dthres) <= 2
        self.obs_models = kwargs.pop('obs_models', [])
        self.adir = kwargs.pop('adir', None)
        self.suffix = kwargs.pop('suffix', None)
        self.init_dt = kwargs.pop('latent_init_period', 0)
        # 0 = none; 1 = spray only; 2 = northwest only; 3 = both
        self.control = kwargs.pop('control', 3)
        if len(kwargs) > 0:
            assert False, "unknown application options: {}".format(kwargs)
        #
        self.resol = '{}km'.format(self.csize / 1000.)
        self.dname = '{}_{}km'.format(self.region, self.csize / 1000.)
        self.outdir = os.path.join(const.output_path, self.dname, self.adir)
        self.sw_file = os.path.join(
            const.input_path, self.dname,
            'sw_{}km.pkl'.format(self.dband / (self.csize * self.cscale)))
        #
        if not os.path.exists(self.outdir):
            os.makedirs(self.outdir)
        for dtype in self.dtypes:
            assert dtype in ['hlb', 'plant', 'psyllid'], dtype


def load_popmodel(app_opts, mod_opts, to_learn):
    # load citrus landscape
    grid, hdens, susc, infs, hzone = load_host(app_opts, with_covs=True)
    # load outbreak data
    df_list = [load_data(app_opts, dtype) for dtype in app_opts.dtypes]
    # load covariates
    tvec, sint, covs = make_covariates(app_opts, susc, infs)
    tvec = np.array([app_opts.t0, app_opts.tF])
    nT = len(tvec) - 1
    # construct Population object
    base = [hzone, np.arange(nT)]
    pop = Population(tvec, hdens, base, covs, grid,
                     app_opts.dband, app_opts.n_maxhost)
    if not os.path.isfile(app_opts.sw_file):
        print "Creating sw file", app_opts.sw_file
        sw = pop.get_continuous_weight(None)
        pickle.dump(sw, open(app_opts.sw_file, "wb"))
    mod_opts.sw_file = app_opts.sw_file
    # derive initial state
    init = get_initial_state(df_list[0], grid, app_opts, to_learn)
    if to_learn:
        assert len(app_opts.obs_models) == len(df_list), app_opts.obs_models
        fields = ['PosNo', 'IncNo', 'SampNo'] if len(app_opts.dthres) == 2\
            else ['PosNo', 'SampNo']
        data = DiscreteSurvey.from_df(
            df_list, app_opts.obs_models, pop, app_opts.dtau[1], fields)
        pmodel = load_learnmodel(app_opts, pop, data, init, mod_opts)
        if pmodel is None:
            assert False, 'Failed to initialise learning model'
    else:
        pass
        #
        pmodel = app_opts.mlib.PriorModel(pop, mod_opts, init)
    return pmodel


def make_covariates(app_opts, susc, infs):
    susc_none = np.ones_like(susc[0])
    infs_none = np.ones_like(infs[0])

    def sin2_pi(tf):
        return np.sin(np.pi * tf) ** 2

    def pick_cov(month, year):
        if app_opts.control == 0:
            return susc_none, infs_none
        elif app_opts.control == 1:
            # north-west control only
            return susc[0], infs[0]
        elif app_opts.control == 2:
            # coordinating spray only
            if month in [11, 2] or (month == 9 and year in [2015, 2016]):
                return susc[1], infs[1]
            else:
                return susc_none, infs_none
        elif app_opts.control == 3:
            # both types of control
            if month in [11, 2] or (month == 9 and year in [2015, 2016]):
                return susc[2], infs[2]
            else:
                return susc[0], infs[0]

    date0 = const.date0[app_opts.region]
    t0 = app_opts.t0 - app_opts.init_dt
    d0 = date0 + datetime.timedelta(float(t0))
    dF = date0 + datetime.timedelta(float(app_opts.tF))
    m0, y0 = d0.month, d0.year
    mF, yF = dF.month, dF.year
    assert y0 < yF
    dp = (d0 - datetime.datetime(y0, 1, 1)).days
    s, i = pick_cov(m0, y0)
    tvec, sint, susmat, infmat = [t0], [], [s], [i]
    yn = (datetime.date(y0, 12, 31) - datetime.date(y0, 1, 1)).days * 1.
    for m in xrange(m0 + 1, 13):
        d = (datetime.date(y0, m, 1) - datetime.date(y0, 1, 1)).days
        tvec += [(datetime.datetime(y0, m, 1) - date0).days]
        sint += [(sin2_pi(d / yn) + sin2_pi(dp / yn)) * 0.5]
        dp = d
        s, i = pick_cov(m, y0)
        susmat += [s]
        infmat += [i]
    for y in xrange(y0 + 1, yF):
        yn = (datetime.date(y, 12, 31) - datetime.date(y, 1, 1)).days * 1.
        for m in xrange(1, 13):
            d = (datetime.date(y, m, 1) - datetime.date(y, 1, 1)).days
            tvec += [(datetime.datetime(y, m, 1) - date0).days]
            sint += [(sin2_pi(d / yn) + sin2_pi(dp / yn)) * 0.5]
            dp = d
            s, i = pick_cov(m, y)
            susmat += [s]
            infmat += [i]
    yn = (datetime.date(yF, 12, 31) - datetime.date(yF, 1, 1)).days * 1.
    for m in xrange(1, mF + 1):
        d = (datetime.date(yF, m, 1) - datetime.date(yF, 1, 1)).days
        tvec += [(datetime.datetime(yF, m, 1) - date0).days]
        sint += [(sin2_pi(d / yn) + sin2_pi(dp / yn)) * 0.5]
        dp = d
        s, i = pick_cov(m, yF)
        susmat += [s]
        infmat += [i]
    tvec += [app_opts.tF]
    d = (dF - datetime.datetime(yF, 1, 1)).days
    sint += [(sin2_pi(d / yn) + sin2_pi(dp / yn)) * 0.5]
    tvec, sint = np.array(tvec), np.array(sint)
    susmat, infmat = np.array(susmat).T, np.array(infmat).T
    assert len(sint) == susmat.shape[1]
    return tvec, sint, [susmat, infmat]


def get_initial_state(df, grid, app_opts, to_learn):
    print "Initial state ..."
    n_pop = grid.n_pos
    prior = np.ones(n_pop)  # equal susceptibility to pre-t0 infection
    value = np.zeros((n_pop, app_opts.mlib.n_comp))
    df = df[df.PosNo > 0]
    df = df[df.TauNo < app_opts.t0]  # known infections by t0
    for i, row in df.iterrows():
            value[int(row['Gi']), -1] += 1  # set instantaneous I
    # ad hoc adjustment
    if app_opts.init_dt > 0:
        value[:, 0] = value[:, -1]  # set E
        value[:, -1] = 0
    if to_learn:
        value = np.cumsum(value[:, ::-1], 1)[:, ::-1]
    return PopInit(value, prior)


def load_host(app_opts, with_covs=False):
    print "Loading host ..."
    n_header = 6
    fname = os.path.join(
        const.input_path, app_opts.dname, 'L_0_HOSTDENSITY.tif')
    with rasterio.open(fname) as src:
        host = src.read(1).T
        tf = src.transform
    ixy = np.nonzero(host)
    hdens = host[ixy]
    print "No. of populated cells:", len(ixy[0])
    grid = Grid(tf, host.shape, ixy,
                epsg=const.epsg[app_opts.region], cscale=app_opts.cscale)
    if not with_covs:
        return grid, hdens
    else:
        susc = [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_SUSC_nwcontrol.txt'),
            skiprows=n_header).T[ixy]]
        infs = [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_INFS_nwcontrol.txt'),
            skiprows=n_header).T[ixy]]
        susc += [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_SUSC_cspray.txt'),
            skiprows=n_header).T[ixy]]
        infs += [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_INFS_cspray.txt'),
            skiprows=n_header).T[ixy]]
        susc += [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_SUSC_nwcontrol_cspray.txt'),
            skiprows=n_header).T[ixy]]
        infs += [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_INFS_nwcontrol_cspray.txt'),
            skiprows=n_header).T[ixy]]
        hzone = np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_HOSTZONE.txt'),
            skiprows=n_header).T[ixy]
        return grid, hdens, susc, infs, hzone.astype(int)


def load_data(app_opts, dtype, with_traj=False):
    df = pd.read_excel(os.path.join(
        const.input_path, app_opts.dname, '{}_data.xlsx'.format(
            dtype)), sheetname=app_opts.dtau[0])
    df.rename(columns={'{}No'.format(app_opts.dtau[0]): 'TauNo'}, inplace=True)
    out_cols = ['Gi', 'TauNo', 'GLon', 'GLat', 'PosNo', 'SampNo']
    if dtype == 'hlb':
        pos_col = 'Ct_pos'
        df['SampNo'] = 1
    elif dtype == 'plant':
        pos_col = 'Ct_{}'.format(app_opts.dthres[0])
        if len(app_opts.dthres) == 2:
            inc_col = 'Ct_{}'.format(app_opts.dthres[1])
            df.rename(columns={inc_col: 'IncNo'}, inplace=True)
            out_cols += ['IncNo']
        df.rename(columns={'Ct': 'SampNo'}, inplace=True)
    elif dtype == 'psyllid':
        pos_col = 'Ct_{}'.format(app_opts.pthres)
        df.rename(columns={'Ct': 'SampNo'}, inplace=True)
        df = df[df[pos_col] > 0]
    df.rename(columns={pos_col: 'PosNo'}, inplace=True)
    if with_traj:
        # range expansion trajectory
        df_pos = df[df.PosNo > 0]
        tvec = np.sort(df_pos.TauNo.unique())
        svec = np.zeros_like(tvec)
        for i in xrange(len(tvec)):
            svec[i] = (df_pos.TauNo == tvec[i]).sum()
        svec = svec.cumsum()
        return df[out_cols], [tvec, svec]
    else:
        return df[out_cols]


def load_raw_positives(app_opts, dtype):
    cols = ['Lon', 'Lat', 'DayNo']
    if dtype == 'hlb':
        fname = '{}_hlb_samples.csv'.format(app_opts.dname)
    else:
        fname = '{}_{}_samples.csv'.format(app_opts.dname, dtype)
        cols += ['Ct']
    df = pd.read_csv(os.path.join(const.raw_path, 'Infection', fname),
                     usecols=cols)
    if dtype != 'hlb':
        df = df[df.Ct < app_opts.dthres[0]]
    return df


def load_learnmodel(app_opts, pop, data, init, mod_opts):
    presim_dir = os.path.join(
        const.output_path, app_opts.dname, 'presim_{}'.format(app_opts.suffix))
    # try simulations
    sims = PopSim.load_sims(
        presim_dir, 'single_continuous_{}'.format('med'))
    s0, T, S = sims['s0'], sims['T'], sims['S']
    s0 = init.value
    # assert np.all(s0 == init.value), (s0.shape, init.value.shape)
    init_trajs = [[
        np.array([[T[0]], [s0[i, c]]]) for i in xrange(s0.shape[0])]
        for c in xrange(app_opts.mlib.n_comp)]
    for i in xrange(len(S)):
        j, k = S[i, :]
        update = app_opts.mlib.rupdate[k]
        assert update[k] == 1
        init_trajs[k][j] = np.hstack([init_trajs[k][j], np.array([
            [T[i + 1]], [init_trajs[k][j][1, -1] + update[k]]])])
    init_trajs = [[
        np.hstack([init_trajs[c][i], np.array([[
            T[-1]], [init_trajs[c][i][1, -1]]])])
        for i in xrange(s0.shape[0])]
        for c in xrange(app_opts.mlib.n_comp)]
    pmodel = app_opts.mlib.PosteriorModel(
        pop, copy.deepcopy(mod_opts), init, data, init_trajs)
    print "Before", pmodel.params['beta'].value
    dlogl = pmodel.beta_gibbs()
    print "After", pmodel.params['beta'].value, dlogl
    assert False
    print "Adjust trajectories ..."
    pmodel.init_node_update()
    init_logl = pmodel.joint_logl()
    print "Initial log-likelihood:", init_logl
    return pmodel
