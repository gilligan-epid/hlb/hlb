import data.input.california as ca
import data.input.texas as ih
import data.const as const
from idm.model.struct import Population
from idm.model.hyper import Quarantine, Treatment


import os
import numpy as np
import cPickle as pickle


class AppOptions(ca.AppOptions):
    pass


def load_host(app_opts, with_covs=False):
    grid, hdens = ih.load_host(app_opts)
    if not with_covs:
        return grid, hdens
    else:
        n_header = 6
        hzone = np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_HOSTZONE.txt'),
            skiprows=n_header).T[grid.ixy]
        cwts = [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_CONTROL_WTS_res.txt'),
            skiprows=n_header).T[grid.ixy]]
        cwts += [np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_CONTROL_WTS_orc.txt'),
            skiprows=n_header).T[grid.ixy]]
        marks = np.loadtxt(os.path.join(
            const.input_path, app_opts.dname, 'L_0_orchard_marks.txt'),
            skiprows=n_header).T[grid.ixy]
        vsuit = np.ones_like(hzone)
        susc, infs = [np.ones_like(vsuit)], [vsuit]
        return grid, hdens, [susc, infs, hzone.astype(int), cwts, marks]


def load_data(app_opts, dtype, with_traj=False, t0=None):
    return ca.load_data(app_opts, dtype, t0=t0, with_traj=with_traj)


def load_raw_positives(app_opts, dtype='hlb', with_neg=False):
    return ca.load_raw_positives(app_opts, dtype, with_neg)


def get_initial_state(df, grid, app_opts, to_learn):
    return ca.get_initial_state(df, grid, app_opts, to_learn)


def make_covariates(app_opts, susc, infs, pick_cov_fn):
    return ca.make_covariates(app_opts, susc, infs, pick_cov_fn)


def pick_covariates(month, year, app_opts, susc, infs):
    return ca.pick_covariates(month, year, app_opts, susc, infs)


def load_popmodel(
        app_opts, mod_opts, to_learn, qradius=None, tradius=None, tstop=None, 
        treset=None):    # load citrus landscape
    grid, hdens, tup = load_host(app_opts, with_covs=True)
    susc, infs, hzone, cwts, marks = tup
    # load covariates
    tvec, sint, covs = make_covariates(app_opts, susc, infs, pick_covariates)
    print("init_dt", app_opts.init_dt, "tvec", tvec[[0, -1]])
    nT = len(tvec) - 1
    assert len(sint) == nT, (len(sint), nT)
    # load outbreak data
    df_list = [load_data(app_opts, dtype, t0=tvec[0])
               for dtype in app_opts.dtypes]
    # construct Population object
    base = [hzone, np.arange(nT)]
    pop = Population(tvec, hdens, base, covs, grid, app_opts.dband,
                     app_opts.n_maxhost, cwts)
    # make quarantine matrix
    quarantines = []
    if qradius is not None:
        if type(qradius) == list:
            for i, r in enumerate(qradius):
                q = Quarantine(r, pop, app_opts.qparams[i])\
                    if r > 0 else None
                quarantines += [q]
        elif qradius > 0:
            quarantines += [Quarantine(qradius, pop, app_opts.qparams)]
    # treatment
    treatments = []
    if tradius is not None:
        if type(tradius) == list:
            for i, r in enumerate(tradius):
                t = Treatment(r, pop, app_opts.tparams[i], marks, tstop,
                              treset) if r > 0 else None
                treatments += [t]
        elif tradius > 0:
            treatments += [Treatment(
                tradius, pop, app_opts.tparams, marks, tstop, treset)]
    #
    if not os.path.isfile(app_opts.sw_file):
        print "Creating sw file", app_opts.sw_file
        sw = pop.get_continuous_weight(None)
        pickle.dump(sw, open(app_opts.sw_file, "wb"))
    mod_opts.sw_file = app_opts.sw_file
    # derive initial state
    init = get_initial_state(df_list, grid, app_opts, to_learn)

    if mod_opts.param0:
        # adjust initial parameter values
        if mod_opts.param_wbins['gamma'] and nT > 1 and np.isscalar(
                mod_opts.param0['gamma']):
            mod_opts.param0['gamma'] = np.tile(
                2 * mod_opts.param0['gamma'] * sint[None, :], (2, 1))
        if 'W' in app_opts.mlib.name and nT > 1 and mod_opts.param_wbins[
                'delta'] and np.isscalar(mod_opts.param0['delta']):
            mod_opts.param0['delta'] = np.tile(
                mod_opts.param0['delta'] * sint[None, :], (2, 1))
            mod_opts.param_wconst['delta'] = mod_opts.param0['delta'][0, 0]
    elif mod_opts.param_dist:
        # adjust parameter values
        for pname in mod_opts.param_dist.keys():
            if 'gamma' in pname:
                if nT > 1 and mod_opts.param_wbins[pname] and\
                   np.isscalar(mod_opts.param_dist[pname][0]):
                    samps = []
                    for i in xrange(len(mod_opts.param_dist[pname])):
                        samps += [np.tile(
                            2 * mod_opts.param_dist[pname][i] * sint[None, :],
                            (2, 1))]
                    mod_opts.param_dist[pname] = samps
        if ('W' in app_opts.mlib.name or 'V' in app_opts.mlib.name)\
           and nT > 1 and mod_opts.param_wbins['delta'] and np.isscalar(
                mod_opts.param_dist['delta'][0]):
            samps = []
            for i in xrange(len(mod_opts.param_dist['delta'])):
                samps += [np.tile(
                    mod_opts.param_dist['delta'][i] * sint[None, :],
                    (2, 1))]
            mod_opts.param_dist['delta'] = samps
    else:
        assert False

    if to_learn:
        assert False
    else:
        pmodel = app_opts.mlib.PriorModel(
            pop, mod_opts, init, dataP=None, quarantines=quarantines,
            treatments=treatments)
    return pmodel
